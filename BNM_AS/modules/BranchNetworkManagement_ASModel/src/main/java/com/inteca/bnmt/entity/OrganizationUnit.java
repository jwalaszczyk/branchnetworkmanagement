package com.inteca.bnmt.entity;

import javax.persistence.*;


@Entity
@Table(name = "OrganizationUnit")
@NamedEntityGraphs({
    @NamedEntityGraph(name = "organizationUnitWithAddress", attributeNodes = @NamedAttributeNode(value = "address"))
})
public class OrganizationUnit extends AbstractOrganizationUnit {
    private static final long serialVersionUID = 1294199116L;
}
