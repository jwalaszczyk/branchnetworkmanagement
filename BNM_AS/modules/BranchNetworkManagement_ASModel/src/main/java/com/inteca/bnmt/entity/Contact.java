package com.inteca.bnmt.entity;

import com.inteca.bnmt.entity.AbstractContact;

import javax.persistence.Entity;
import javax.persistence.Table;


@Entity
@Table(name = "Contact")
public class Contact extends AbstractContact {
    private static final long serialVersionUID = 1966634461L;
}
