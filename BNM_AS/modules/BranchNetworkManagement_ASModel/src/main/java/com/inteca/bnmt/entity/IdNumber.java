package com.inteca.bnmt.entity;

import com.inteca.bnmt.entity.AbstractIdNumber;

import javax.persistence.Entity;
import javax.persistence.Table;


@Entity
@Table(name = "IdNumber")
public class IdNumber extends AbstractIdNumber {
    private static final long serialVersionUID = 1056657759L;
}
