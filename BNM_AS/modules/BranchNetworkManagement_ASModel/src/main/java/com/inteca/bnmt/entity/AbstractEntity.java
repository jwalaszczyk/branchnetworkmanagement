package com.inteca.bnmt.entity;
 
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.SequenceGenerator;
import java.io.Serializable;

@MappedSuperclass
public class AbstractEntity implements Serializable {
	
  @Id
  @SequenceGenerator(name = "FIRST_JPA_GEN", sequenceName = "FIRST_JPA_SEQ", allocationSize=1, initialValue=1000)
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "FIRST_JPA_GEN")
  private Long id;
 
  public Long getId() {
    return id;
  }
  
  
}