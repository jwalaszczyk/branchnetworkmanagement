package com.inteca.bnmt.entity;

import com.inteca.bnmt.entity.AbstractAddress;

import javax.persistence.Entity;
import javax.persistence.Table;


@Entity
@Table(name = "Address")
public class Address extends AbstractAddress {
    private static final long serialVersionUID = 229114359L;
}
