package com.inteca.bnmt.entity;

import com.inteca.bnmt.entity.AbstractAdSyncData;

import javax.persistence.Entity;
import javax.persistence.Table;


@Entity
@Table(name = "AdSyncData")
public class AdSyncData extends AbstractAdSyncData {
    private static final long serialVersionUID = 855995067L;
}
