package com.inteca.bnmt.entity;

import com.inteca.bnmt.entity.AbstractSalesPointInformation;

import javax.persistence.Entity;
import javax.persistence.Table;


@Entity
@Table(name = "SalesPointInformation")
public class SalesPointInformation extends AbstractSalesPointInformation {
    private static final long serialVersionUID = 738539669L;
}
