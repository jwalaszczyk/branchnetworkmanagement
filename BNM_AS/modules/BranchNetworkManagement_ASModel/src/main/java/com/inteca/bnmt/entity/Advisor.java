package com.inteca.bnmt.entity;

import javax.persistence.*;


@Entity
@Table(name = "Advisor")
@NamedEntityGraphs({
        @NamedEntityGraph(name = "advisorRecursive", includeAllAttributes = true),

        @NamedEntityGraph(name = "advisorWithOrganizationUnitAndPrimary", attributeNodes = {
                @NamedAttributeNode(value = "organizationUnit"),
                @NamedAttributeNode(value = "primary"),
        }),

        @NamedEntityGraph(name = "advisorWithOrganizationUnitAndAdGroup", attributeNodes = {
                @NamedAttributeNode(value = "organizationUnit"),
                @NamedAttributeNode(value = "adGroup")
        })
})
public class Advisor extends AbstractAdvisor {
    private static final long serialVersionUID = 245482947L;
    
    @Override
    public String toString() {
        return getLogin() + " | " + getDataSource();
    }
}
