package com.inteca.bnmt.entity;

import com.inteca.bnmt.entity.AbstractFunctionalRole;

import javax.persistence.Entity;
import javax.persistence.Table;


@Entity
@Table(name = "FunctionalRole")
public class FunctionalRole extends AbstractFunctionalRole {
    private static final long serialVersionUID = 16942518L;
}
