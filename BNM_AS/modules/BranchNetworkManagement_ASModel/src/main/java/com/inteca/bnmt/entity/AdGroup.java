package com.inteca.bnmt.entity;

import com.inteca.bnmt.entity.AbstractAdGroup;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "AdGroup", uniqueConstraints = @UniqueConstraint(columnNames = "name"))
public class AdGroup extends AbstractAdGroup {
    private static final long serialVersionUID = 202341919L;
}
