-- TWORZENIE TABLESPACE
create tablespace BNM_TABLESPACE datafile 'bnm_tabspace.dat' size 100M autoextend on;
create temporary tablespace BNM_TABLESPACE_TEMP tempfile 'bnm_tabspace_temp.dat' size 100M autoextend on;

-- TWORZENIE UŻYTKOWNIKA
create user BRANCHNETWORKMANAGEMENT
  identified by branchnetworkmanagement123 default tablespace BNM_TABLESPACE temporary tablespace BNM_TABLESPACE_TEMP;

-- NADANIE UPRAWNIEŃ
grant create session to BRANCHNETWORKMANAGEMENT;
grant create table to BRANCHNETWORKMANAGEMENT;
grant create sequence to BRANCHNETWORKMANAGEMENT;

alter user BRANCHNETWORKMANAGEMENT quota unlimited on BNM_TABLESPACE;