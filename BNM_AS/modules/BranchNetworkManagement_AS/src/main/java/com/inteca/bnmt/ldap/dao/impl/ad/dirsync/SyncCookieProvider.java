package com.inteca.bnmt.ldap.dao.impl.ad.dirsync;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;

import com.inteca.bnmt.entity.AdSyncData;
import com.inteca.bnmt.repository.AdSyncDataRepository;

public class SyncCookieProvider {
    
    @Autowired 
    private AdSyncDataRepository dataRepo;

    private byte []dirSyncCookie ;

    private String repoKey;
    
    public SyncCookieProvider(String repoKey) {
        this.repoKey=repoKey;
    }

    @PostConstruct
    public void init() {
        String bytes = dataRepo.findByKey(repoKey);
        if(bytes!=null && !"INIT_DATA".equals(bytes)) {
            dirSyncCookie = bytes.getBytes();
        }
        if(bytes==null) {
//          Initail save data
            dataRepo.save(getAdSyncData(repoKey, "INIT_DATA"));
        }
    }

    public byte [] getCookie() {
        return dirSyncCookie;
    }

    public void update(byte [] cookie) {
        dirSyncCookie = cookie;
        dataRepo.updateValueForKey(repoKey, new String(dirSyncCookie));
    }

    private AdSyncData getAdSyncData(String key, String value) {
        AdSyncData d = new AdSyncData();
        d.setKey(key);
        d.setValue(value);
        return d;
    }
}
