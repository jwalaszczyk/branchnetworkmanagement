package com.inteca.bnmt.ldap.service.exceptions;

public class LdapUpdateException extends Exception {

	private static final long serialVersionUID = 1L;
	public LdapUpdateException(Exception e) {
		super(e);
	}
    public LdapUpdateException(String msg, Exception e) {
        super(msg, e);
    }


}
