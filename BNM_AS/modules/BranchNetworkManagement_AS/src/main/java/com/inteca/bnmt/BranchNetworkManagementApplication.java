package com.inteca.bnmt;

import java.util.Arrays;

import com.inteca.bnmt.entity.AbstractEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.info.BuildProperties;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.inteca.bnmt.ldap.dao.ADUserDao;
import com.inteca.bnmt.ldap.dao.impl.ad.dirsync.SyncCookieProvider;
import com.inteca.bnmt.ldap.service.LDAPUserService;


@SpringBootApplication(scanBasePackages={"com.inteca.bnmt", "com.inteca.bnmt.translation"})
@ComponentScan(basePackages = "com.inteca.bnmt")
@EntityScan(basePackageClasses = { AbstractEntity.class })
@EnableJpaRepositories("com.inteca.bnmt.repository")
@EnableScheduling
@EnableTransactionManagement
@EnableAutoConfiguration
public class BranchNetworkManagementApplication {
    private static final Logger log = LoggerFactory.getLogger(BranchNetworkManagementApplication.class);

    public static void main(String[] args) {
        log.info("Starting BranchNetworkManagementApplication with args: {}", Arrays.asList(args));
        ConfigurableApplicationContext context = SpringApplication.run(BranchNetworkManagementApplication.class, args);
        log.info("Build info");
        context.getBean(BuildProperties.class).forEach( i -> log.info("{}: {}" , i.getKey(), i.getValue()));
    }
    
    @Bean("internalUserService")
    public LDAPUserService intenalUserService(ADUserDao aDIntUserDao) {
        return new LDAPUserService(aDIntUserDao);
    }
    
    @Bean("externalUserService")
    public LDAPUserService externalUserService(ADUserDao aDExtUserDao) {
        return new LDAPUserService(aDExtUserDao);
    }

    @Bean
    public SyncCookieProvider externalCookieProvider() {
        return new SyncCookieProvider("com.inteca.ldap.ad.ext.sync.changed.update");
    }
    
    @Bean
    public SyncCookieProvider internalCookieProvider() {
        return new SyncCookieProvider("com.inteca.ldap.ad.int.sync.changed.update");
    }
    
}
