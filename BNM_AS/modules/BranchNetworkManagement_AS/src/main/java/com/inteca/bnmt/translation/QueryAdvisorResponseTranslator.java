package com.inteca.bnmt.translation;

import java.util.List;
import java.util.Set;

import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Mappings;
import org.mapstruct.ReportingPolicy;

import com.inteca.bnmt.entity.Address;
import com.inteca.bnmt.entity.Advisor;
import com.inteca.bnmt.entity.FunctionalRole;
import com.inteca.bnmt.entity.OrganizationUnit;

import pl.credit_agricole.schemas.bnmt._105._2018._08._001.AdvisorList;
import pl.credit_agricole.schemas.bnmt._105._2018._08._001.FunctionalRoleList;
import pl.credit_agricole.schemas.bnmt._105._2018._08._001.SalesPoint2;
import pl.credit_agricole.schemas.bnmt._105._2018._08._001.SalesPointList2;

@Mapper(unmappedTargetPolicy=ReportingPolicy.IGNORE)
public abstract class QueryAdvisorResponseTranslator {

    public AdvisorList translate(List<Advisor> advisors) {
        AdvisorList al = new AdvisorList();
        al.setAdvsr(toCAAdvisorList(advisors));
        return al;
    }
    
    public abstract List<pl.credit_agricole.schemas.bnmt._105._2018._08._001.Advisor> toCAAdvisorList(List<Advisor> advisors);

    @Mappings({
        @Mapping(target="email", source="email"),
        @Mapping(target="frstNm", source="firstName"),
        @Mapping(target="lgn", source="login"),
        @Mapping(target="mobNb", source="mobileNumber"),
        @Mapping(target="oprtrId", source="operatorId"),
        @Mapping(target="srnm", source="surname"),
        @Mapping(target="sttnryNb", source="stationaryNumber"),
        @Mapping(target="fctnlRoleList", source="functionalRole"),
        @Mapping(target="salesPtList", source="organizationUnit")
    })
    public abstract pl.credit_agricole.schemas.bnmt._105._2018._08._001.Advisor toCAAdvisor (Advisor advisor);
    
    public FunctionalRoleList toFunctionalRoleListList(Set<FunctionalRole> frs) {
        FunctionalRoleList frl = new FunctionalRoleList();
        frl.setFctnlRole(toFunctionalRoleList(frs));
        return frl;
    }
    
    @Mappings({
        @Mapping(target="cd", source="code"),
        @Mapping(target="id", source="roleId"),
        @Mapping(target="nm", source="name")
    })
    public abstract pl.credit_agricole.schemas.bnmt._105._2018._08._001.FunctionalRole toFunctionalRole(FunctionalRole fr);
    
    public abstract List<pl.credit_agricole.schemas.bnmt._105._2018._08._001.FunctionalRole> toFunctionalRoleList(Set<FunctionalRole> frs);
    
    public SalesPointList2 toSalesPointList2(Set<OrganizationUnit> spi) {
        SalesPointList2 spl = new SalesPointList2();
        spl.setSalesPt(toSalesPointList(spi));
        return spl;
    }

    public abstract List<SalesPoint2> toSalesPointList(Set<OrganizationUnit> spi);
    
    @Mappings({
        @Mapping(target="id", source="unitIdNumber"),
        @Mapping(target="nm", source="name"),
        @Mapping(target="pmry", constant="false"),
        @Mapping(target="brcd", source="brcd"),
        @Mapping(target="tp", source="type"),
        @Mapping(target="adr", source="address")
    })
    public abstract SalesPoint2 toSalesPoint2(OrganizationUnit ou);
    
    @Mappings({
        @Mapping(source = "city", target = "city"),
        @Mapping(source = "country", target = "ctry"),
        @Mapping(source = "flatNumber", target = "flatNo"),
        @Mapping(source = "houseNumber", target = "hsNo"),
        @Mapping(source = "postOffice", target = "pstOffc"),
        @Mapping(source = "street", target = "strt"),
        @Mapping(source = "streetKind", target = "strtKnd"),
        @Mapping(source = "zipCode", target = "zpCd")
    })
    public abstract pl.credit_agricole.schemas.bnmt._105._2018._08._001.Address toAddress(Address adr);

    @AfterMapping
    protected void resolveCrossData(@MappingTarget pl.credit_agricole.schemas.bnmt._105._2018._08._001.Advisor advisor, Advisor src) {
        updateAdvisorPrimaryOU(advisor.getSalesPtList(), src.getPrimary());
        updateRegionsIds(advisor.getSalesPtList(), src.getOrganizationUnit());
    }

    private void updateAdvisorPrimaryOU(SalesPointList2 salesPtList, OrganizationUnit primary) {
        if(primary==null) return;
        for(pl.credit_agricole.schemas.bnmt._105._2018._08._001.SalesPoint2 sp: salesPtList.getSalesPt()) {
//            TODO verify that we use correct value from OU 
            if(!sp.getId().equals(String.valueOf(primary.getUnitIdNumber()))) continue;
            sp.setPmry(true);
            return;
        }
    }

    private void updateRegionsIds(SalesPointList2 salesPtList, Set<OrganizationUnit> set) {
        // TODO Auto-generated method stub
//        czekamy na analize
    }
}
