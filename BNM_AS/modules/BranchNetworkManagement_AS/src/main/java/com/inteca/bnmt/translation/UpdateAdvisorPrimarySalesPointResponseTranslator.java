package com.inteca.bnmt.translation;

import com.inteca.bnmt.dto.PrimarySalesPointUpdateResult;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import pl.credit_agricole.schemas.bnmt._107._2018._08._001.UpdateAdvisorPrimarySalesPointResponse;

@Mapper
public interface UpdateAdvisorPrimarySalesPointResponseTranslator {

    @Mappings({
        @Mapping(source = "status", target = "rspnSts.sts"),
        @Mapping(source = "warningList", target = "rspnSts.wrngList.wrng")
    })
    UpdateAdvisorPrimarySalesPointResponse translate(PrimarySalesPointUpdateResult result);

    @Mappings({
        @Mapping(source = "attribute", target = "wrngAttr"),
        @Mapping(source = "code", target = "wrngCd"),
        @Mapping(source = "clazz", target = "wrngClss"),
        @Mapping(source = "description", target = "wrngDsc"),
        @Mapping(source = "element", target = "wrngElmt"),
        @Mapping(source = "value", target = "wrngVl")
    })
    pl.credit_agricole.schemas.bnmt._107._2018._08._001.Warning toWarning(com.inteca.bnmt.dto.Warning warning);
}
