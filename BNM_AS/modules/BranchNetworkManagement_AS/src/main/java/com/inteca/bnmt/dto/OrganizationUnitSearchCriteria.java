package com.inteca.bnmt.dto;

public class OrganizationUnitSearchCriteria {

    private Integer operatorId;
    private String login;

    private String brcd;
    private String unitId;

    public Integer getOperatorId() {
        return operatorId;
    }

    public void setOperatorId(Integer operatorId) {
        this.operatorId = operatorId;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login.isEmpty() ? null : login;
    }

    public String getBrcd() {
        return brcd;
    }

    public void setBrcd(String brcd) {
        this.brcd = brcd.isEmpty() ? null : brcd;
    }

    public String getUnitId() {
        return unitId;
    }

    public void setUnitId(String unitId) {
        this.unitId = unitId.isEmpty() ? null : unitId;
    }
}
