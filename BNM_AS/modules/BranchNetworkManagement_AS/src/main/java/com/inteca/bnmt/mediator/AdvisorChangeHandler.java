package com.inteca.bnmt.mediator;

import org.springframework.scheduling.annotation.Async;

import com.inteca.bnmt.entity.Advisor;

public interface AdvisorChangeHandler {

    @Async
    void notifyUserRemoved(Advisor user);

    @Async
    void notifyUserChanged(Advisor user);

}
