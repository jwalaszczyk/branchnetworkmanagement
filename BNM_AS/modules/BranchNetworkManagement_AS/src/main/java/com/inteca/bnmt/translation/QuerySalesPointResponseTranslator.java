package com.inteca.bnmt.translation;

import com.inteca.bnmt.entity.OrganizationUnit;
import com.inteca.bnmt.translation.util.ListWrapper;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.Named;
import pl.credit_agricole.schemas.bnmt._103._2015._08._001.QuerySalesPointResponse;
import pl.credit_agricole.schemas.bnmt._103._2015._08._001.SalesPoint;

import java.math.BigDecimal;
import java.util.List;

@Mapper
public abstract class QuerySalesPointResponseTranslator {

    public QuerySalesPointResponse translate(List<OrganizationUnit> organizationUnits) {
        return translate(new ListWrapper<>(organizationUnits));
    }

    @Mappings({
        @Mapping(source = "list", target = "salesPtList.salesPt")
    })
    public abstract QuerySalesPointResponse translate(ListWrapper<OrganizationUnit> organizationUnits);

    @Mappings({
        @Mapping(source = "address", target = "adr"),
        @Mapping(source = "brcd", target = "brcd"),
        @Mapping(source = "channel", target = "chanl"),
        @Mapping(source = "unitId", target = "id"),
        @Mapping(source = "name", target = "nm"),
        @Mapping(source = ".", target = "rgnId", qualifiedByName = "regionIdResolver"),
        @Mapping(source = "type", target = "tp")
    })
    public abstract SalesPoint toSalesPoint(OrganizationUnit organizationUnit);

    @Named("regionIdResolver")
    public BigDecimal getRegionId(OrganizationUnit organizationUnit) {
        return null; // todo czekamy na analize algorytmu region ID
    }

    @Mappings({
        @Mapping(source = "city", target = "city"),
        @Mapping(source = "country", target = "ctry"),
        @Mapping(source = "flatNumber", target = "flatNo"),
        @Mapping(source = "houseNumber", target = "hsNo"),
        @Mapping(source = "postOffice", target = "pstOffc"),
        @Mapping(source = "street", target = "strt"),
        @Mapping(source = "streetKind", target = "strtKnd"),
        @Mapping(source = "zipCode", target = "zpCd")
    })
    public abstract pl.credit_agricole.schemas.bnmt._103._2015._08._001.Address toAddress(com.inteca.bnmt.entity.Address organizationUnit);
}
