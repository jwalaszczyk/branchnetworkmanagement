package com.inteca.bnmt.dto;


import java.util.ArrayList;
import java.util.List;

public class PrimarySalesPointUpdateResult {
    private boolean status = false;
    private List<Warning> warningList = new ArrayList<>();

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public List<Warning> getWarningList() {
        return warningList;
    }

    public void setWarningList(List<Warning> warningList) {
        this.warningList = warningList;
    }
}
