package com.inteca.bnmt.ldap.dao.impl.ad;

public class AccountControlFlags {
	private AccountControlFlags () {}
	
	public static final int SCRIPT = 0x0001;
	public static final int ACCOUNTDISABLE=0x0002;
	public static final int HOMEDIR_REQUIRED=0x0008;
	public static final int LOCKOUT=0x0010;
	public static final int PASSWD_NOTREQD=0x0020;
	public static final int ENCRYPTED_TEXT_PWD_ALLOWED=0x0080;
	public static final int TEMP_DUPLICATE_ACCOUNT=0x0100;
	public static final int NORMAL_ACCOUNT=0x0200;
	public static final int INTERDOMAIN_TRUST_ACCOUNT=0x0800;
	public static final int WORKSTATION_TRUST_ACCOUNT=0x1000;
	public static final int SERVER_TRUST_ACCOUNT=0x2000;
	public static final int DONT_EXPIRE_PASSWORD=0x10000;
	public static final int MNS_LOGON_ACCOUNT=0x20000;
	public static final int SMARTCARD_REQUIRED=0x40000;
	public static final int TRUSTED_FOR_DELEGATION=0x80000;
	public static final int NOT_DELEGATED=0x100000;
	public static final int USE_DES_KEY_ONLY=0x200000;
	public static final int DONT_REQ_PREAUTH=0x400000;
	public static final int PASSWORD_EXPIRED=0x800000;
	public static final int TRUSTED_TO_AUTH_FOR_DELEGATION=0x1000000;
	public static final int PARTIAL_SECRETS_ACCOUNT=0x04000000;  
}
