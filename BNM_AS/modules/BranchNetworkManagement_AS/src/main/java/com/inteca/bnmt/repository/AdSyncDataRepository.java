package com.inteca.bnmt.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.inteca.bnmt.entity.AdSyncData;

public interface AdSyncDataRepository extends JpaRepository<AdSyncData, Long>{
    
    @Transactional(readOnly=true)
    @Query("Select value FROM AdSyncData WHERE key=:key")
    String findByKey(@Param(value = "key") String key);
    
    @Transactional(readOnly=false)
    @Modifying
    @Query("UPDATE AdSyncData SET value=:value WHERE key=:key")
    void updateValueForKey(@Param(value = "key") String key, @Param(value = "value") String value);

}
