package com.inteca.bnmt.ldap.service.exceptions;

public class LdapSearchException extends Exception {

	private static final long serialVersionUID = 1L;
    public LdapSearchException(String msg, Exception e) {
        super(msg, e);
    }


}
