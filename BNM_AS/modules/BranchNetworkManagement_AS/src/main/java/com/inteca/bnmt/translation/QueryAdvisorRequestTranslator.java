package com.inteca.bnmt.translation;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.ReportingPolicy;

import com.inteca.bnmt.dto.AdvisorsSearchCriteria;
import com.inteca.bnmt.entity.FunctionalRole;
import com.inteca.bnmt.entity.OrganizationUnit;

import pl.credit_agricole.schemas.bnmt._104._2018._08._001.QueryAdvisorRequest;
import pl.credit_agricole.schemas.bnmt._104._2018._08._001.SalesPoint1;

@Mapper(unmappedTargetPolicy=ReportingPolicy.IGNORE)
public interface QueryAdvisorRequestTranslator {

    @Mappings({
        @Mapping(target="functionalRoles", source="fctnlRoleList.fctnlRole"),
        @Mapping(target="organizationUnits", source="salesPtList.salesPt")
    })
    AdvisorsSearchCriteria translate(QueryAdvisorRequest request);
    
    @Mappings({
        @Mapping(target="unitIdNumber", source="id")
    })
    OrganizationUnit toOU(SalesPoint1 sp);

    @Mappings({
        @Mapping(target="roleId", source="id"),
        @Mapping(target="code", source="cd"),
        @Mapping(target="name", source="nm")
    })
    FunctionalRole toFunctionalRole(pl.credit_agricole.schemas.bnmt._104._2018._08._001.FunctionalRole fr);

}
