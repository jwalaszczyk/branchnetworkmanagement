package com.inteca.bnmt.utils;

import org.apache.commons.lang.StringUtils;
import org.hibernate.boot.model.naming.Identifier;
import org.hibernate.boot.model.naming.PhysicalNamingStrategy;
import org.hibernate.engine.jdbc.env.spi.JdbcEnvironment;

import java.util.regex.Pattern;

/**
 * This naming strategy is underscore separated all uppercase. It wraps every identifier with backquote ` character
 * If created name is longer than 30 chars it starts to removing all vovels from end until length is equal to {@link #maxNameLength}
 * Default maxNameLength is equal to {@link #ORACLE_11_MAX_IDENTIFIER_LENGTH} so its 30 bytes
 * <br>
 * Warning: if in the word there is more than 30 consonants its crops end. Thats may cause DDL exception duplicated name.
 *
 * @author bwiercinski
 * <p>
 * todo chars are treated same as bytes. Oracle limit is 30 BYTES
 */
public class OracleNamingStrategy implements PhysicalNamingStrategy {

    private final static Pattern VOVELS = Pattern.compile(".*[EUIOAY].*");

    public static final int ORACLE_11_MAX_IDENTIFIER_LENGTH = 30;

    public static final int ORACLE_12_MAX_IDENTIFIER_LENGTH = 128;

    protected final int maxNameLength;

    public OracleNamingStrategy(int maxNameLength) {
        this.maxNameLength = maxNameLength;
    }

    public OracleNamingStrategy() {
        this(ORACLE_11_MAX_IDENTIFIER_LENGTH);
    }

    @Override
    public Identifier toPhysicalCatalogName(Identifier identifier, JdbcEnvironment jdbcEnv) {
        return convert(identifier);
    }

    @Override
    public Identifier toPhysicalColumnName(Identifier identifier, JdbcEnvironment jdbcEnv) {
        return convert(identifier);
    }

    @Override
    public Identifier toPhysicalSchemaName(Identifier identifier, JdbcEnvironment jdbcEnv) {
        return convert(identifier);
    }

    @Override
    public Identifier toPhysicalSequenceName(Identifier identifier, JdbcEnvironment jdbcEnv) {
        return convert(identifier);
    }

    @Override
    public Identifier toPhysicalTableName(Identifier identifier, JdbcEnvironment jdbcEnv) {
        return convert(identifier);
    }

    protected String convert(String identifier, int maxNameLength) {

        String newName = identifier.replaceAll("([a-z])([A-Z])", "$1_$2").toUpperCase();

        while (newName.length() > maxNameLength && VOVELS.matcher(newName).matches()) {
            newName = newName.replaceFirst("[EUIOAY]", "");
        }

        newName = newName.substring(0, Math.min(newName.length(), maxNameLength));
        if (newName.endsWith("_")) {
            newName = newName.substring(0, newName.length() - 1);
        }
        return newName;
    }

    protected Identifier convert(Identifier identifier, int maxNameLength) {
        if (identifier == null || StringUtils.isBlank(identifier.getText())) {
            return identifier;
        }
        return Identifier.toIdentifier(String.format("`%s`", convert(identifier.getText(), maxNameLength)));
    }

    protected Identifier convert(Identifier identifier) {
        return convert(identifier, maxNameLength);
    }
}
