package com.inteca.bnmt.profile;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsOperations;
import org.springframework.stereotype.Component;

@Component
public class Listener {
    
    @Autowired
    private JmsOperations jmsOperations;
    
    public void receive(){
        Object ob = jmsOperations.receiveAndConvert("QUEUE.USER");
        System.out.println("Get: " + ob);
    }
}
