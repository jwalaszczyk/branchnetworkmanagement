package com.inteca.bnmt.ldap.dao.impl.generic;

import java.util.List;

import javax.naming.directory.ModificationItem;
import javax.naming.directory.SearchControls;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ldap.control.PagedResultsCookie;
import org.springframework.ldap.control.PagedResultsDirContextProcessor;
import org.springframework.ldap.core.DirContextAdapter;
import org.springframework.ldap.core.DirContextProcessor;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.core.support.AggregateDirContextProcessor;

import com.inteca.bnmt.ldap.dao.impl.ad.dirsync.DeleteSyncContextProcesor;
import com.inteca.bnmt.ldap.dao.impl.ad.mappers.ADUserContextMapper;
import com.inteca.bnmt.ldap.dao.impl.ad.mappers.DeletedUserContextMapper;
import com.inteca.bnmt.ldap.domain.LdapUser;


public class LDAPUserDaoImpl {
    
    private static final Logger log = LoggerFactory.getLogger(LDAPUserDaoImpl.class);

    private LdapTemplate ldapTemplate;
	private String ldapPersonClass;
    private String peselFiledName;


    private ADUserContextMapper userContextMapper;
    private DeletedUserContextMapper deletedUserContextMapper = new DeletedUserContextMapper();

    private PagedResultsCookie pageCookie;
	
    public LDAPUserDaoImpl(LdapTemplate ldapTemplate, ADUserContextMapper mapper, String ldapPersonClass,String peselFiledName) {
		this.ldapTemplate = ldapTemplate;
		userContextMapper = mapper;
		this.ldapPersonClass=ldapPersonClass;
		this.peselFiledName=peselFiledName;
	}
       
	public void setLdapPersonClass(String ldapPersonClass) {
		this.ldapPersonClass = ldapPersonClass;
	}

    public List<LdapUser> getAllChanged(DirContextProcessor proc)  {
        log.debug("Get all changed");
        SearchControls controls = new SearchControls();
        controls.setTimeLimit(0);
        controls.setCountLimit(0);
        controls.setSearchScope(SearchControls.SUBTREE_SCOPE);
        String filter = String.format("(&(objectClass=%s))", ldapPersonClass);
        return ldapTemplate.search("", filter,controls ,userContextMapper, proc);
    }

    public List<LdapUser> getAllRemoved(String lastUpdateDate) {
        log.debug("Get all removed for time: {}" , lastUpdateDate);
        DeleteSyncContextProcesor proc = new DeleteSyncContextProcesor();
        PagedResultsDirContextProcessor page = new PagedResultsDirContextProcessor(1000, pageCookie);
        AggregateDirContextProcessor agr = new AggregateDirContextProcessor();
        agr.addDirContextProcessor(proc);
        agr.addDirContextProcessor(page);
        
        SearchControls controls = new SearchControls();
        controls.setCountLimit(0);
        controls.setTimeLimit(0);
        controls.setSearchScope(SearchControls.SUBTREE_SCOPE);
        if(!lastUpdateDate.isEmpty()) lastUpdateDate = String.format("(whenChanged>=%s)",lastUpdateDate);
        String filter = String.format("(&(objectClass=%s)%s)", ldapPersonClass, lastUpdateDate);
        List<LdapUser> result = ldapTemplate.search("cn=Deleted Objects", filter,controls ,deletedUserContextMapper, agr);
        pageCookie = result.isEmpty() ? null : page.getCookie();
        return result;
    }
    
    public List<LdapUser> findByPesel(String pesel) {
        final SearchControls searchControls = new SearchControls();
        searchControls.setSearchScope(SearchControls.SUBTREE_SCOPE);
        searchControls.setTimeLimit(0);
        searchControls.setCountLimit(0);
        String filter = String.format("(&(objectclass=%s)(%s=%s))", ldapPersonClass,peselFiledName,pesel);
        return ldapTemplate.search("",filter, searchControls, userContextMapper);
    }
    
    public void updateUser(LdapUser user)  {
        DirContextAdapter contextData = new DirContextAdapter(user.getDn());
        userContextMapper.setupOperatorId(user, contextData);
        ModificationItem[] items = contextData.getModificationItems();
        ldapTemplate.modifyAttributes(user.getDn(), items);
    }

}
