package com.inteca.bnmt.profile;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.inteca.bnmt.service.OrganizationUnitService;

@Component
public class ProfileSync {
    
    private static final Logger log = LoggerFactory.getLogger(ProfileSync.class);
    
    @Autowired
    private Listener client;

    @Scheduled(fixedRate=10000)
    public void reportCurrentTime() {
        log.info("Call Profile");
        client.receive();
    }

}
