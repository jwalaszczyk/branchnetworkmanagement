package com.inteca.bnmt.service;

import com.inteca.bnmt.dto.PrimarySalesPointUpdateCriteria;
import com.inteca.bnmt.dto.PrimarySalesPointUpdateResult;
import com.inteca.bnmt.dto.Warning;
import com.inteca.bnmt.entity.AbstractAdvisor;
import com.inteca.bnmt.entity.Address;
import com.inteca.bnmt.entity.Advisor;
import com.inteca.bnmt.entity.OrganizationUnit;
import com.inteca.bnmt.exception.business.DuplicatedOperatorIdException;
import com.inteca.bnmt.repository.AdvisorRepository;
import com.inteca.bnmt.repository.OrganizationUnitRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.text.MessageFormat;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class UpdateAdvisorPrimarySalesPointService {
    private static final Logger log = LoggerFactory.getLogger(UpdateAdvisorPrimarySalesPointService.class);

    @Autowired
    private AdvisorRepository advisorRepository;

    @Autowired
    private OrganizationUnitRepository organizationUnitRepository;

    @Transactional
    public PrimarySalesPointUpdateResult updateAdvisorPrimarySalesPoint(List<PrimarySalesPointUpdateCriteria> criteria) {
        log.info("updateAdvisorPrimarySalesPoint - updating {} advisors", criteria.size());
        PrimarySalesPointUpdateResult updateResult = new PrimarySalesPointUpdateResult();
        Map<Integer, Advisor> operatorIdAdvisorMap = getOperatorIdAdvisorMap(criteria, updateResult);
        log.info("updateAdvisorPrimarySalesPoint - found {} advisors in db", operatorIdAdvisorMap.size());

        for (PrimarySalesPointUpdateCriteria criterion : criteria) {
            log.info("updateAdvisorPrimarySalesPoint - updating advisor's operatorId: {} for salesPoint's unitId {}",
                criterion.getOperatorId(), criterion.getUnitId());
            if (criterion.getOperatorId() != null && criterion.getUnitId() != null) {
                updateAdvisorPrimarySalesPoint(criterion, operatorIdAdvisorMap, updateResult);
            } else {
                log.warn("updateAdvisorPrimarySalesPoint - operatorId: {} unitId: {}. One or more of these fields are null, but in contract are required. Translation error?",
                    criterion.getOperatorId(), criterion.getUnitId());
            }
        }

        if (updateResult.getStatus()) {
            advisorRepository.save(operatorIdAdvisorMap.values());
        }
        return updateResult;
    }

    private Map<Integer, Advisor> getOperatorIdAdvisorMap(List<PrimarySalesPointUpdateCriteria> criteria, PrimarySalesPointUpdateResult updateResult) {
        Set<Integer> operatorIds = criteria.stream().map(PrimarySalesPointUpdateCriteria::getOperatorId).collect(Collectors.toSet());
        if (operatorIds.size() != criteria.size()) {
            throwDuplicatedOperatorIdError(updateResult, criteria);
        }
        Set<Advisor> advisors = advisorRepository.findByOperatorIdIn(operatorIds);
        return advisors.stream().collect(Collectors.toMap(AbstractAdvisor::getOperatorId, Function.identity()));
    }

    private void updateAdvisorPrimarySalesPoint(PrimarySalesPointUpdateCriteria criterion,
        Map<Integer, Advisor> operatorIdAdvisorMap, PrimarySalesPointUpdateResult updateResult) {
        Integer operatorId = criterion.getOperatorId();
        String unitId = criterion.getUnitId();
        if (operatorIdAdvisorMap.containsKey(operatorId)) {
            Advisor advisor = operatorIdAdvisorMap.get(operatorId);
            OrganizationUnit organizationUnitForCriterion = advisor.getOrganizationUnit().stream().filter(organizationUnit ->
                organizationUnit.getUnitId() != null && organizationUnit.getUnitId().equals(unitId))
                .findFirst().orElse(null);

            if (organizationUnitForCriterion != null) {
                log.info("updateAdvisorPrimarySalesPoint - both advisor and organizationUnit is present. updating");
                advisor.setPrimary(organizationUnitForCriterion); // assign new primary salesPoint
                updateResult.setStatus(true);
            } else {
                operatorIdAdvisorMap.remove(operatorId);
                appendNoOrganizationUnitWarning(updateResult, unitId, advisor);
            }
        } else {
            operatorIdAdvisorMap.remove(operatorId);
            appendNoAdvisorWarning(updateResult, operatorId);
        }
    }

    private void throwDuplicatedOperatorIdError(PrimarySalesPointUpdateResult updateResult, List<PrimarySalesPointUpdateCriteria> criteria) {
        HashSet<Integer> allOperatorIds = new HashSet<>();
        Set<String> duplicatedOperatorIds = criteria.stream()
            .map(PrimarySalesPointUpdateCriteria::getOperatorId)
            .filter(n -> !allOperatorIds.add(n))
            .map(Object::toString)
            .collect(Collectors.toSet()); // find duplicated operatorsIds with complexity O(n)
        log.error("updateAdvisorPrimarySalesPoint - found duplicated operatorId in request: duplicated operatorIds are: [{}]", String.join(", ", duplicatedOperatorIds));
        throw new DuplicatedOperatorIdException(duplicatedOperatorIds);
    }

    private void appendNoAdvisorWarning(PrimarySalesPointUpdateResult updateResult, Integer operatorId) {
        log.warn("updateAdvisorPrimarySalesPoint - advisor with operatorId: {} is not present in db", operatorId);
        updateResult.getWarningList().add(createWarningClass1("Wskazany doradca nie istnieje w systemie",
            MessageFormat.format("Wskazany doradca ({0}) nie istnieje w systemie", operatorId), "OperatorId", operatorId.toString()));
    }

    private void appendNoOrganizationUnitWarning(PrimarySalesPointUpdateResult updateResult, String unitId, Advisor advisor) {
        OrganizationUnit organizationUnit = organizationUnitRepository.findByUnitId(unitId);
        if (organizationUnit != null) {
            Address address = organizationUnit.getAddress() != null ? organizationUnit.getAddress() : new Address();
            log.warn("updateAdvisorPrimarySalesPoint - organizationUnit is not in relation with advisor. OperatorId: {}, Db Id: {}, UnitId: {}",
                advisor.getOperatorId(), advisor.getId(), unitId);
            updateResult.getWarningList().add(createWarningClass1("Brak relacji między doradcą i placówką",
                MessageFormat.format("Brak relacji między wskazanym doradcą ({0} {1} {2}) i placówką ({3} {4} {5})", advisor.getFirstName(), advisor.getSurname(),
                    advisor.getOperatorId(), unitId, address.getCity(), address.getStreet()), "OperatorId, UnitId", advisor.getOperatorId() + ", " + unitId));
        } else {
            log.warn("updateAdvisorPrimarySalesPoint - organizationUnit with unitId: {} is not present in db", unitId);
            updateResult.getWarningList().add(createWarningClass1("Wskazana placówka nie istnieje w systemie",
                MessageFormat.format("Wskazana placówka ({0}) nie istnieje w systemie", unitId), "UnitId", unitId));
        }
    }

    private Warning createWarningClass1(String attribute, String description, String element, String value) {
        return new Warning(attribute, "1", "1", description, element, value);
    }
}
