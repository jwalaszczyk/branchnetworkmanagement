package com.inteca.bnmt.exception;

import net.sf.mmm.util.nls.api.NlsBundle;
import net.sf.mmm.util.nls.api.NlsBundleMessage;
import net.sf.mmm.util.nls.api.NlsMessage;

import javax.inject.Named;

/**
 * Interface for storing messages for exceptions
 *
 * @author bartek
 */
public interface NlsBundleApplicationRoot extends NlsBundle {

    @NlsBundleMessage("Found duplicated operatorId in request: duplicated operatorIds are: [{joinedOperatorIds}]")
    NlsMessage duplicatedOperatorId(@Named("joinedOperatorIds") String joinedOperatorIds);
}
