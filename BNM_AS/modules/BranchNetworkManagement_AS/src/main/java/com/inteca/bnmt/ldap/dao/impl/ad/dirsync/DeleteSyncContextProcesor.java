package com.inteca.bnmt.ldap.dao.impl.ad.dirsync;

import javax.naming.NamingException;
import javax.naming.directory.DirContext;
import javax.naming.ldap.Control;

import org.springframework.ldap.control.AbstractRequestControlDirContextProcessor;

public class DeleteSyncContextProcesor extends AbstractRequestControlDirContextProcessor {
    
    @Override
    public Control createRequestControl() {
        return new DeleteSyncControl();
    }


    @Override
    public void postProcess(DirContext ctx) throws NamingException {
//        not used 
    }
    
}
