package com.inteca.bnmt.ldap.domain;

import java.util.HashSet;
import java.util.Set;

public class LdapUser {

    private String name;
    private String pesel;
    private String login;
    private String surname;
    private String dn;
    private boolean loginDisabled;
    private boolean lockedByIntruder;
    private int userAccountControl;
    private Set<String> memberGroupSet = new HashSet<>();
    private boolean controlSetup = false;
    private String source;
    private String objectGUID;
    private String operatorId;
    private String mail;
    private String stationaryPhone;
    private String mobile;
    private Set<String> organizationUnitcodes = new HashSet<>();
    private String lastModificationDate;

    
    
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getPesel() {
        return pesel;
    }
    public void setPesel(String pesel) {
        this.pesel = pesel;
    }
    public String getLogin() {
        return login;
    }
    public void setLogin(String login) {
        this.login = login;
    }
    public String getSurname() {
        return surname;
    }
    public void setSurname(String surname) {
        this.surname = surname;
    }
    public boolean isLoginDisabled() {
        return loginDisabled;
    }
    public void setLoginDisabled(boolean loginDisabled) {
        this.loginDisabled = loginDisabled;
    }
    public boolean isLockedByIntruder() {
        return lockedByIntruder;
    }
    public void setLockedByIntruder(boolean lockedByIntruder) {
        this.lockedByIntruder = lockedByIntruder;
    }
    public int getUserAccountControl() {
        return userAccountControl;
    }
    public void setUserAccountControl(int userAccountControl) {
        this.userAccountControl = userAccountControl;
    }
    
    public Set<String> getMemberGroupSet() {
        return memberGroupSet;
    }
    
    public void setMemberGroupSet(Set<String> memberGroupSet) {
        this.memberGroupSet = memberGroupSet;
    }
    
    public void setDn(String dn) {
        this.dn = dn;
    }
    
    public String getDn() {
        return dn;
    }
    public boolean isControlSet() {
        return controlSetup ;
    }
    
    @Override
    public String toString() {
        return dn;
    }

    public void setSource(String src) {
        source= src;
    }
    
    public String getSource() {
        return source;
    }
    
    public String getObjectGUID() {
        return objectGUID;
    }
    
    public void setObjectGUID(String objectGUID) {
        this.objectGUID = objectGUID;
    }
    
    public String getOperatorId() {
        return operatorId;
    }
    
    public void setOperatorId(String operatorId) {
        this.operatorId = operatorId;
    }
    public void setMail(String mail) {
        this.mail = mail;
    }
    
    public String getMail() {
        return mail;
    }
    
    public String getStationaryPhone() {
        return stationaryPhone;
    }
    
    public void setStationaryPhone(String stationaryPhone) {
        this.stationaryPhone = stationaryPhone;
    }
    
    public String getMobile() {
        return mobile;
    }
    
    public void setMobile(String mobile) {
        this.mobile = mobile;
    }
    
    public Set<String> getOrganizationUnitcodes() {
        return organizationUnitcodes;
    }
    
    public void setOrganizationUnitcodes(Set<String> organizationUnitcodes) {
        this.organizationUnitcodes = organizationUnitcodes;
    }
    public void setLastModificationDate(String lastModificationDate) {
        this.lastModificationDate=lastModificationDate;
    }
    
    public String getLastModificationDate() {
        return lastModificationDate;
    }
    
}
