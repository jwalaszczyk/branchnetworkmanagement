package com.inteca.bnmt.facade;

import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.ws.BindingType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.inteca.bnmt.exception.ApplicationException;
import com.inteca.bnmt.mediator.BranchNetworkManagementASMediator;

import pl.credit_agricole.soap.bnmt._1._2015._09._001.BranchNetworkManagementASPortType;
import pl.credit_agricole.soap.bnmt._1._2015._09._001.FaultMessage;

/**
 * @author bw
 */
@WebService(serviceName = "BranchNetworkManagementAS",
    portName = "BranchNetworkManagementAS_port",
    targetNamespace = "http://soap.credit-agricole.pl/bnmt/1/2015/09/001",
    wsdlLocation = "wsdl/BranchNetworkManagementAS.wsdl")
@SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.BARE)
@BindingType("http://www.w3.org/2003/05/soap/bindings/HTTP/")
public class BranchNetworkManagementASFacade implements BranchNetworkManagementASPortType{
    private static final Logger log = LoggerFactory.getLogger(BranchNetworkManagementASFacade.class);

    @Autowired
    private BranchNetworkManagementASMediator branchNetworkManagementASMediator;

    @Override
    public pl.credit_agricole.schemas.bnmt._103._2015._08._001.Document querySalesPoint(
        pl.credit_agricole.schemas.bnmt._102._2015._08._001.Document requestBody) throws FaultMessage {
        log.info("START - facade - querySalesPoint ");
        try {
            pl.credit_agricole.schemas.bnmt._103._2015._08._001.Document responseBody = new pl.credit_agricole.schemas.bnmt._103._2015._08._001.Document();
            responseBody.setQrySalesPtRspn(branchNetworkManagementASMediator.querySalesPoint(requestBody.getQrySalesPtReq()));
            log.info("STOP - facade - querySalesPoint ");
            return responseBody;
        } catch (ApplicationException ex) {
            log.error(ex.getMessage(), ex);
            throw ex.getFaultMessage();
        }
    }

    @Override
    public pl.credit_agricole.schemas.bnmt._105._2018._08._001.Document queryAdvisor(
        pl.credit_agricole.schemas.bnmt._104._2018._08._001.Document requestBody) throws FaultMessage {
        log.info("START - facade - queryAdvisor ");
        try {
            pl.credit_agricole.schemas.bnmt._105._2018._08._001.Document responseBody = new pl.credit_agricole.schemas.bnmt._105._2018._08._001.Document();
            responseBody.setQryAdvsrRspn(branchNetworkManagementASMediator.queryAdvisor(requestBody.getQryAdvsrReq()));
            log.info("STOP - facade - queryAdvisor ");
            return responseBody;
        } catch (ApplicationException ex) {
            log.error(ex.getMessage(), ex);
            throw ex.getFaultMessage();
        }

    }

    @Override
    public pl.credit_agricole.schemas.bnmt._107._2018._08._001.Document updateAdvisorPrimarySalesPoint(
        pl.credit_agricole.schemas.bnmt._106._2018._08._001.Document requestBody) throws FaultMessage {
        log.info("START - facade - updateAdvisorPrimarySalesPoint ");
        try {
            pl.credit_agricole.schemas.bnmt._107._2018._08._001.Document responseBody = new pl.credit_agricole.schemas.bnmt._107._2018._08._001.Document();
            responseBody.setUpdAdvsrPmrySalesPtRspn(branchNetworkManagementASMediator.updateAdvisorPrimarySalesPoint(requestBody.getUpdAdvsrPmrySalesPtReq()));
            log.info("STOP - facade - updateAdvisorPrimarySalesPoint ");
            return responseBody;
        } catch (ApplicationException ex) {
            log.error(ex.getMessage(), ex);
            throw ex.getFaultMessage();
        }
    }
}
