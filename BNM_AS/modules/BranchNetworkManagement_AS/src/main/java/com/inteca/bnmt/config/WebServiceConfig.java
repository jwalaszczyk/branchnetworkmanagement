package com.inteca.bnmt.config;

import com.inteca.bnmt.facade.BranchNetworkManagementASFacade;
import org.apache.cxf.Bus;
import org.apache.cxf.bus.spring.SpringBus;
import org.apache.cxf.jaxws.EndpointImpl;
import org.apache.cxf.transport.servlet.CXFServlet;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.xml.ws.Endpoint;

/**
 * @author bw
 */
@Configuration
public class WebServiceConfig {

    @Bean
    public ServletRegistrationBean dispatcherServlet() {
        return new ServletRegistrationBean(new CXFServlet(), "/soap-api/*");
    }

    @Bean(name = Bus.DEFAULT_BUS_ID)
    public SpringBus springBus() {
        return new SpringBus();
    }

    @Bean
    public BranchNetworkManagementASFacade branchNetworkManagementASFacade() {
        return new BranchNetworkManagementASFacade();
    }

    @Bean
    public Endpoint branchNetworkManagementASEndpoint(SpringBus springBus, BranchNetworkManagementASFacade branchNetworkManagementASFacade) {
        EndpointImpl endpoint = new EndpointImpl(springBus, branchNetworkManagementASFacade);
        endpoint.publish("/branchNetworkManagementAS");
        return endpoint;
    }

}
