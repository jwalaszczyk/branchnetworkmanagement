package com.inteca.bnmt.ldap.dao;

import java.util.List;

import com.inteca.bnmt.ldap.domain.LdapUser;

public interface ADUserDao {

    List<LdapUser> getAllChanged() ;
    List<LdapUser> getAllRemoved(String lastUpdateDate);
    List<LdapUser> findByPesel(String pesel);
    void updateOperatorId(LdapUser user);
}
