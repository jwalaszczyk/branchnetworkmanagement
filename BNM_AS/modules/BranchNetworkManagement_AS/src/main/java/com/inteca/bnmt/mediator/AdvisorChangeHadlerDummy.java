package com.inteca.bnmt.mediator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class AdvisorChangeHadlerDummy implements AdvisorChangeHandler {
    private static final Logger log = LoggerFactory.getLogger(AdvisorChangeHadlerDummy.class);

    @Override
    public void notifyUserRemoved(com.inteca.bnmt.entity.Advisor user) {
        log.info("[REMOVE]: {}" ,user);
    }

    @Override
    public void notifyUserChanged(com.inteca.bnmt.entity.Advisor user) {
        log.info("[CHANGE]: {}" ,user);
    }

}
