package com.inteca.bnmt.ldap.dao.impl.ad.dirsync;

import javax.naming.ldap.Control;

public class DeleteSyncControl implements Control {

    private static final long serialVersionUID = -930993758829518418L;

    @Override
    public String getID() {
        return SyncFlags.LDAP_SERVER_SHOW_DELETED_OID;
    }

    @Override
    public boolean isCritical() {
        return true;
    }

    @Override
    public byte[] getEncodedValue() {
        return new byte[] {};
    }
}