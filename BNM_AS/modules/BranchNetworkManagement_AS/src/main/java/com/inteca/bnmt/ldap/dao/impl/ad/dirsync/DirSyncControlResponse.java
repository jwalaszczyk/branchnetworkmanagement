package com.inteca.bnmt.ldap.dao.impl.ad.dirsync;

import javax.naming.ldap.BasicControl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sun.jndi.ldap.Ber;
import com.sun.jndi.ldap.BerDecoder;

@SuppressWarnings("restriction")
public class DirSyncControlResponse extends BasicControl {
    
    private static final Logger log = LoggerFactory.getLogger(DirSyncControlResponse.class);

    private static final long serialVersionUID = 1L;
    private byte[] cookie;
    public static final String OID = SyncFlags.LDAP_SERVER_DIRSYNC_OID;


    public DirSyncControlResponse(String id, boolean criticality, byte[] value) {
        super(id, criticality, value);

        // decode value
        BerDecoder ber = new BerDecoder(value, 0, value.length);
        try {
            ber.parseSeq(null);
            ber.parseInt(); // flags 
            ber.parseInt(); // response buff size
            cookie = ber.parseOctetString(Ber.ASN_OCTET_STR, null);
        } catch (Exception e) {
            log.error("Cannot parse AD response message",e);
        }
    }

    public byte[] getCookie() {
        return cookie.length== 0 ? null : cookie;
    }
}
