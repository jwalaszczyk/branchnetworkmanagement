package com.inteca.bnmt.mediator;

import com.inteca.bnmt.entity.Advisor;
import com.inteca.bnmt.ldap.domain.LdapUser;
import com.inteca.bnmt.repository.AdvisorRepository;
import com.inteca.bnmt.service.AdvisorService;
import com.inteca.bnmt.service.OperatorIdProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class UserChangeHandler implements FederationChangeHadler {

    private static final Logger log = LoggerFactory.getLogger(UserChangeHandler.class);

    @Autowired
    private OperatorIdProvider operatorIdProvider;

    @Autowired
    AdvisorRepository advisorRepository;

    @Autowired
    private AdvisorChangeHandler handler;

    @Autowired
    AdvisorService advisorService;


    @Override
    public void onUserRemove(LdapUser user) {
        log.info("[REMOVE]: {}", user);
        try {
            processRemove(user);
        } catch (Exception e) {
            log.error("Cannot delete user {}", user.getLogin(), e);
        }
    }

    private void processRemove(LdapUser user) {
        Advisor dbUser = advisorRepository.findByObjectGuid(user.getObjectGUID());
        if (dbUser == null) {
            log.warn("Cannot find user: {} in local DB", user.getLogin());
            return;
        }
        advisorRepository.delete(dbUser);
        handler.notifyUserRemoved(dbUser);
    }

    @Override
    public void onUserChange(LdapUser user) {
        log.info("[CHANGED]: {}", user);
        try {
            processChange(user);
        } catch (Exception e) {
            log.error("Cannot change user {}", user.getLogin(), e);
        }
    }

    private void processChange(LdapUser user) {

        operatorIdProvider.resolve(user);

        /* Create or Update advisor */
        Advisor advisor = advisorService.createUpdate(user);

        if (user.getOperatorId() == null) {
            user.setOperatorId(String.valueOf(advisor.getOperatorId()));
            operatorIdProvider.persist(user);
        }
        handler.notifyUserChanged(advisor);
    }


}
