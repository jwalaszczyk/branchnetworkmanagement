package com.inteca.bnmt.ldap.sheduler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.inteca.bnmt.mediator.SyncLdapMediator;

@Component
public class UserFederationSyncSheduler {
    
    @Autowired
    SyncLdapMediator syncManager;
    
    @Scheduled(cron = "${ldap.sync.cron}")
    public void reportCurrentTime() {
        syncManager.syncAll();
    }
}
