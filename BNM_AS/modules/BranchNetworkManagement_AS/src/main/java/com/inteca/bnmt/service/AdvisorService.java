package com.inteca.bnmt.service;

import com.inteca.bnmt.dto.AdvisorsSearchCriteria;
import com.inteca.bnmt.entity.Advisor;
import com.inteca.bnmt.entity.FunctionalRole;
import com.inteca.bnmt.entity.OrganizationUnit;
import com.inteca.bnmt.ldap.domain.LdapUser;
import com.inteca.bnmt.repository.AdvisorRepository;
import com.inteca.bnmt.translation.LdapToBNMUserTranslator;
import com.inteca.bnmt.utils.GroupResolveUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service responsible for create and update Advisor in Db.
 *
 * @author ddedek
 * 23.08.2018
 */

@Service
public class AdvisorService {
    private static final Logger log = LoggerFactory.getLogger(AdvisorService.class);

    @Autowired
    private AdvisorRepository advisorRepository;

    @Autowired
    LdapToBNMUserTranslator ldapToBNMUserTranslator;

    @Autowired
    AdvisorGroupResolver advisorGroupResolver;

    @Transactional
    public Advisor createUpdate(LdapUser ldapUser) {
        log.info("START - createUpdateAdvisor");

        Advisor advisor = findAdvisorInDb(ldapUser.getObjectGUID());
        /* Translate LdapUser to Advisor */
        ldapToBNMUserTranslator.toAdvisor(ldapUser, advisor);

        /* Cut Ldap Groups */
        ldapUser = GroupResolveUtils.cutLdapOrganizationUnitcodes(ldapUser);
        /* Add new LdapGroups to AdGroups */
        advisor.getAdGroup().clear();
        advisor.getAdGroup().addAll(advisorGroupResolver.adGroupsResolver(ldapUser));

        /* Persist Advisor.OrganizationUnits */
        advisor = advisorGroupResolver.persistOrganizationUnits(advisor);

        /* Persist primary and Advisor.OrganizationUnits */
        advisor = advisorGroupResolver.checkPrimaryOrganizationUnit(advisor);

        advisor = advisorRepository.saveAndFlush(advisor);
        log.info("Advisor {} updated successfully!", advisor.getLogin());
        return advisor;

    }


    private Advisor findAdvisorInDb(String objectGUID) {
        log.info("Looking for advisor with specified objectGUID... {}", objectGUID);
        Advisor advisor = advisorRepository.findByObjectGuidWithOrganizationUnitAndAdGroup(objectGUID);
        if (advisor == null) {
            log.info("Advisor with objectGUILD {}  not found, create new", objectGUID);
            advisor = new Advisor();

            advisor.setOperatorId(advisorRepository.findNextNumberForOperatorId());

        }
        return advisor;
    }


    public List<Advisor> queryAdvisor(AdvisorsSearchCriteria criteria) {
        List<Advisor> resuts = new ArrayList<>();
        for(OrganizationUnit ou : criteria.getOrganizationUnits()) {
            List<Integer> roleIds = criteria.getFunctionalRoles().stream().map(FunctionalRole::getRoleId).collect(Collectors.toList());
            resuts.addAll(querryAdvisor(ou.getUnitIdNumber(), roleIds ));
        }
        return resuts;
    }


    private Collection<? extends Advisor> querryAdvisor(int unitIdNumber, List<Integer> roleIds) {
        if(roleIds.isEmpty()) return advisorRepository.findByOrganizationUnitUnitIdNumber(unitIdNumber);
        return advisorRepository.findByOrganizationUnitUnitIdNumberAndFunctionalRoleRoleIdIn(unitIdNumber,roleIds);
    }


}
