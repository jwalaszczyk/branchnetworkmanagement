package com.inteca.bnmt.utils;

import com.inteca.bnmt.entity.AdGroup;
import com.inteca.bnmt.ldap.domain.LdapUser;

import java.util.HashSet;
import java.util.Set;

/**
 * @author ddedek
 * 23.08.2018
 */

public class GroupResolveUtils {

    public static LdapUser cutLdapOrganizationUnitcodes(LdapUser ldapUser) {

        Set<String> stringHashSet = new HashSet<>();

        for (String s : ldapUser.getOrganizationUnitcodes()) {
            stringHashSet.add(s.replaceFirst("^.*\\D", ""));
        }

        ldapUser.setOrganizationUnitcodes(stringHashSet);

        return ldapUser;
    }



}
