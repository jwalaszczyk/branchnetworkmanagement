package com.inteca.bnmt.dto;

public class PrimarySalesPointUpdateCriteria {

    private Integer operatorId;
    private String unitId;

    public Integer getOperatorId() {
        return operatorId;
    }

    public void setOperatorId(Integer operatorId) {
        this.operatorId = operatorId;
    }

    public String getUnitId() {
        return unitId;
    }

    public void setUnitId(String unitId) {
        this.unitId = unitId;
    }
}
