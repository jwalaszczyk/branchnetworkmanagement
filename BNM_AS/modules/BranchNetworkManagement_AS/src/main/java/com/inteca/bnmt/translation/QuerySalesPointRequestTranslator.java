package com.inteca.bnmt.translation;

import com.inteca.bnmt.dto.OrganizationUnitSearchCriteria;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import pl.credit_agricole.schemas.bnmt._102._2015._08._001.QuerySalesPointRequest;

@Mapper
public interface QuerySalesPointRequestTranslator {

    @Mappings({
        @Mapping(source = "advsr.oprtrId", target = "operatorId"),
        @Mapping(source = "advsr.lgn", target = "login"),
        @Mapping(source = "salesPt.id", target = "unitId"),
        @Mapping(source = "salesPt.brcd", target = "brcd")
    })
    OrganizationUnitSearchCriteria translate(QuerySalesPointRequest request);
}
