package com.inteca.bnmt.ldap.dao.impl.ad.mappers;

import java.util.HashSet;
import java.util.Set;

import org.springframework.ldap.core.DirContextAdapter;
import org.springframework.ldap.core.DirContextOperations;
import org.springframework.ldap.core.support.AbstractContextMapper;

import com.inteca.bnmt.ldap.dao.impl.ad.AccountControlFlags;
import com.inteca.bnmt.ldap.dao.impl.utils.LDAPutils;
import com.inteca.bnmt.ldap.domain.LdapUser;

public class ADUserContextMapper extends AbstractContextMapper<LdapUser> {
	
    private String peselFiledName;
    private String operatorIdFiledName;
    
    public ADUserContextMapper(String peselFiledName, String operatorIdFiledName) {
        this.peselFiledName=peselFiledName;
        this.operatorIdFiledName=operatorIdFiledName;
    }
    
	@Override
    public LdapUser doMapFromContext(DirContextOperations context) {
	    LdapUser user = new LdapUser();
        
        for(String atr : new String[]{"cn", "uid"} ) {
        	String val = getAttribute(atr, context);
        	if(val != null) {
        		user.setLogin(val);
        		break;
        	}
        }

        user.setObjectGUID((context.getStringAttribute("objectguid")));

        if(user.getLogin()==null ) {
        	String dn = context.getDn().getSuffix(context.getDn().size()-1).toString();
            user.setLogin(LDAPutils.fromLdap(dn));
        }
        user.setName(context.getStringAttribute("givenName"));
        user.setSurname(context.getStringAttribute("sn"));
        user.setPesel(context.getStringAttribute(peselFiledName));
        
        //WS-155078 - in CABP user is locked if loginDisabled is true or lockedByIntruder is true
        String userAccountControlStr = context.getStringAttribute("userAccountControl");
        String lockoutString = context.getStringAttribute("lockoutTime");
        long lockoutTime = Long.parseLong(lockoutString!=null ? lockoutString : "0");
        if(userAccountControlStr!=null && !userAccountControlStr.isEmpty()) {
			int userAccountControl= Integer.parseInt(userAccountControlStr);
			user.setUserAccountControl(userAccountControl);
			user.setLoginDisabled((userAccountControl & AccountControlFlags.ACCOUNTDISABLE)==AccountControlFlags.ACCOUNTDISABLE);
			user.setLockedByIntruder(lockoutTime>0);
        }
        
        user.setMail(context.getStringAttribute("mail"));
        user.setMobile(context.getStringAttribute("mobile"));
        user.setStationaryPhone(context.getStringAttribute("telephoneNumber"));
        user.setOperatorId(context.getStringAttribute(operatorIdFiledName));

        if (context.getStringAttribute("whenChanged") != null)
            user.setLastModificationDate(context.getStringAttribute("whenChanged"));
        else user.setLastModificationDate(context.getStringAttribute("whenCreated"));

        Set<String> memberGroupSet = context.getAttributeSortedStringSet("memberOf");
		user.setMemberGroupSet(memberGroupSet == null ? new HashSet<>(): memberGroupSet);
		
//		TODO resolve oragnization units and role
		
        user.setDn(context.getDn().toString());
        return user;
    }
	
    private String getAttribute(String atr, DirContextOperations context) {
    	String val = context.getStringAttribute(atr);
    	if(val != null && val.isEmpty()) return null;
		return val;
	}

    public void setupOperatorId(LdapUser user, DirContextAdapter context) {
        context.setUpdateMode(true);
        context.setAttributeValue(operatorIdFiledName, user.getOperatorId());
    } 

}
