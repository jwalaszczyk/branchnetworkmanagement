package com.inteca.bnmt.service;

import com.inteca.bnmt.entity.AdGroup;
import com.inteca.bnmt.entity.Advisor;
import com.inteca.bnmt.entity.OrganizationUnit;
import com.inteca.bnmt.ldap.domain.LdapUser;
import com.inteca.bnmt.repository.AdGroupRepository;
import com.inteca.bnmt.repository.AdvisorRepository;
import com.inteca.bnmt.repository.OrganizationUnitRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

/**
 * @author ddedek
 * 10.08.2018
 */

@Service
public class AdvisorGroupResolver {
    private static final Logger log = LoggerFactory.getLogger(AdvisorGroupResolver.class);

    @Autowired
    OrganizationUnitRepository organizationUnitRepository;

    @Autowired
    AdvisorRepository advisorRepository;

    @Autowired
    AdGroupRepository adGroupRepository;


    Advisor persistOrganizationUnits(Advisor advisor) {

        advisor.getOrganizationUnit().clear();

        for (AdGroup a : advisor.getAdGroup()) {
            OrganizationUnit organizationUnitFromDataBase = getOrganizationUnitFromDataBase(a.getName());

            if (organizationUnitFromDataBase != null) {
                advisor.getOrganizationUnit().add(organizationUnitFromDataBase);
            }
        }

        return advisor;
    }

    Advisor checkPrimaryOrganizationUnit(Advisor advisor) {
        log.info("Looking if primary exist in Advisor.OrganizationUnits ...");

        if (advisor.getPrimary() != null) {

            for (OrganizationUnit a : advisor.getOrganizationUnit()) {

                if (advisor.getPrimary().getUnitId().equals(a.getUnitId())) {
                    log.info(String.format("Primary %s", advisor.getPrimary().getUnitId()));
                    return advisor;
                }

            }
            advisor.setPrimary(null);
            return advisor;
        }
        return advisor;
    }

    Set<AdGroup> adGroupsResolver(LdapUser ldapUser) {

        Set<AdGroup> adGroups = new HashSet<>();

        for (String s : ldapUser.getOrganizationUnitcodes()) {

            AdGroup adGroup = getAdGroupFromDataBase(s);

            if (adGroup == null || adGroup.getId() == null) {
                adGroup = new AdGroup();
                adGroup.setName(s);
            }
            adGroups.add(adGroup);
        }

        return adGroups;
    }

    private OrganizationUnit getOrganizationUnitFromDataBase(String s) {

        try {
            OrganizationUnit organizationUnitByUnitId = organizationUnitRepository.findByUnitId(s);
            if (organizationUnitByUnitId != null) {
                log.info(String.format("OrganizationUnit with unitId: %s found successfully", s));
                return organizationUnitByUnitId;
            } else
                return null;

        } catch (Exception e) {
            log.warn(String.format("OrganizationUnit with unitId: %s not found", s));
            return null;
        }
    }


    private AdGroup getAdGroupFromDataBase(String s) {

        AdGroup adGroupByName = adGroupRepository.findByName(s);

        if (adGroupByName != null) {
            log.info(String.format("AdGroup with name : %s found successfully", s));
            return adGroupByName;
        }

        log.info(String.format("AdGroup with name: %s not found", s));
        adGroupByName = new AdGroup();
        adGroupByName.setName(s);

        return adGroupByName;

    }
}


