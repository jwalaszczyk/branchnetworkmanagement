package com.inteca.bnmt.ldap.dao.impl.utils;

/**
 * Created by Upir on 2016-11-30.
 */
public class LDAPutils {


	public static String fromLdap(String str) {
		if(str.isEmpty()) return "";
		String[] elemList = str.split(",");
		String temp = "";
		for (int i = 0; i < elemList.length - 1; i++) {
			temp += elemList[i].split("=")[1] + ".";
		}
		temp += elemList[elemList.length - 1].split("=")[1];
		return temp;
	}

	public static String toLdap(String str) {
		String[] elemList = str.split("\\.");
		String temp = "";
		for (int i = 0; i < elemList.length - 1; i++) {
			temp += "ou=" + elemList[i] + ",";
		}
		temp += "ou=" + elemList[elemList.length - 1];
		return temp;
	}

	public static String toLdapADGroup(String str)	{
		return toLdap(str).replaceFirst("ou=", "cn=");
	}
	
	
}
