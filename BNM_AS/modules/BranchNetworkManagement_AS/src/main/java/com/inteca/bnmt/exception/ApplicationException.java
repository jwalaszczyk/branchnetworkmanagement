package com.inteca.bnmt.exception;

import net.sf.mmm.util.exception.api.NlsRuntimeException;
import net.sf.mmm.util.nls.api.NlsMessage;
import pl.credit_agricole.schemas.bnmt._101._2015._08._001.*;

import java.util.Collections;

/**
 * General class for generating FaultMessage both for business and technical
 * exceptions. It is responsible for filling whole FaultMessage object except
 * error reason and error code which have to be set by specific classes extended
 * ApplicationBusinessException or ApplicationTechnicalException classes.
 *
 * @author Agnieszka
 */
public abstract class ApplicationException extends NlsRuntimeException {

    private static final long serialVersionUID = 1L;
    private static final int MAX_ERR_DESC_LENGTH = 255;
    private static final int MAX_ERR_CODE_LENGTH = 4;
    private static final int MAX_ERR_RSN_SRC_LENGTH = 50;
    private static final String APP_NAME = "BranchNetworkManagement";

    /**
     * @param message the error {@link #getNlsMessage() message}.
     */
    public ApplicationException(NlsMessage message) {
        super(message);
    }

    /**
     * @param cause   the error {@link #getCause() cause}.
     * @param message the error {@link #getNlsMessage() message}.
     */
    public ApplicationException(Throwable cause, NlsMessage message) {
        super(cause, message);
    }

    public pl.credit_agricole.soap.bnmt._1._2015._09._001.FaultMessage getFaultMessage() {
        Document document = new Document();
        FaultMessage faultMessage = new FaultMessage();
        FaultType faultType = new FaultType();
        ErrorDetails details = new ErrorDetails();
        details.setErrDesc(Collections.singletonList(substringMessageNullSafe(getMessage(), MAX_ERR_DESC_LENGTH)));
        faultType.setDtls(details);
        faultType.setErrRsn(substringMessageNullSafe(getErrorReason(), MAX_ERR_RSN_SRC_LENGTH));
        faultType.setErrSrc(substringMessageNullSafe(APP_NAME, MAX_ERR_RSN_SRC_LENGTH));
        faultType.setErrCdVal(substringMessageNullSafe(getCode(), MAX_ERR_CODE_LENGTH));
        faultType.setErrTp(isTechnical() ? ErrorTypeCode.T : ErrorTypeCode.B);
        faultMessage.setFlt(faultType);
        document.setFltMsg(faultMessage);
        return new pl.credit_agricole.soap.bnmt._1._2015._09._001.FaultMessage(getMessage(), document, getCause());
    }

    protected abstract String getErrorReason();

    protected String substringMessageNullSafe(String value, int endIndex) {
        if (value == null || value.length() <= endIndex)
            return value;
        return value.substring(0, endIndex);
    }

    @Override
    public String getMessage() {

        StringBuilder buffer = new StringBuilder(getNlsMessage().getMessage());
        String code = getCode();
        if (!getClass().getSimpleName().equals(code)) {
            buffer.append(". Error code: ");
            buffer.append(code);
        }
        return buffer.toString();
    }

}
