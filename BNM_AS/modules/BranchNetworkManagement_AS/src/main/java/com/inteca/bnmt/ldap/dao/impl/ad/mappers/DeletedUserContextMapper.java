package com.inteca.bnmt.ldap.dao.impl.ad.mappers;

import org.springframework.ldap.core.DirContextOperations;
import org.springframework.ldap.core.support.AbstractContextMapper;

import com.inteca.bnmt.ldap.domain.LdapUser;

public class DeletedUserContextMapper extends AbstractContextMapper<LdapUser> {

    @Override
    protected LdapUser doMapFromContext(DirContextOperations ctx) {
        LdapUser user = new LdapUser();
        user.setObjectGUID((ctx.getStringAttribute("objectguid")));
        user.setLogin(ctx.getStringAttribute("cn"));
        return user;
    }

}
