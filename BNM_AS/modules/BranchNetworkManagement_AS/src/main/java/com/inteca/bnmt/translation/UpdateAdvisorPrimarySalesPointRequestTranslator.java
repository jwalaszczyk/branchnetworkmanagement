package com.inteca.bnmt.translation;

import com.inteca.bnmt.dto.PrimarySalesPointUpdateCriteria;
import com.inteca.bnmt.translation.util.ListWrapper;
import org.mapstruct.*;
import pl.credit_agricole.schemas.bnmt._106._2018._08._001.Advisor2;
import pl.credit_agricole.schemas.bnmt._106._2018._08._001.UpdateAdvisorPrimarySalesPointRequest;

import java.util.List;

@Mapper(nullValueMappingStrategy = NullValueMappingStrategy.RETURN_DEFAULT)
public abstract class UpdateAdvisorPrimarySalesPointRequestTranslator {

    public List<PrimarySalesPointUpdateCriteria> translate(UpdateAdvisorPrimarySalesPointRequest request) {
        return toPrimarySalesPointUpdateCriteriaList(request).getList();
    }

    @Mappings({
        @Mapping(source = "advsr", target = "list"),
    })
    public abstract ListWrapper<PrimarySalesPointUpdateCriteria> toPrimarySalesPointUpdateCriteriaList(UpdateAdvisorPrimarySalesPointRequest request);

    @Mappings({
        @Mapping(source = "oprtrId", target = "operatorId"),
        @Mapping(source = "pmrySalesPt.id", target = "unitId")
    })
    public abstract PrimarySalesPointUpdateCriteria toPrimarySalesPointUpdateCriteria(Advisor2 advisor2);
}
