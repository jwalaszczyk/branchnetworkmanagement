package com.inteca.bnmt.service;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.inteca.bnmt.entity.AdSyncData;
import com.inteca.bnmt.repository.AdSyncDataRepository;

/**
 *  Get date from db or generate current date.
 *  For pop date save recently returning date to db; 
 */
@Service
public class SyncDateResolver {
    
    private static final String KEY_EXT_LAST_UPDATE = "com.inteca.ldap.ad.ext.sync.deleted.update";
    private static final String KEY_INT_LAST_UPDATE = "com.inteca.ldap.ad.int.sync.deleted.update";

    @Autowired 
    private AdSyncDataRepository dataRepo;
    
    private DateTimeFormatter formatter = DateTimeFormatter.ofPattern("YYYYMMddHHmm00.0XXXX") ;
    
    private String externalLastUpdateDate = "";
    private String internalLastUpdateDate = "";
    
    @PostConstruct
    public void init() {
        String currentTime = ZonedDateTime.now().format(formatter);
        String extDate = dataRepo.findByKey(KEY_EXT_LAST_UPDATE);
        String intDate = dataRepo.findByKey(KEY_INT_LAST_UPDATE);
        externalLastUpdateDate = extDate==null ? currentTime: extDate;
        internalLastUpdateDate = intDate==null ? currentTime: intDate;
        
//        Initail save data
        if(extDate==null) dataRepo.save(getAdSyncData(KEY_EXT_LAST_UPDATE, externalLastUpdateDate));
        if(intDate==null) dataRepo.save(getAdSyncData(KEY_INT_LAST_UPDATE, internalLastUpdateDate));
    }

    public String popLastExternalDate() {
//        minus is for prevent missing some update during this method call
        String currentTime = ZonedDateTime.now().minusSeconds(10).format(formatter);
        String ret = externalLastUpdateDate;
        dataRepo.updateValueForKey(KEY_EXT_LAST_UPDATE, ret);
        externalLastUpdateDate=currentTime;
        return ret;
    }
    
    public String popLastInternalDate() {
//      minus is for prevent missing some update during this method call
        String currentTime = ZonedDateTime.now().minusSeconds(10).format(formatter);
        String ret = internalLastUpdateDate;
        dataRepo.updateValueForKey(KEY_INT_LAST_UPDATE, ret);
        internalLastUpdateDate=currentTime;
        return ret;
    }
    
    private AdSyncData getAdSyncData(String key, String value) {
        AdSyncData d = new AdSyncData();
        d.setKey(key);
        d.setValue(value);
        return d;
    }
}
