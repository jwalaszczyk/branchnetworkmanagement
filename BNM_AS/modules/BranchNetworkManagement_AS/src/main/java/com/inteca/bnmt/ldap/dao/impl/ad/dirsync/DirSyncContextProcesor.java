package com.inteca.bnmt.ldap.dao.impl.ad.dirsync;

import javax.naming.NamingException;
import javax.naming.directory.DirContext;
import javax.naming.ldap.Control;
import javax.naming.ldap.LdapContext;

import org.springframework.ldap.control.AbstractRequestControlDirContextProcessor;

public class DirSyncContextProcesor extends AbstractRequestControlDirContextProcessor {
    
    private byte [] cookie;
    
    public DirSyncContextProcesor(byte [] dirSyncCookie) {
        cookie = dirSyncCookie;
    }

    @Override
    public void postProcess(DirContext ctx) throws NamingException {
        LdapContext ldapContext = (LdapContext) ctx;
        Control[] responseControls = ldapContext.getResponseControls();
        if(responseControls==null) return;
        for (Control con: responseControls) {
           if (DirSyncControlResponse.OID.equals(con.getID())) {
               DirSyncControlResponse resp = new DirSyncControlResponse(DirSyncControlResponse.OID, con.isCritical(), con.getEncodedValue());
               this.cookie = resp.getCookie();
           }
        }
    }

    @Override
    public Control createRequestControl() {
        if(cookie != null)        return new DirSyncControl(cookie);
        return new DirSyncControl();
    }
    
    public byte [] getCookie() {
        return cookie;
    }
    
}
