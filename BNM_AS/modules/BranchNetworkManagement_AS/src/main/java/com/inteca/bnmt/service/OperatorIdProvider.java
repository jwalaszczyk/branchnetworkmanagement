package com.inteca.bnmt.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.inteca.bnmt.ldap.domain.LdapUser;
import com.inteca.bnmt.ldap.domain.UserSource;
import com.inteca.bnmt.ldap.service.LDAPUserService;
import com.inteca.bnmt.ldap.service.exceptions.LdapSearchException;
import com.inteca.bnmt.ldap.service.exceptions.LdapUpdateException;

import java.util.List;

@Service
public class OperatorIdProvider {
    private static final Logger log = LoggerFactory.getLogger(OperatorIdProvider.class);

    @Autowired
    private LDAPUserService externalUserService;

    @Autowired
    private LDAPUserService internalUserService;

    public void resolve(LdapUser user) {

        try {
            if (user.getOperatorId() != null) {
                return ;
            }
            if(setupUserOperatorId(user, externalUserService)) {
                return ;
            }
            setupUserOperatorId(user, internalUserService);
        } catch (LdapSearchException e) {
            log.error("Error while searching ldap base", e);
            log.error(e.getMessage(), e);
        }
    }

    private boolean setupUserOperatorId(LdapUser user, LDAPUserService service) throws LdapSearchException {
        List<LdapUser> usersExt = service.getUsers(user.getPesel());
        for (LdapUser checkUser : usersExt) {
            if (checkUser.getOperatorId() != null) {
                log.info("User with oprtrId found in {}", user.getSource());
                user.setOperatorId(checkUser.getOperatorId());
                return true;
            } 
        }
        return false;
    }

    public void persist(LdapUser user) {
        try {
            boolean isExternal = user.getSource().equals(UserSource.AD_EXTERNAL.getName());
            LDAPUserService service = isExternal ?  externalUserService : internalUserService;
            log.info("User: {} updated in {}", user.getDn(), user.getSource());
            service.updateUser(user);
        } catch (LdapUpdateException e) {
            log.error("Cannot update user: " + user.getDn(),e);
        }


    }

}
