package com.inteca.bnmt.translation.util;

import java.util.ArrayList;
import java.util.List;

public class ListWrapper<T> {
    private final List<T> list;

    public ListWrapper() {
        this(new ArrayList<>());
    }

    public ListWrapper(List<T> list) {
        this.list = list;
    }

    public List<T> getList() {
        return list;
    }
}
