package com.inteca.bnmt.exception.business;

import com.inteca.bnmt.exception.ApplicationBusinessException;
import com.inteca.bnmt.exception.NlsBundleApplicationRoot;

public class DuplicatedOperatorIdException extends ApplicationBusinessException {

    private static final long serialVersionUID = 370779502346257587L;

    public DuplicatedOperatorIdException(Iterable<String> duplicatedOperatorIds) {
        super(createBundle(NlsBundleApplicationRoot.class).duplicatedOperatorId(String.join(", ", duplicatedOperatorIds)));
    }

    @Override
    public String getCode() {
        return "1000";
    }

    @Override
    protected String getErrorReason() {
        return "Found duplicated OperatorId";
    }
}
