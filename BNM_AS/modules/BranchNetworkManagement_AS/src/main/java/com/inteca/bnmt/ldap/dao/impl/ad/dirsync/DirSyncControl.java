package com.inteca.bnmt.ldap.dao.impl.ad.dirsync;

import javax.naming.ldap.BasicControl;
import javax.websocket.EncodeException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sun.jndi.ldap.Ber;
import com.sun.jndi.ldap.BerEncoder;

@SuppressWarnings("restriction")
public class DirSyncControl extends BasicControl {
    private static final Logger log = LoggerFactory.getLogger(DirSyncControl.class);

    private static final long serialVersionUID = -930993758829518418L;

    private static final byte[] EMPTY_COOKIE = new byte[0];
    public static final String OID = SyncFlags.LDAP_SERVER_DIRSYNC_OID;

    private static final int FLAGS = SyncFlags.LDAP_DIRSYNC_OBJECT_SECURITY ;

    public DirSyncControl() {
        super(SyncFlags.LDAP_SERVER_DIRSYNC_OID, true, null);
        super.value = berEncodedValue(EMPTY_COOKIE);
    }
    
    public DirSyncControl(byte []cookie) {
        super(SyncFlags.LDAP_SERVER_DIRSYNC_OID, true, cookie);
        super.value = berEncodedValue(cookie == null ? EMPTY_COOKIE : cookie);
    }

    /**
     * BER encode the cookie value.
     *
     * @param cookie cookie value to be encoded.
     * @return ber encoded cookie value.
     * @throws EncodeException 
     */
    private byte[] berEncodedValue(byte[] cookie)   {
//         build the ASN.1 BER encoding
        BerEncoder ber = new BerEncoder();

        try {
            ber.beginSeq(Ber.ASN_SEQUENCE | Ber.ASN_CONSTRUCTOR);
                ber.encodeInt(FLAGS); 
                ber.encodeInt(Integer.MAX_VALUE); 
                ber.encodeOctetString(cookie, Ber.ASN_OCTET_STR);
            ber.endSeq();
        } catch (Exception e) {
            log.error("Cannot prepare AD request message",e);
        }

        return ber.getTrimmedBuf();
    }
}