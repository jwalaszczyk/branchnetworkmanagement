package com.inteca.bnmt.ldap.domain;

public enum UserSource {
    
    AD_EXTERNAL("AD_EXTERNAL"),
    AD_INTERNAL("AD_INTERNAL");
    
    private UserSource(String name ){
        this.name=name;
    }
    
    private String name;
    
    public String getName() {
        return name;
    }

}
