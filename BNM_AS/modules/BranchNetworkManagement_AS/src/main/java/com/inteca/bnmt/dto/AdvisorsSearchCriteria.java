package com.inteca.bnmt.dto;

import java.util.List;

import com.inteca.bnmt.entity.FunctionalRole;
import com.inteca.bnmt.entity.OrganizationUnit;
import com.inteca.bnmt.entity.SalesPointInformation;

public class AdvisorsSearchCriteria {

    private List<OrganizationUnit> organizationUnits;
    private List<FunctionalRole> functionalRoles;
    
    public List<FunctionalRole> getFunctionalRoles() {
        return functionalRoles;
    }
    public void setFunctionalRoles(List<FunctionalRole> functionalRoles) {
        this.functionalRoles = functionalRoles;
    }
    public List<OrganizationUnit> getOrganizationUnits() {
        return organizationUnits;
    }
    
    public void setOrganizationUnits(List<OrganizationUnit> organizationUnits) {
        this.organizationUnits = organizationUnits;
    }
    
}
