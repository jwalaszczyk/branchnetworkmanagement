package com.inteca.bnmt.repository;

import com.inteca.bnmt.entity.AdGroup;
import org.springframework.data.jpa.repository.JpaRepository;


public interface AdGroupRepository extends JpaRepository<AdGroup, Long> {

    AdGroup findByName(String name);
}
