package com.inteca.bnmt.mediator;

import com.inteca.bnmt.ldap.domain.LdapUser;

public interface FederationChangeHadler {

    void onUserRemove(LdapUser user);

    void onUserChange(LdapUser user);

}
