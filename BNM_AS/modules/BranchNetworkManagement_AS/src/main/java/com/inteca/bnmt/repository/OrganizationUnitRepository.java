package com.inteca.bnmt.repository;

import com.inteca.bnmt.entity.OrganizationUnit;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrganizationUnitRepository extends JpaRepository<OrganizationUnit, Long> {

    OrganizationUnit findByUnitId(String unitId);

    @EntityGraph(value = "organizationUnitWithAddress", type = EntityGraph.EntityGraphType.FETCH)
    @Query("SELECT ou "
        + "FROM OrganizationUnit ou "
        + "LEFT JOIN ou.advisor a "
        + "WHERE (:operatorId IS NULL OR a.operatorId = :operatorId) AND (:login  IS NULL OR a.login = :login) "
        + "OR    (:brcd       IS NULL OR ou.brcd = :brcd)            AND (:unitId IS NULL OR ou.unitId = :unitId)")
    List<OrganizationUnit> findAllForQuerySalesPoint(Integer operatorId, String login, String brcd, String unitId);

    @EntityGraph(value = "organizationUnitWithAddress", type = EntityGraph.EntityGraphType.FETCH)
    @Query("SELECT ou "
        + "FROM OrganizationUnit ou "
        + "WHERE (:brcd IS NULL OR ou.brcd = :brcd) AND (:unitId IS NULL OR ou.unitId = :unitId)")
    List<OrganizationUnit> findAllForQuerySalesPointOrganizationUnit(String brcd, String unitId);

    @EntityGraph(value = "organizationUnitWithAddress", type = EntityGraph.EntityGraphType.FETCH)
    @Query("SELECT ou "
        + "FROM OrganizationUnit ou "
        + "LEFT JOIN ou.advisor a "
        + "WHERE (:operatorId IS NULL OR a.operatorId = :operatorId) AND (:login  IS NULL OR a.login = :login)")
    List<OrganizationUnit> findAllForQuerySalesPointAdvisor(Integer operatorId, String login);

}
