package com.inteca.bnmt.ldap.dao.impl.ad;

import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.stereotype.Service;

import com.inteca.bnmt.ldap.dao.ADUserDao;
import com.inteca.bnmt.ldap.dao.impl.ad.dirsync.DirSyncContextProcesor;
import com.inteca.bnmt.ldap.dao.impl.ad.dirsync.SyncCookieProvider;
import com.inteca.bnmt.ldap.dao.impl.ad.mappers.ADUserContextMapper;
import com.inteca.bnmt.ldap.dao.impl.generic.LDAPUserDaoImpl;
import com.inteca.bnmt.ldap.domain.LdapUser;

@Service("aDIntUserDao")
public class ADInternalUserDaoImpl implements ADUserDao {

	LDAPUserDaoImpl service;

	@Autowired
	@Qualifier("adIntLdapTemplate")
	private LdapTemplate ldapTemplate;

	@Value("${ldap.ad.int.personclass}")
	private String ldapPersonClass;

    @Value("${ldap.ad.int.base}")
	private String ldapBase;
    
    @Value("${ldap.ad.int.peselFiledName}")
    private String peselFiledName;
    
    @Value("${ldap.ad.int.operatorIdFiledName}")
    private String operatorIdFiledName;

	@Value("${ldap.ad.ext.base}")
	private String baseDn;
	
    @Autowired
    private SyncCookieProvider internalCookieProvider;


	@PostConstruct
	public void initialize() {
	    ADUserContextMapper contextMapper = new ADUserContextMapper( peselFiledName,operatorIdFiledName);
	    service = new LDAPUserDaoImpl(ldapTemplate, contextMapper, ldapPersonClass,peselFiledName);
	}

	@Override
    public List<LdapUser> getAllChanged()  {
        DirSyncContextProcesor proc = new DirSyncContextProcesor(internalCookieProvider.getCookie());
        List<LdapUser> ret = service.getAllChanged(proc);
        internalCookieProvider.update( proc.getCookie());
        return ret;
    }

    @Override
    public List<LdapUser> getAllRemoved(String lastUpdateDate) {
        return service.getAllRemoved(lastUpdateDate);
    }

    @Override
    public List<LdapUser> findByPesel(String pesel) {
        return service.findByPesel(pesel);
    }

    @Override
    public void updateOperatorId(LdapUser user) {
        service.updateUser(user);
    }
}
