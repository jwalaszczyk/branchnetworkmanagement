package com.inteca.bnmt.repository;

import com.inteca.bnmt.entity.Advisor;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Set;

public interface AdvisorRepository extends JpaRepository<Advisor, Long> {

    Advisor findByObjectGuid(String objectGuid);

    @EntityGraph(value = "advisorWithOrganizationUnitAndAdGroup", type = EntityGraph.EntityGraphType.FETCH)
    @Query("SELECT a FROM Advisor a " +
        "WHERE a.objectGuid = (:objectGuid)")
    Advisor findByObjectGuidWithOrganizationUnitAndAdGroup(String objectGuid);

    @Query(nativeQuery = true, value = "SELECT operator_id_seq.nextval FROM dual")
    int findNextNumberForOperatorId();

    List<Advisor> findByOrganizationUnitUnitIdNumber(int id);

    List<Advisor> findByOrganizationUnitUnitIdNumberAndFunctionalRoleRoleIdIn(int id, List<Integer> roles);

    @EntityGraph(value = "advisorWithOrganizationUnitAndPrimary", type = EntityGraph.EntityGraphType.FETCH)
    Set<Advisor> findByOperatorIdIn(Set<Integer> operatorIds);
}
