package com.inteca.bnmt.mediator;

import com.inteca.bnmt.dto.AdvisorsSearchCriteria;
import com.inteca.bnmt.dto.OrganizationUnitSearchCriteria;
import com.inteca.bnmt.dto.PrimarySalesPointUpdateCriteria;
import com.inteca.bnmt.dto.PrimarySalesPointUpdateResult;
import com.inteca.bnmt.entity.Advisor;
import com.inteca.bnmt.entity.OrganizationUnit;
import com.inteca.bnmt.service.AdvisorService;
import com.inteca.bnmt.service.OrganizationUnitService;
import com.inteca.bnmt.service.UpdateAdvisorPrimarySalesPointService;
import com.inteca.bnmt.translation.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.credit_agricole.schemas.bnmt._102._2015._08._001.QuerySalesPointRequest;
import pl.credit_agricole.schemas.bnmt._103._2015._08._001.QuerySalesPointResponse;
import pl.credit_agricole.schemas.bnmt._104._2018._08._001.QueryAdvisorRequest;
import pl.credit_agricole.schemas.bnmt._105._2018._08._001.QueryAdvisorResponse;
import pl.credit_agricole.schemas.bnmt._106._2018._08._001.UpdateAdvisorPrimarySalesPointRequest;
import pl.credit_agricole.schemas.bnmt._107._2018._08._001.UpdateAdvisorPrimarySalesPointResponse;

import java.util.List;

@Component
public class BranchNetworkManagementASMediator {
    private static final Logger log = LoggerFactory.getLogger(BranchNetworkManagementASMediator.class);

    // services
    @Autowired
    private OrganizationUnitService organizationUnitService;

    @Autowired
    private AdvisorService advisorService;

    @Autowired
    private UpdateAdvisorPrimarySalesPointService updateAdvisorPrimarySalesPointService;

    // translators
    @Autowired
    QueryAdvisorRequestTranslator queryAdvisorRequestTranslator;

    @Autowired
    QueryAdvisorResponseTranslator queryAdvisorResponseTranslator;

    @Autowired
    QuerySalesPointRequestTranslator querySalesPointRequestTranslator;

    @Autowired
    QuerySalesPointResponseTranslator querySalesPointResponseTranslator;

    @Autowired
    UpdateAdvisorPrimarySalesPointRequestTranslator updateAdvisorPrimarySalesPointRequestTranslator;

    @Autowired
    UpdateAdvisorPrimarySalesPointResponseTranslator updateAdvisorPrimarySalesPointResponseTranslator;

    public QuerySalesPointResponse querySalesPoint(QuerySalesPointRequest request) {
        log.info("START - mediator - querySalesPoint");
        OrganizationUnitSearchCriteria criteria = querySalesPointRequestTranslator.translate(request);
        List<OrganizationUnit> organizationUnits = organizationUnitService.querySalesPoint(criteria);
        log.info("mediator - querySalesPoint: found {} organizationUnits", organizationUnits.size());
        QuerySalesPointResponse translate = querySalesPointResponseTranslator.translate(organizationUnits);
        log.info("STOP - mediator - querySalesPoint");
        return translate;
    }

    public QueryAdvisorResponse queryAdvisor(QueryAdvisorRequest request) {
        log.info("START - mediator - queryAdvisor");
        AdvisorsSearchCriteria criteria = queryAdvisorRequestTranslator.translate(request);
        List<Advisor> advisors = advisorService.queryAdvisor(criteria);
        log.info("mediator - queryAdvisor: found {} advisors", advisors.size());
        QueryAdvisorResponse response = new QueryAdvisorResponse();
        response.setAdvsrList(queryAdvisorResponseTranslator.translate(advisors));
        log.info("STOP - mediator - queryAdvisor");
        return response;
    }

    public UpdateAdvisorPrimarySalesPointResponse updateAdvisorPrimarySalesPoint(UpdateAdvisorPrimarySalesPointRequest request) {
        log.info("START - mediator - updateAdvisorPrimarySalesPoint");
        List<PrimarySalesPointUpdateCriteria> criteria = updateAdvisorPrimarySalesPointRequestTranslator.translate(request);
        log.info("mediator - updateAdvisorPrimarySalesPoint: updating {} advisors", criteria.size());
        PrimarySalesPointUpdateResult result = updateAdvisorPrimarySalesPointService.updateAdvisorPrimarySalesPoint(criteria);
        log.info("mediator - updateAdvisorPrimarySalesPoint: updated {} advisors with status {}", criteria.size(), result.getStatus());
        UpdateAdvisorPrimarySalesPointResponse response = updateAdvisorPrimarySalesPointResponseTranslator.translate(result);
        log.info("STOP - mediator - updateAdvisorPrimarySalesPoint");
        return response;
    }
}
