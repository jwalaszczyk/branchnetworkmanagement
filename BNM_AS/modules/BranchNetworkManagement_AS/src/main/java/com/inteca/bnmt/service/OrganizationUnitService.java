package com.inteca.bnmt.service;

import com.inteca.bnmt.dto.OrganizationUnitSearchCriteria;
import com.inteca.bnmt.entity.OrganizationUnit;
import com.inteca.bnmt.repository.OrganizationUnitRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrganizationUnitService {
    private static final Logger log = LoggerFactory.getLogger(OrganizationUnitService.class);

    @Autowired
    private OrganizationUnitRepository organizationUnitRepository;

    public List<OrganizationUnit> querySalesPoint(OrganizationUnitSearchCriteria searchCriteria) {
        if (searchCriteria.getOperatorId() == null && searchCriteria.getLogin() == null) {
            log.info("querySalesPoint - using findAllForQuerySalesPoint with parameters: brcd unitId");
            return organizationUnitRepository.findAllForQuerySalesPointOrganizationUnit(searchCriteria.getBrcd(), searchCriteria.getUnitId());
        } else if (searchCriteria.getBrcd() == null && searchCriteria.getUnitId() == null) {
            log.info("querySalesPoint - using findAllForQuerySalesPoint with parameters: operatorId login");
            return organizationUnitRepository.findAllForQuerySalesPointAdvisor(searchCriteria.getOperatorId(), searchCriteria.getLogin());
        } else {
            log.info("querySalesPoint - using findAllForQuerySalesPoint with parameters: operatorId login brcd unitId");
            return organizationUnitRepository.findAllForQuerySalesPoint(searchCriteria.getOperatorId(), searchCriteria.getLogin(),
                searchCriteria.getBrcd(), searchCriteria.getUnitId());
        }
    }
}
