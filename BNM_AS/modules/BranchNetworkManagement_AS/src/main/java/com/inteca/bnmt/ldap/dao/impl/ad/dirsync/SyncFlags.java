package com.inteca.bnmt.ldap.dao.impl.ad.dirsync;

public class SyncFlags {
    
    private SyncFlags(){}
    
    public static final String LDAP_SERVER_DIRSYNC_OID ="1.2.840.113556.1.4.841";
    public static final String LDAP_SERVER_SHOW_DELETED_OID = "1.2.840.113556.1.4.417";
    
    public static final int LDAP_DIRSYNC_OBJECT_SECURITY =  0x00000001;
    public static final int LDAP_DIRSYNC_ANCESTORS_FIRST_ORDER =  0x00000800;
    public static final int LDAP_DIRSYNC_PUBLIC_DATA_ONLY =  0x00002000;
    public static final int LDAP_DIRSYNC_INCREMENTAL_VALUES =  0x80000000;
}
