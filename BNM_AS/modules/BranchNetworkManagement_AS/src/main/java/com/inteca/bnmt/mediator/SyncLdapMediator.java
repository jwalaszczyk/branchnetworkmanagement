package com.inteca.bnmt.mediator;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.inteca.bnmt.ldap.domain.LdapUser;
import com.inteca.bnmt.ldap.domain.UserSource;
import com.inteca.bnmt.ldap.service.LDAPUserService;
import com.inteca.bnmt.service.SyncDateResolver;

@Service
public class SyncLdapMediator {
    
    private static final Logger log = LoggerFactory.getLogger(SyncLdapMediator.class);

    @Autowired
    private LDAPUserService externalUserService;
    @Autowired
    private LDAPUserService internalUserService;
    
    @Autowired 
    private SyncDateResolver dateResolver;
    
    @Autowired
    private FederationChangeHadler hadler;
    
    private volatile boolean running = false;

    public void syncAll() {
        if(running) return;
        running = true;
        syncUpdated(externalUserService, UserSource.AD_EXTERNAL);
        syncDeleted(externalUserService,dateResolver.popLastExternalDate(), UserSource.AD_EXTERNAL);
        syncUpdated(internalUserService, UserSource.AD_INTERNAL);
        syncDeleted(internalUserService,dateResolver.popLastInternalDate(),UserSource.AD_INTERNAL);
        running = false;
    }
    
    /**
     * Call get changes until get it all and notify handler
     */
    private void syncDeleted(LDAPUserService service, String lastUpdateDate, UserSource src) {
        log.info("Synchronizing Deleted {} users", src);
        try {
            while(true) {
                List<LdapUser> result = service.getAllRemoved(lastUpdateDate);
                log.debug("Found: {} removed objects", result.size());
                result.stream().forEach(user->{
                    user.setSource(src.getName());
                    hadler.onUserRemove(user);
                });
                if(result.isEmpty()) break;
            }
        }catch (Exception e) {
            log.error("Error when synchronise Deleted users",e);   
        }
    }

    /**
     * Call get changes until get it all and notify handler
     */
    private void syncUpdated(LDAPUserService service,UserSource src) {
        log.info("Synchronizing Changed {} users", src);
        try {
            while(true) {
                List<LdapUser> result = service.getAllChanged();
                log.debug("Found: {} changed objects", result.size());
                result.stream().forEach(user->{
                    user.setSource(src.getName());
                    hadler.onUserChange(user);   
                });
                if(result.isEmpty()) break;
            }
        }catch (Exception e) {
            log.error("Error when synchronise Updated users",e);   
        }
    }


}
