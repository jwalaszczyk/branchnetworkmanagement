package com.inteca.bnmt.config;

import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.net.ssl.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.ldap.core.ContextSource;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.core.support.LdapContextSource;
import org.springframework.ldap.pool2.factory.MutablePooledContextSource;
import org.springframework.ldap.pool2.factory.PoolConfig;
import org.springframework.ldap.pool2.validation.DefaultDirContextValidator;
import org.springframework.ldap.transaction.compensating.manager.TransactionAwareContextSourceProxy;


@Configuration
public class LDAPConfig {

	private static final String LDAP_PFX = "ldap.";

    private final Logger logger = LoggerFactory.getLogger(LDAPConfig.class);
	
    @Autowired
    Environment env;
	private volatile boolean initialized = false;

    public synchronized void trustSelfSignedSSL() {
    	if(initialized) {
    		return;
    	}
    	initialized = true;
        try {
            SSLContext ctx = SSLContext.getInstance("TLS");
            X509TrustManager tm = new X509TrustManager() {

                public void checkClientTrusted(X509Certificate[] xcs, String string) throws CertificateException {
//                	bcz trust all
                }

                public void checkServerTrusted(X509Certificate[] xcs, String string) throws CertificateException {
//                	bcz trust all
                }

                public X509Certificate[] getAcceptedIssuers() {
                    return new X509Certificate[0];
                }
            };
            ctx.init(null, new TrustManager[]{tm}, new java.security.SecureRandom());
            SSLContext.setDefault(ctx);

            HostnameVerifier hostnameVerifier = new HostnameVerifier() {
                @Override
                public boolean verify(String s, SSLSession sslSession) {
                    return true;
                }
            };
            HttpsURLConnection.setDefaultHostnameVerifier(hostnameVerifier);

        } catch (Exception ex) {
            logger.error("Cannot setup SSL" ,ex );
        }
    }

    @PostConstruct
    public void init() {
        trustSelfSignedSSL();
    }
    
    public LdapContextSource ldapContextSource(String configPrefix) {
        LdapContextSource contextSource = new LdapContextSource();
        Map<String, Object> contextEnv = new HashMap<>();
        contextSource.setBaseEnvironmentProperties(contextEnv );
        contextSource.setUrl(env.getRequiredProperty(LDAP_PFX+configPrefix+".url"));
        contextSource.setUserDn(env.getRequiredProperty(LDAP_PFX+configPrefix+".userDn"));
        contextSource.setPassword(env.getRequiredProperty(LDAP_PFX+configPrefix+".password"));
        contextSource.setBase(env.getRequiredProperty(LDAP_PFX+configPrefix+".base"));
        contextSource.setReferral("ignore");
        contextSource.setPooled(false);
        contextSource.afterPropertiesSet();
        return contextSource;
    }
    
    @Bean(name = "adExtContextSource")
    public LdapContextSource adExtContextSource() {
    	return ldapContextSource("ad.ext");
    }
    
    @Bean(name = "adIntContextSource")
    public LdapContextSource adIntContextSource() {
    	return ldapContextSource("ad.int");
    }

    @Bean
    public ContextSource poolingContextSourceAdInt(LdapContextSource adIntContextSource) {
    	MutablePooledContextSource mutablePooledContextSourceEd = getMutablePooledContextSource(adIntContextSource);
        return new TransactionAwareContextSourceProxy(mutablePooledContextSourceEd);
    }
    
    @Bean
    public ContextSource poolingContextSourceAdExt(LdapContextSource adExtContextSource) {
    	MutablePooledContextSource mutablePooledContextSourceAdExt = getMutablePooledContextSource(adExtContextSource);
        return new TransactionAwareContextSourceProxy(mutablePooledContextSourceAdExt);
    }
    
    public MutablePooledContextSource getMutablePooledContextSource(LdapContextSource conetxt){
        PoolConfig poolConfig = new PoolConfig();
        poolConfig.setTestWhileIdle(true);
		poolConfig.setTestOnBorrow(true);
        MutablePooledContextSource poolingContextSource = new MutablePooledContextSource(poolConfig);
        poolingContextSource.setContextSource(conetxt);
        poolingContextSource.setDirContextValidator(new DefaultDirContextValidator());
        return poolingContextSource;
    }

    /**
     * Ldap template pool bean for Active Directory EXTERNAL
     * @throws Exception 
     */
	@Bean(name = "adExtLdapTemplate")
	public LdapTemplate adExtLdapTemplate(ContextSource poolingContextSourceAdExt) throws Exception {
		LdapTemplate tmpl = new LdapTemplate(poolingContextSourceAdExt);
		tmpl.setIgnorePartialResultException(true);
		tmpl.afterPropertiesSet();
		return tmpl;
    }
	
    /**
     * Ldap template pool bean for Active Directory INTERNAL
     * @throws Exception 
     */
	@Bean(name = "adIntLdapTemplate")
	public LdapTemplate adIntLdapTemplate(ContextSource poolingContextSourceAdInt) throws Exception {
		LdapTemplate tmpl = new LdapTemplate(poolingContextSourceAdInt);
	    tmpl.setIgnorePartialResultException(true);
	    tmpl.afterPropertiesSet();
		return tmpl;
    }
}
