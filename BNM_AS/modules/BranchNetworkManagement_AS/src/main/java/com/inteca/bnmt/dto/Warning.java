package com.inteca.bnmt.dto;

public class Warning {
    private String attribute;
    private String code;
    private String clazz;
    private String description;
    private String element;
    private String value;

    public Warning() {
    }

    public Warning(String attribute, String code, String clazz, String description, String element, String value) {
        this.attribute = attribute;
        this.code = code;
        this.clazz = clazz;
        this.description = description;
        this.element = element;
        this.value = value;
    }

    public String getAttribute() {
        return attribute;
    }

    public void setAttribute(String attribute) {
        this.attribute = attribute;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getClazz() {
        return clazz;
    }

    public void setClazz(String clazz) {
        this.clazz = clazz;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getElement() {
        return element;
    }

    public void setElement(String element) {
        this.element = element;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
