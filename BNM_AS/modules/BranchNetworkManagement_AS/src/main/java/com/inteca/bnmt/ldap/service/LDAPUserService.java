package com.inteca.bnmt.ldap.service;

import java.util.List;

import com.inteca.bnmt.ldap.dao.ADUserDao;
import com.inteca.bnmt.ldap.domain.LdapUser;
import com.inteca.bnmt.ldap.service.exceptions.LdapSearchException;
import com.inteca.bnmt.ldap.service.exceptions.LdapUpdateException;

public class LDAPUserService {
	
	private ADUserDao ldapUserDao;
	
	public LDAPUserService(ADUserDao ldapUserDao) {
	    this.ldapUserDao = ldapUserDao;
	}
	
	public List<LdapUser> getAllChanged() throws LdapSearchException{
	    try {
	        return ldapUserDao.getAllChanged();
	    }catch (Exception e) {
            throw new LdapSearchException("Getting chnaged objects fail", e);
        }
	}

    public List<LdapUser> getAllRemoved(String lastUpdateDate) throws LdapSearchException {
        try {
            return ldapUserDao.getAllRemoved(lastUpdateDate);
        }catch (Exception e) {
            throw new LdapSearchException("Getting deleted objects fail", e);
        }
    }
    
    public List<LdapUser> getUsers(String pesel) throws LdapSearchException {
        try {
            return ldapUserDao.findByPesel(pesel);
        }catch (Exception e) {
            throw new LdapSearchException("Getting user by pesel: " + pesel +" fail", e);
        }
    }
    
    public void updateUser(LdapUser user) throws LdapUpdateException {
        try {
            ldapUserDao.updateOperatorId(user);
        }catch (Exception e) {
            throw new LdapUpdateException("Updating user ("+user+") fail", e);
        }
    }
	
}
