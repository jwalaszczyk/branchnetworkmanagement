package com.inteca.bnmt.translation;

import com.inteca.bnmt.entity.Advisor;
import com.inteca.bnmt.ldap.domain.LdapUser;
import org.joda.time.DateTime;
import org.mapstruct.*;

import java.time.*;
import java.time.format.DateTimeFormatter;

/**
 * @author ddedek
 * 10.08.2018
 */

@Mapper
public abstract class LdapToBNMUserTranslator {

    @Mappings({
            @Mapping(source = "login", target = "code"),
            @Mapping(source = "name", target = "firstName"),
            @Mapping(source = "surname", target = "surname"),
            @Mapping(source = "login", target = "login"),
            @Mapping(source = "loginDisabled", target = "active"),
            @Mapping(source = "operatorId", target = "operatorId"),
            @Mapping(source = "source", target = "dataSource"),
            @Mapping(source = "objectGUID", target = "objectGuid"),
            @Mapping(source = "mail", target = "email"),
            @Mapping(source = "mobile", target = "mobileNumber"),
            @Mapping(source = "stationaryPhone", target = "stationaryNumber"),
            @Mapping(source = "lastModificationDate", target = "lastModified", qualifiedByName = "ldapStringDateToDateTime"),
            @Mapping(ignore = true, target = "functionalRole"),
            @Mapping(ignore = true, target = "primary"),
            @Mapping(ignore = true, target = "adGroup"),
            @Mapping(ignore = true, target = "organizationUnit"),
    })
    public abstract void toAdvisor(LdapUser ldapUser, @MappingTarget Advisor referenceToCurrentAdvisor);


    /**
     * Convert AD when created to joda DateTime
     *
     * @param lastModificationDate
     * @return
     */
    @Named("ldapStringDateToDateTime")
    public DateTime dateTime(String lastModificationDate) {

        //to ZonedDateTime
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("uuuuMMddHHmmss[,S][.S]X");
        OffsetDateTime offsetDateTime = OffsetDateTime.parse(lastModificationDate, dateTimeFormatter);
        LocalDateTime localDateTime = offsetDateTime.toLocalDateTime();
        ZonedDateTime zonedDateTime = localDateTime.atZone(ZoneId.systemDefault());

        //convert java.time to joda DateTime
        Instant instant = zonedDateTime.toInstant();
        long millis = instant.toEpochMilli();
        org.joda.time.LocalDateTime jodaLocalDateTime = new org.joda.time.LocalDateTime(millis);
        DateTime dateTime = jodaLocalDateTime.toDateTime();

        return dateTime;
    }
}
