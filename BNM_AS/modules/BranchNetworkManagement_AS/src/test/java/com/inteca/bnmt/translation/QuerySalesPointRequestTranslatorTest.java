package com.inteca.bnmt.translation;

import com.inteca.bnmt.dto.OrganizationUnitSearchCriteria;
import org.junit.Test;
import org.mapstruct.factory.Mappers;
import org.mockito.InjectMocks;
import pl.credit_agricole.schemas.bnmt._102._2015._08._001.Advisor1;
import pl.credit_agricole.schemas.bnmt._102._2015._08._001.QuerySalesPointRequest;
import pl.credit_agricole.schemas.bnmt._102._2015._08._001.SalesPoint3;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class QuerySalesPointRequestTranslatorTest {

    @InjectMocks
    QuerySalesPointRequestTranslator querySalesPointRequestTranslator = Mappers.getMapper(QuerySalesPointRequestTranslator.class);

    @Test
    public void translate_whenRequestEmpty() {
        // given
        QuerySalesPointRequest request = new QuerySalesPointRequest();

        // when
        OrganizationUnitSearchCriteria criteria = querySalesPointRequestTranslator.translate(request);

        // then
        assertNull(criteria.getOperatorId());
        assertNull(criteria.getLogin());
        assertNull(criteria.getBrcd());
        assertNull(criteria.getUnitId());
    }

    @Test
    public void translate_whenRequestEmptyAdvisorAndSalesPoint() {
        // given
        QuerySalesPointRequest request = createRequest(null, null, null, null);

        // when
        OrganizationUnitSearchCriteria criteria = querySalesPointRequestTranslator.translate(request);

        // then
        assertNull(criteria.getOperatorId());
        assertNull(criteria.getLogin());
        assertNull(criteria.getBrcd());
        assertNull(criteria.getUnitId());
    }

    @Test
    public void translate_whenRequestFull() {
        // given
        QuerySalesPointRequest request = createRequest(1, "1", "1", "1");

        // when
        OrganizationUnitSearchCriteria criteria = querySalesPointRequestTranslator.translate(request);

        // then
        assertEquals(new Integer(1), criteria.getOperatorId());
        assertEquals("1", criteria.getLogin());
        assertEquals("1", criteria.getBrcd());
        assertEquals("1", criteria.getUnitId());
    }

    @Test
    public void translate_isNullingEmptyString() {
        // given
        QuerySalesPointRequest request = createRequest(1, "", "", "");

        // when
        OrganizationUnitSearchCriteria criteria = querySalesPointRequestTranslator.translate(request);

        // then
        assertNull(criteria.getLogin());
        assertNull(criteria.getBrcd());
        assertNull(criteria.getUnitId());
    }

    private QuerySalesPointRequest createRequest(Integer operatorId, String login, String brcd, String unitId) {
        Advisor1 advisor = new Advisor1();
        advisor.setOprtrId(operatorId == null ? null : new BigDecimal(operatorId));
        advisor.setLgn(login);

        SalesPoint3 salesPoint = new SalesPoint3();
        salesPoint.setBrcd(brcd);
        salesPoint.setId(unitId);

        QuerySalesPointRequest querySalesPointRequest = new QuerySalesPointRequest();
        querySalesPointRequest.setAdvsr(advisor);
        querySalesPointRequest.setSalesPt(salesPoint);
        return querySalesPointRequest;
    }
}