/*
package com.inteca.bnmt.service;

import com.inteca.bnmt.entity.Advisor;
import com.inteca.bnmt.entity.OrganizationUnit;
import com.inteca.bnmt.ldap.domain.LdapUser;
import com.inteca.bnmt.repository.OrganizationUnitRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

*/
/**
 * @author ddedek
 * 10.08.2018
 *//*


@RunWith(MockitoJUnitRunner.class)
public class AdvisorGroupResolverTest {

    @InjectMocks
    private AdvisorGroupResolver advisorGroupResolver;

    @Mock
    private OrganizationUnitRepository organizationUnitRepository;

    @Before
    public void initClass() {
        when(organizationUnitRepository.findByUnitId(anyString())).thenReturn(new OrganizationUnit());
    }

    private LdapUser initLdapGroups(LdapUser ldapUser) {
        Set<String> organizationUnitCodes = new HashSet<>();

        organizationUnitCodes.add("uid=al,cn=users,dc=RPTSVT,dc=1234");
        organizationUnitCodes.add("uid=al,cn=users,dc=RPTSVT,dc=5678");
        organizationUnitCodes.add("uid=al,cn=users,dc=RPTSVT,dc=9012");

        ldapUser.setOrganizationUnitcodes(organizationUnitCodes);
        ldapUser.setName("Imie");

        return ldapUser;
    }

    private Advisor initAdvisorGroups(Advisor advisor, boolean primary) {

        AdvisorOrganizationUnit advisorOrganizationUnit1 = new AdvisorOrganizationUnit();
        advisorOrganizationUnit1.setPrimary(false);
        OrganizationUnit organizationUnit1 = new OrganizationUnit();
        organizationUnit1.setUnitId("7777");
        advisorOrganizationUnit1.setOrganizationUnit(organizationUnit1);

        AdvisorOrganizationUnit advisorOrganizationUnit2 = new AdvisorOrganizationUnit();
        advisorOrganizationUnit2.setPrimary(primary);
        OrganizationUnit organizationUnit2 = new OrganizationUnit();
        organizationUnit2.setUnitId("5678");
        advisorOrganizationUnit2.setOrganizationUnit(organizationUnit2);

        advisor.addToAdvisorOrganizationUnit(advisorOrganizationUnit1, advisorOrganizationUnit2);

        advisor.setFirstName("Pierwsze");
        return advisor;
    }

    @Test
    public void testGroupResolverWithPrimary() {
        //        ARRANGE
        LdapUser ldapUser = new LdapUser();
        LdapUser ldapUser1 = initLdapGroups(ldapUser);

        Advisor advisor = new Advisor();
        Advisor advisor1 = initAdvisorGroups(advisor, true);

        //        ACT
        advisorGroupResolver.groupResolver(ldapUser1, advisor1);

        //        ASSERT
        //        assertEquals(3, advisor1.getAdvisorOrganizationUnit().size());
    }

    @Test
    public void testGroupResolverWithoutPrimary() {
        //        ARRANGE
        LdapUser ldapUser = new LdapUser();
        LdapUser ldapUser1 = initLdapGroups(ldapUser);

        Advisor advisor = new Advisor();
        Advisor advisor1 = initAdvisorGroups(advisor, false);

        //        ACT
        advisorGroupResolver.groupResolver(ldapUser1, advisor1);

        //        ASSERT
        //        assertEquals(3, advisor1.getAdvisorOrganizationUnit().size());
    }

    @Test
    public void testCutLdapOrganizationUnitcodes() {
        //        ARRANGE
        LdapUser ldapUser = new LdapUser();
        initLdapGroups(ldapUser);

        Set<String> stringHashSet = new HashSet<String>();
        stringHashSet.add("1234");
        stringHashSet.add("9012");
        stringHashSet.add("5678");

        //        ACT
        advisorGroupResolver.cutLdapOrganizationUnitcodes(ldapUser);

        //        ASSERT
        assertEquals(stringHashSet, ldapUser.getOrganizationUnitcodes());
    }

}
*/
