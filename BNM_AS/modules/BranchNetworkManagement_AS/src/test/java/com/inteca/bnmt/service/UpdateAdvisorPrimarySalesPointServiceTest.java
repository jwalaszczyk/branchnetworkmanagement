package com.inteca.bnmt.service;

import com.google.common.collect.Sets;
import com.inteca.bnmt.dto.PrimarySalesPointUpdateCriteria;
import com.inteca.bnmt.dto.PrimarySalesPointUpdateResult;
import com.inteca.bnmt.entity.Advisor;
import com.inteca.bnmt.entity.OrganizationUnit;
import com.inteca.bnmt.exception.business.DuplicatedOperatorIdException;
import com.inteca.bnmt.repository.AdvisorRepository;
import com.inteca.bnmt.repository.OrganizationUnitRepository;
import org.assertj.core.util.Lists;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Collection;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class UpdateAdvisorPrimarySalesPointServiceTest {

    @Mock
    private AdvisorRepository advisorRepository;

    @Mock
    private OrganizationUnitRepository organizationUnitRepository;

    @InjectMocks
    private UpdateAdvisorPrimarySalesPointService updateAdvisorPrimarySalesPointService;

    @Test
    public void updateAdvisorPrimarySalesPoint_happyPath() {
        // given
        PrimarySalesPointUpdateCriteria updateCriteria = new PrimarySalesPointUpdateCriteria();
        updateCriteria.setOperatorId(1);
        updateCriteria.setUnitId("1");
        List<PrimarySalesPointUpdateCriteria> updateCriteriaList = Lists.newArrayList(updateCriteria);
        Advisor advisor = new Advisor()
            .withOperatorId(1)
            .addToOrganizationUnit(new OrganizationUnit().withUnitId("1"));
        when(advisorRepository.findByOperatorIdIn(Sets.newHashSet(1))).thenReturn(Sets.newHashSet(advisor));

        // when
        PrimarySalesPointUpdateResult updateResult = updateAdvisorPrimarySalesPointService.updateAdvisorPrimarySalesPoint(updateCriteriaList);

        // then
        assertNotNull(updateResult);
        assertTrue(updateResult.getStatus());
        assertTrue(updateResult.getWarningList().isEmpty());
        ArgumentCaptor<Collection<Advisor>> setCaptor = ArgumentCaptor.forClass(Collection.class);
        verify(advisorRepository, times(1)).save(setCaptor.capture());

        assertEquals(1, setCaptor.getValue().size());
        advisor = setCaptor.getValue().stream().findFirst().orElse(null);
        assertNotNull(advisor);
        assertNotNull(advisor.getPrimary());
        assertEquals("1", advisor.getPrimary().getUnitId());
    }

    @Test
    public void updateAdvisorPrimarySalesPoint_multipleUpdates() {
        // given
        PrimarySalesPointUpdateCriteria updateCriteria1 = new PrimarySalesPointUpdateCriteria();
        updateCriteria1.setOperatorId(1);
        updateCriteria1.setUnitId("1");
        PrimarySalesPointUpdateCriteria updateCriteria2 = new PrimarySalesPointUpdateCriteria();
        updateCriteria2.setOperatorId(2);
        updateCriteria2.setUnitId("2");
        List<PrimarySalesPointUpdateCriteria> updateCriteriaList = Lists.newArrayList(updateCriteria1, updateCriteria2);

        Advisor advisor1 = new Advisor()
            .withOperatorId(1)
            .addToOrganizationUnit(new OrganizationUnit().withUnitId("1"));
        Advisor advisor2 = new Advisor()
            .withOperatorId(2)
            .addToOrganizationUnit(new OrganizationUnit().withUnitId("2"));
        when(advisorRepository.findByOperatorIdIn(Sets.newHashSet(1, 2))).thenReturn(Sets.newHashSet(advisor1, advisor2));

        // when
        PrimarySalesPointUpdateResult updateResult = updateAdvisorPrimarySalesPointService.updateAdvisorPrimarySalesPoint(updateCriteriaList);

        // then
        assertNotNull(updateResult);
        assertTrue(updateResult.getStatus());
        assertTrue(updateResult.getWarningList().isEmpty());
        ArgumentCaptor<Collection<Advisor>> setCaptor = ArgumentCaptor.forClass(Collection.class);
        verify(advisorRepository, times(1)).save(setCaptor.capture());

        assertEquals(2, setCaptor.getValue().size());
        advisor1 = setCaptor.getValue().stream().filter(advisor -> advisor.getOperatorId() == 1).findFirst().orElse(null);
        assertNotNull(advisor1);
        assertNotNull(advisor1.getPrimary());
        assertEquals("1", advisor1.getPrimary().getUnitId());
        advisor2 = setCaptor.getValue().stream().filter(advisor -> advisor.getOperatorId() == 2).findFirst().orElse(null);
        assertNotNull(advisor2);
        assertNotNull(advisor2.getPrimary());
        assertEquals("2", advisor2.getPrimary().getUnitId());
    }

    @Test
    public void updateAdvisorPrimarySalesPoint_noOperatorId() {
        // given
        PrimarySalesPointUpdateCriteria updateCriteria = new PrimarySalesPointUpdateCriteria();
        updateCriteria.setOperatorId(1);
        updateCriteria.setUnitId("1");
        List<PrimarySalesPointUpdateCriteria> updateCriteriaList = Lists.newArrayList(updateCriteria);
        when(advisorRepository.findByOperatorIdIn(Sets.newHashSet(1))).thenReturn(Sets.newHashSet());

        // when
        PrimarySalesPointUpdateResult updateResult = updateAdvisorPrimarySalesPointService.updateAdvisorPrimarySalesPoint(updateCriteriaList);

        // then
        assertNotNull(updateResult);
        assertFalse(updateResult.getStatus());
        assertEquals(1, updateResult.getWarningList().size());
        assertEquals("Wskazany doradca nie istnieje w systemie", updateResult.getWarningList().get(0).getAttribute());
        verify(advisorRepository, times(0)).save(anyCollection());
    }

    @Test
    public void updateAdvisorPrimarySalesPoint_noUnitId() {
        // given
        PrimarySalesPointUpdateCriteria updateCriteria = new PrimarySalesPointUpdateCriteria();
        updateCriteria.setOperatorId(1);
        updateCriteria.setUnitId("1");
        List<PrimarySalesPointUpdateCriteria> updateCriteriaList = Lists.newArrayList(updateCriteria);
        Advisor advisor = new Advisor()
            .withOperatorId(1);
        when(advisorRepository.findByOperatorIdIn(Sets.newHashSet(1))).thenReturn(Sets.newHashSet(advisor));
        when(organizationUnitRepository.findByUnitId("1")).thenReturn(null);

        // when
        PrimarySalesPointUpdateResult updateResult = updateAdvisorPrimarySalesPointService.updateAdvisorPrimarySalesPoint(updateCriteriaList);

        // then
        assertNotNull(updateResult);
        assertFalse(updateResult.getStatus());
        assertEquals(1, updateResult.getWarningList().size());
        assertEquals("Wskazana placówka nie istnieje w systemie", updateResult.getWarningList().get(0).getAttribute());
        verify(advisorRepository, times(0)).save(anyCollection());
    }

    @Test
    public void updateAdvisorPrimarySalesPoint_noRelationWithOrganizationUnit() {
        // given
        PrimarySalesPointUpdateCriteria updateCriteria = new PrimarySalesPointUpdateCriteria();
        updateCriteria.setOperatorId(1);
        updateCriteria.setUnitId("1");
        List<PrimarySalesPointUpdateCriteria> updateCriteriaList = Lists.newArrayList(updateCriteria);
        Advisor advisor = new Advisor()
            .withOperatorId(1).withFirstName("Andrzej").withSurname("Kela");
        when(advisorRepository.findByOperatorIdIn(Sets.newHashSet(1))).thenReturn(Sets.newHashSet(advisor));
        when(organizationUnitRepository.findByUnitId("1")).thenReturn(new OrganizationUnit());

        // when
        PrimarySalesPointUpdateResult updateResult = updateAdvisorPrimarySalesPointService.updateAdvisorPrimarySalesPoint(updateCriteriaList);

        // then
        assertNotNull(updateResult);
        assertFalse(updateResult.getStatus());
        assertEquals(1, updateResult.getWarningList().size());
        assertEquals("Brak relacji między doradcą i placówką", updateResult.getWarningList().get(0).getAttribute());
        verify(advisorRepository, times(0)).save(anyCollection());
    }

    @Test(expected = DuplicatedOperatorIdException.class)
    public void updateAdvisorPrimarySalesPoint_duplicatedOperatorId() {
        // given
        PrimarySalesPointUpdateCriteria updateCriteria1 = new PrimarySalesPointUpdateCriteria();
        updateCriteria1.setOperatorId(1);
        PrimarySalesPointUpdateCriteria updateCriteria2 = new PrimarySalesPointUpdateCriteria();
        updateCriteria2.setOperatorId(1);
        List<PrimarySalesPointUpdateCriteria> updateCriteriaList = Lists.newArrayList(updateCriteria1, updateCriteria2);

        // when
        PrimarySalesPointUpdateResult updateResult = updateAdvisorPrimarySalesPointService.updateAdvisorPrimarySalesPoint(updateCriteriaList);

        // then exception
    }

    @Test
    public void updateAdvisorPrimarySalesPoint_emptyUpdateCriteriaSkipped() {
        // given
        List<PrimarySalesPointUpdateCriteria> updateCriteriaList = Lists.newArrayList(new PrimarySalesPointUpdateCriteria());

        // when
        PrimarySalesPointUpdateResult updateResult = updateAdvisorPrimarySalesPointService.updateAdvisorPrimarySalesPoint(updateCriteriaList);

        // then
        assertNotNull(updateResult);
        assertFalse(updateResult.getStatus());
        verify(advisorRepository, times(0)).save(anyCollection());
    }
}