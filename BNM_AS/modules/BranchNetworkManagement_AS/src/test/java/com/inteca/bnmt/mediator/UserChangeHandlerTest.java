package com.inteca.bnmt.mediator;

import com.inteca.bnmt.entity.Advisor;
import com.inteca.bnmt.ldap.domain.LdapUser;
import com.inteca.bnmt.repository.AdvisorRepository;
import com.inteca.bnmt.service.OperatorIdProvider;
import com.inteca.bnmt.translation.LdapToBNMUserTranslator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class UserChangeHandlerTest {

    @Mock
    private OperatorIdProvider operatorIdProvider;

    @Mock
    private LdapToBNMUserTranslator translator;

    @Mock
    private AdvisorRepository advisorRepository;

    @InjectMocks
    private UserChangeHandler handler;

    @Before
    public void initClass() {
        when(advisorRepository.findByObjectGuid(anyString())).thenReturn(new Advisor());
    }

    @Test
    public void testRemove_correct() {
        handler.onUserRemove(getUser("aqsda"));
        verify(advisorRepository).delete(any(Advisor.class));
    }

    @Test
    public void testRemove_nullGuid() {
        handler.onUserRemove(getUser(null));
        verify(advisorRepository, times(0)).delete(any(Advisor.class));
    }

    @Test
    public void testRemove_nullResponse() {
        when(advisorRepository.findByObjectGuid("noObject")).thenReturn(null);
        handler.onUserRemove(getUser("noObject"));
        verify(advisorRepository, times(0)).delete(any(Advisor.class));
    }

    @Test
    public void testRemove_exception() {
        when(advisorRepository.findByObjectGuid("noObject")).thenThrow(new RuntimeException());
        handler.onUserRemove(getUser("noObject"));
        verify(advisorRepository, times(0)).delete(any(Advisor.class));
    }

    private LdapUser getUser(String objectGuid) {
        LdapUser u = new LdapUser();
        u.setObjectGUID(objectGuid);
        return u;
    }
}
