package com.inteca.bnmt.translation;

import java.util.ArrayList;

import org.junit.Test;
import org.mapstruct.factory.Mappers;

import com.inteca.bnmt.dto.AdvisorsSearchCriteria;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import pl.credit_agricole.schemas.bnmt._104._2018._08._001.FunctionalRole;
import pl.credit_agricole.schemas.bnmt._104._2018._08._001.FunctionalRoleList;
import pl.credit_agricole.schemas.bnmt._104._2018._08._001.QueryAdvisorRequest;
import pl.credit_agricole.schemas.bnmt._104._2018._08._001.SalesPoint1;
import pl.credit_agricole.schemas.bnmt._104._2018._08._001.SalesPointList1;

public class QueryAdvisorRequestTranslatorTest {
    
    private QueryAdvisorRequestTranslator translator = Mappers.getMapper(QueryAdvisorRequestTranslator.class);
    
    @Test
    public void test_translate_empty() {
        QueryAdvisorRequest request = new QueryAdvisorRequest();
        request.setFctnlRoleList(new FunctionalRoleList());
        request.getFctnlRoleList();
        AdvisorsSearchCriteria result = translator.translate(request );
        assertNotNull(result);
    }
    
    @Test
    public void test_translate() {
        QueryAdvisorRequest request = new QueryAdvisorRequest();
        request.setFctnlRoleList(new FunctionalRoleList());
        FunctionalRole role = new FunctionalRole();
        role.setCd("code");
        role.setId("11");
        role.setNm("name");
        request.getFctnlRoleList().getFctnlRole().add(role);
        request.setSalesPtList(new SalesPointList1());
        request.getSalesPtList().setSalesPt(new ArrayList<SalesPoint1>());
        SalesPoint1 sp = new SalesPoint1();
        sp.setId("10");
        request.getSalesPtList().getSalesPt().add(sp );
        AdvisorsSearchCriteria result = translator.translate(request );
        assertNotNull(result);
        assertNotNull(result.getFunctionalRoles());
        assertEquals("code", result.getFunctionalRoles().get(0).getCode());
        assertEquals(11, result.getFunctionalRoles().get(0).getRoleId().intValue());
        assertEquals("name", result.getFunctionalRoles().get(0).getName());
        assertNotNull(result.getOrganizationUnits());
        assertEquals(10, result.getOrganizationUnits().get(0).getUnitIdNumber());
    }

}
