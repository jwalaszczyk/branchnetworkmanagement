package com.inteca.bnmt.service;

import com.inteca.bnmt.dto.OrganizationUnitSearchCriteria;
import com.inteca.bnmt.entity.OrganizationUnit;
import com.inteca.bnmt.repository.OrganizationUnitRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Collections;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class OrganizationUnitServiceTest {

    @Mock
    private OrganizationUnitRepository organizationUnitRepository;

    @InjectMocks
    private OrganizationUnitService organizationUnitService;

    @Test
    public void querySalesPoint_whenAllCriteriaFilled() {
        // given
        OrganizationUnitSearchCriteria criteria = new OrganizationUnitSearchCriteria();
        criteria.setOperatorId(1);
        criteria.setLogin("1");
        criteria.setBrcd("1");
        criteria.setUnitId("1");
        when(organizationUnitRepository.findAllForQuerySalesPoint(any(), any(), any(), any())).thenReturn(Collections.emptyList());

        // when
        List<OrganizationUnit> organizationUnits = organizationUnitService.querySalesPoint(criteria);

        // then
        assertNotNull(organizationUnits);
        assertTrue(organizationUnits.isEmpty());
        verify(organizationUnitRepository, times(1)).findAllForQuerySalesPoint(any(), any(), any(), any());
        verify(organizationUnitRepository, times(0)).findAllForQuerySalesPointOrganizationUnit(any(), any());
        verify(organizationUnitRepository, times(0)).findAllForQuerySalesPointAdvisor(any(), any());

        verify(organizationUnitRepository, times(1)).findAllForQuerySalesPoint(1, "1", "1", "1");
    }

    @Test
    public void querySalesPoint_whenAllOrganizationUnitCriteriaFilled() {
        // given
        OrganizationUnitSearchCriteria criteria = new OrganizationUnitSearchCriteria();
        criteria.setBrcd("1");
        criteria.setUnitId("1");
        when(organizationUnitRepository.findAllForQuerySalesPointOrganizationUnit(any(), any())).thenReturn(Collections.singletonList(null));

        // when
        List<OrganizationUnit> organizationUnits = organizationUnitService.querySalesPoint(criteria);

        // then
        assertNotNull(organizationUnits);
        assertEquals(1, organizationUnits.size());
        verify(organizationUnitRepository, times(0)).findAllForQuerySalesPoint(any(), any(), any(), any());
        verify(organizationUnitRepository, times(1)).findAllForQuerySalesPointOrganizationUnit(any(), any());
        verify(organizationUnitRepository, times(0)).findAllForQuerySalesPointAdvisor(any(), any());

        verify(organizationUnitRepository, times(1)).findAllForQuerySalesPointOrganizationUnit("1", "1");
    }

    @Test
    public void querySalesPoint_whenAllAdvisorCriteriaFilled() {
        // given
        OrganizationUnitSearchCriteria criteria = new OrganizationUnitSearchCriteria();
        criteria.setOperatorId(1);
        criteria.setLogin("1");
        when(organizationUnitRepository.findAllForQuerySalesPointAdvisor(any(), any())).thenReturn(Collections.singletonList(null));

        // when
        List<OrganizationUnit> organizationUnits = organizationUnitService.querySalesPoint(criteria);

        // then
        assertNotNull(organizationUnits);
        assertEquals(1, organizationUnits.size());
        verify(organizationUnitRepository, times(0)).findAllForQuerySalesPoint(any(), any(), any(), any());
        verify(organizationUnitRepository, times(0)).findAllForQuerySalesPointOrganizationUnit(any(), any());
        verify(organizationUnitRepository, times(1)).findAllForQuerySalesPointAdvisor(any(), any());

        verify(organizationUnitRepository, times(1)).findAllForQuerySalesPointAdvisor(1, "1");
    }

    @Test
    public void querySalesPoint_whenAllNull() {
        // given
        OrganizationUnitSearchCriteria criteria = new OrganizationUnitSearchCriteria();
        when(organizationUnitRepository.findAllForQuerySalesPointOrganizationUnit(any(), any())).thenReturn(Collections.singletonList(null));

        // when
        List<OrganizationUnit> organizationUnits = organizationUnitService.querySalesPoint(criteria);

        // then
        assertNotNull(organizationUnits);
        assertEquals(1, organizationUnits.size());
        verify(organizationUnitRepository, times(1)).findAllForQuerySalesPointOrganizationUnit(any(), any());
        verify(organizationUnitRepository, times(0)).findAllForQuerySalesPointAdvisor(any(), any());
        verify(organizationUnitRepository, times(0)).findAllForQuerySalesPoint(any(), any(), any(), any());

        verify(organizationUnitRepository, times(1)).findAllForQuerySalesPointOrganizationUnit(null, null);
    }
}