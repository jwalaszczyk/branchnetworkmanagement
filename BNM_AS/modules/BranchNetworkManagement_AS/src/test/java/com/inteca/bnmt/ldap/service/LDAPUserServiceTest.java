package com.inteca.bnmt.ldap.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.inteca.bnmt.ldap.dao.ADUserDao;
import com.inteca.bnmt.ldap.domain.LdapUser;
import com.inteca.bnmt.ldap.service.exceptions.LdapSearchException;
import com.inteca.bnmt.ldap.service.exceptions.LdapUpdateException;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class LDAPUserServiceTest {

    @Mock
    private ADUserDao ldapUserDao;
    
    @InjectMocks
    LDAPUserService service;

    @Test(expected=LdapSearchException.class)
    public void test_getAllChanged_exception() throws LdapSearchException {
        doThrow(new RuntimeException()).when(ldapUserDao).getAllChanged();
        service.getAllChanged();
    }
    
    @Test(expected=LdapSearchException.class)
    public void test_getAllRemoved_exception() throws LdapSearchException {
        doThrow(new RuntimeException()).when(ldapUserDao).getAllRemoved(anyString());
        service.getAllRemoved("");
    }
    
    @Test(expected=LdapSearchException.class)
    public void test_getUSers_exception() throws LdapSearchException {
        doThrow(new RuntimeException()).when(ldapUserDao).findByPesel(anyString());
        service.getUsers("");
    }
    
    @Test(expected=LdapUpdateException.class)
    public void test_updateUser_exception() throws LdapUpdateException {
        doThrow(new RuntimeException()).when(ldapUserDao).updateOperatorId(any());
        service.updateUser(null);
    }
}
