package com.inteca.bnmt.mediator;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import com.inteca.bnmt.ldap.domain.LdapUser;
import com.inteca.bnmt.ldap.service.LDAPUserService;
import com.inteca.bnmt.ldap.service.exceptions.LdapSearchException;
import com.inteca.bnmt.service.SyncDateResolver;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


@RunWith(MockitoJUnitRunner.class)
public class SyncLdapMediatorTest {
    
    @Mock
    private LDAPUserService externalUserService;
    @Mock
    private LDAPUserService internalUserService;
    
    @Mock 
    private SyncDateResolver dateResolver;
    
    @Mock
    private FederationChangeHadler hadler;
    
    @InjectMocks
    private SyncLdapMediator mediator;
    
    @Before
    public void init () {
        doReturn(ZonedDateTime.now().toString()).when(dateResolver).popLastExternalDate();
        doReturn(ZonedDateTime.now().toString()).when(dateResolver).popLastInternalDate();
    }
    
    @Test
    public void test_all_methods_finshed() throws LdapSearchException {
        mediator.syncAll();
        verify(externalUserService, times(1)).getAllChanged();
        verify(internalUserService, times(1)).getAllChanged();
        verify(externalUserService, times(1)).getAllRemoved(anyString());
        verify(internalUserService, times(1)).getAllRemoved(anyString());
    }
    
    @Test
    public void test_ext_change_return_user() throws LdapSearchException {
        mockUserChange(externalUserService , getUsers("user1"));
        mediator.syncAll();
        verify(hadler, times(1)).onUserChange(any());
        verify(externalUserService, times(2)).getAllChanged();
    }
    
    @Test
    public void test_ext_change_return_user_twice() throws LdapSearchException {
        mockUserChange(externalUserService , getUsers("user1"), getUsers("user2"));
        mediator.syncAll();
        verify(hadler, times(2)).onUserChange(any());
        verify(externalUserService, times(3)).getAllChanged();
    }
    
    @Test
    public void test_int_change_return_user() throws LdapSearchException {
        mockUserChange(internalUserService , getUsers("user1"));
        mediator.syncAll();
        verify(hadler, times(1)).onUserChange(any());
        verify(internalUserService, times(2)).getAllChanged();
    }
    
    @Test
    public void test_int_change_return_user_twice() throws LdapSearchException {
        mockUserChange(internalUserService , getUsers("user1"), getUsers("user2"));
        mediator.syncAll();
        verify(hadler, times(2)).onUserChange(any());
        verify(internalUserService, times(3)).getAllChanged();
    }
    
    @Test
    public void test_multi_changes_return_user() throws LdapSearchException {
        mockUserChange(internalUserService , getUsers("user1"));
        mockUserChange(externalUserService , getUsers("user2"));
        mediator.syncAll();
        verify(hadler, times(2)).onUserChange(any());
        verify(externalUserService, times(2)).getAllChanged();
        verify(internalUserService, times(2)).getAllChanged();
    }
    
    @Test
    public void test_ext_remove_return_user() throws LdapSearchException {
        mockUserRemove(externalUserService , getUsers("user1"));
        mediator.syncAll();
        verify(hadler, times(1)).onUserRemove(any());
        verify(externalUserService, times(2)).getAllRemoved(anyString());
    }
    
    @Test
    public void test_ext_remove_return_user_twice() throws LdapSearchException {
        mockUserRemove(externalUserService , getUsers("user1"), getUsers("user2"));
        mediator.syncAll();
        verify(hadler, times(2)).onUserRemove(any());
        verify(externalUserService, times(3)).getAllRemoved(anyString());
    }
    
    @Test
    public void test_int_remove_return_user() throws LdapSearchException {
        mockUserRemove(internalUserService , getUsers("user1"));
        mediator.syncAll();
        verify(hadler, times(1)).onUserRemove(any());
        verify(internalUserService, times(2)).getAllRemoved(anyString());
    }
    
    @Test
    public void test_int_remove_return_user_twice() throws LdapSearchException {
        mockUserRemove(internalUserService , getUsers("user1"), getUsers("user2"));
        mediator.syncAll();
        verify(hadler, times(2)).onUserRemove(any());
        verify(internalUserService, times(3)).getAllRemoved(anyString());
    }
    
    @Test
    public void test_multi_removes_return_user() throws LdapSearchException {
        mockUserRemove(internalUserService , getUsers("user1"));
        mockUserRemove(externalUserService , getUsers("user2"));
        mediator.syncAll();
        verify(hadler, times(2)).onUserRemove(any());
        verify(externalUserService, times(2)).getAllRemoved(anyString());
        verify(internalUserService, times(2)).getAllRemoved(anyString());
    }
    
    private void mockUserRemove(LDAPUserService service, List<LdapUser> ...usersLsts) throws LdapSearchException {
        when(service.getAllRemoved(anyString())).then(new Answer<List<LdapUser>>() {
            int index = 0;

            @Override
            public List<LdapUser> answer(InvocationOnMock invocation) throws Throwable {
                List<LdapUser> ret = index>=usersLsts.length ? new ArrayList<>(): usersLsts[index];
                index++;
                return ret;
            }
        });
    }

    private void mockUserChange(LDAPUserService service, List<LdapUser> ...usersLsts) throws LdapSearchException {
        when(service.getAllChanged()).then(new Answer<List<LdapUser>>() {
            int index = 0;

            @Override
            public List<LdapUser> answer(InvocationOnMock invocation) throws Throwable {
                List<LdapUser> ret = index>=usersLsts.length ? new ArrayList<>(): usersLsts[index];
                index++;
                return ret;
            }
        });
    }

    private List<LdapUser> getUsers(String ...names) {
        List<LdapUser> ret = new ArrayList<>();
        for(String name : names) {
            LdapUser u = new LdapUser();
            u.setName(name);
            u.setDn(name);
            ret.add(u);
        }
        return ret;
    }
}
