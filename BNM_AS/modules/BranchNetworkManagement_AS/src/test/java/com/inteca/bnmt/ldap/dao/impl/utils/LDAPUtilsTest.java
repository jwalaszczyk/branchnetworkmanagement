package com.inteca.bnmt.ldap.dao.impl.utils;

import java.util.HashSet;
import java.util.Set;

import javax.naming.InvalidNameException;

import org.junit.Test;

import com.inteca.bnmt.ldap.dao.impl.utils.LDAPutils;

import static org.junit.Assert.assertEquals;

public class LDAPUtilsTest {
	
	@Test(expected=NullPointerException.class)
	public void test_fromLdap_null() {
		LDAPutils.fromLdap(null);
	}
	
	@Test
	public void test_fromLdap_empty() {
		String result = LDAPutils.fromLdap("");
		assertEquals("", result);
	}
	
	@Test
	public void test_fromLdap_o() {
		String result = LDAPutils.fromLdap("o=lukas");
		assertEquals("lukas", result);
	}
	
	@Test
	public void test_fromLdap_O() {
		String result = LDAPutils.fromLdap("O=lukas");
		assertEquals("lukas", result);
	}
	
	@Test
	public void test_fromLdap_OU() {
		String result = LDAPutils.fromLdap("OU=lukas");
		assertEquals("lukas", result);
	}
	
	@Test
	public void test_fromLdap_cn() {
		String result = LDAPutils.fromLdap("cn=lukas");
		assertEquals("lukas", result);
	}
	
	@Test
	public void test_fromLdap_OU_O() {
		String result = LDAPutils.fromLdap("OU=lukas,o=com");
		assertEquals("lukas.com", result);
	}
	
	@Test
	public void test_fromLdap_OU_OU_O() {
		String result = LDAPutils.fromLdap("ou=grupa,OU=lukas,o=com");
		assertEquals("grupa.lukas.com", result);
	}
	
	@Test
	public void test_fromLdap_CN_OU_O() {
		String result = LDAPutils.fromLdap("cn=test,OU=lukas,o=com");
		assertEquals("test.lukas.com", result);
	}
	
	@Test(expected=ArrayIndexOutOfBoundsException.class)
	public void test_fromLdap_no_o() {
		LDAPutils.fromLdap("lukas");
	}
	
	@Test(expected=ArrayIndexOutOfBoundsException.class)
	public void test_fromLdap_wrong_syntax_at_end() {
		LDAPutils.fromLdap("ou=lukas,com");
	}
	
	@Test(expected=ArrayIndexOutOfBoundsException.class)
	public void test_fromLdap_wrong_syntax_at_beg() {
		LDAPutils.fromLdap("ukas,ou=com");
	}
	
	private Set<String> getSet(String ...strs) {
		Set<String> result = new HashSet<String>();
		for(String s : strs) {
			result.add(s);
		}
		return result;
	}
}
