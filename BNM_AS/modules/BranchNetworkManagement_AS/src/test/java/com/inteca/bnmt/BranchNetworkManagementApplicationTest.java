package com.inteca.bnmt;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.junit4.SpringRunner;


/**
 * @author ddedek
 * 27.07.2018
 */
@RunWith(SpringRunner.class)
@SpringBootTest(properties = "application.properties")
public class BranchNetworkManagementApplicationTest {

    @Test
    public void contextLoads() {
    }
}