package com.inteca.bnmt.repository;

import com.inteca.bnmt.BranchNetworkManagementApplication;
import com.inteca.bnmt.entity.*;
import org.joda.time.DateTime;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Random;
import java.util.UUID;
import java.util.stream.IntStream;

@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration(classes = BranchNetworkManagementApplication.class)
public class FeedDbMockDataTest {

    @Autowired
    private AdvisorRepository advisorRepository;

    @Autowired
    private OrganizationUnitRepository organizationUnitRepository;

    Random random = new Random(0);

    @Test
    @Ignore
    public void feedDbWithAdvisors() {
        for (int i = 0; i < 50; i++) {
            advisorRepository.save(createRandomAdvisor());
        }
    }

    @Test
    @Ignore
    public void feedDbWithOrganizationUnit() {
        System.out.println("feedDbWithOrganizationUnit");
        for (int i = 0; i < 30; i++) {
            organizationUnitRepository.save(createRandomOrganizationUnit());
        }
    }

    protected Advisor createRandomAdvisor() {
        return new Advisor()
            .withOperatorId(random.nextInt(1000000))
            .withFirstName("Imie " + random.nextInt(1000))
            .withSurname("Imie " + random.nextInt(1000))
            .withEmail("Mail " + random.nextInt(1000))
            .withMobileNumber(Integer.toString(random.nextInt(1100000000) + 100000000))
            .withStationaryNumber("62" + Integer.toString(random.nextInt(11000000) + 1000000))
            .withActive(random.nextBoolean())
            .withLogin("login" + random.nextInt(1000))
            .withDataSource("dt" + random.nextInt(1000))
            .withCode("code" + random.nextInt(1000))
            .withLastModified(new DateTime().minusDays(random.nextInt(100)))
            .withObjectGuid(UUID.randomUUID().toString())
            .addToFunctionalRole(IntStream.range(0, random.nextInt(4)).boxed()
                .map(x -> createRandomFunctionalRole()).toArray(FunctionalRole[]::new));
    }

    protected FunctionalRole createRandomFunctionalRole() {
        return new FunctionalRole()
            .withRoleId(random.nextInt(1000000))
            .withCode("code" + random.nextInt(1000))
            .withName("name" + random.nextInt(1000));
    }

    protected OrganizationUnit createRandomOrganizationUnit() {
        return new OrganizationUnit()
            .withBrcd("brcd" + random.nextInt(1000))
            .withChannel("channel" + random.nextInt(1000))
            .withCode("code" + random.nextInt(1000))
            .withLastModified(new DateTime())
            .withLegalForm(Integer.toString(random.nextInt(100)))
            .withName("name" + random.nextInt(1000))
            .withRegistrationAuthority("registrationAuthority" + random.nextInt(1000))
            .withStatus("status" + random.nextInt(1000))
            .withType("type" + random.nextInt(1000))
            .withTypeId(random.nextInt(1000))
            .withUnitId(Integer.toString(random.nextInt(1000)))
            .withUnitIdNumber(random.nextInt(1000))
            .withUnitLevel(random.nextInt(1000))
            .withSalesPointInformation(createRandomSalesPointInformation())
            .withAddress(createRandomAddress())
            .addToIdNumber(IntStream.range(0, random.nextInt(4)).boxed()
                .map(x -> createRandomIdNumber()).toArray(IdNumber[]::new))
            .addToContact(IntStream.range(0, random.nextInt(4)).boxed()
                .map(x -> createRandomContact()).toArray(Contact[]::new))
            .withParent(random.nextBoolean() ? createRandomOrganizationUnit() : null);
    }

    protected IdNumber createRandomIdNumber() {
        return new IdNumber()
            .withValue("value" + random.nextInt(1000))
            .withType("type" + random.nextInt(1000));
    }

    protected Contact createRandomContact() {
        return new Contact()
            .withType("type" + random.nextInt(1000))
            .withCountryPrefix("countryPrefix" + random.nextInt(1000))
            .withValue("value" + random.nextInt(1000));
    }

    protected SalesPointInformation createRandomSalesPointInformation() {
        return new SalesPointInformation()
            .withCrand("crand" + random.nextInt(1000))
            .withCareOfficer("careOfficer" + random.nextInt(1000))
            .withCashModule("cashModule" + random.nextInt(1000))
            .withCommisionReimbursmentInCost(random.nextBoolean())
            .withAccountingNoteIdentifier(random.nextBoolean())
            .withClientAccessDataFlag(random.nextBoolean())
            .withInvoiceBaseComissionPaid(random.nextBoolean())
            .withIsPokInPsp(random.nextBoolean())
            .withLeadingAssortmentGroup("leadingAssortmentGroup" + random.nextInt(1000))
            .withLeadingAssortmentGroupMrkt("leadingAssortmentGroupMrkt" + random.nextInt(1000))
            .withOwnerId("ownerId" + random.nextInt(1000))
            .withPrintGroup("printGroup" + random.nextInt(1000))
            .withSalesChannelType("salesChannelType" + random.nextInt(1000))
            .withSalesNetworkType("salesNetworkType" + random.nextInt(1000))
            .withSalesType("salesType" + random.nextInt(1000))
            .withTrustedPartner(random.nextBoolean());
    }

    protected Address createRandomAddress() {
        return new Address()
            .withCity("city" + random.nextInt(1000))
            .withCountry("country" + random.nextInt(1000))
            .withFlatNumber("flatNumber" + random.nextInt(1000))
            .withHouseNumber("houseNumber" + random.nextInt(1000))
            .withPostOffice("postOffice" + random.nextInt(1000))
            .withStreet("street" + random.nextInt(1000))
            .withStreetKind("streetKind" + random.nextInt(1000))
            .withZipCode("zipCode" + random.nextInt(1000));
    }
}
