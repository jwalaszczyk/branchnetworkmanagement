package com.inteca.bnmt.service;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.inteca.bnmt.dto.AdvisorsSearchCriteria;
import com.inteca.bnmt.entity.FunctionalRole;
import com.inteca.bnmt.entity.OrganizationUnit;
import com.inteca.bnmt.repository.AdvisorRepository;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class AdvisorServiceTest {

    @Mock
    private AdvisorRepository advisorRepository;
    
    @InjectMocks
    private AdvisorService service;
    
    AdvisorsSearchCriteria crit;
    
    @Before
    public void init() {
        crit = new AdvisorsSearchCriteria();
        crit.setFunctionalRoles(new ArrayList<>());
        crit.setOrganizationUnits(new ArrayList<>());
    }
    
    @Test(expected=NullPointerException.class)
    public void test_query_nullCriteria() {
        service.queryAdvisor(null);
    }
    
    @Test(expected=NullPointerException.class)
    public void test_query_emptyCriteria() {
        service.queryAdvisor(new AdvisorsSearchCriteria());
    }
    
    @Test
    public void test_query_emptyInitedCriteria() {
        service.queryAdvisor(crit);
        verify(advisorRepository, times(0)).findByOrganizationUnitUnitIdNumber(anyInt());
        verify(advisorRepository, times(0)).findByOrganizationUnitUnitIdNumberAndFunctionalRoleRoleIdIn(anyInt(), any());
    }
    
    @Test
    public void test_query_oneOU() {
        crit.getOrganizationUnits().add(new OrganizationUnit());
        service.queryAdvisor(crit);
        verify(advisorRepository, times(1)).findByOrganizationUnitUnitIdNumber(anyInt());
        verify(advisorRepository, times(0)).findByOrganizationUnitUnitIdNumberAndFunctionalRoleRoleIdIn(anyInt(), any());
    }
    
    @Test
    public void test_query_twoOU() {
        crit.getOrganizationUnits().add(new OrganizationUnit());
        crit.getOrganizationUnits().add(new OrganizationUnit());
        service.queryAdvisor(crit);
        verify(advisorRepository, times(2)).findByOrganizationUnitUnitIdNumber(anyInt());
        verify(advisorRepository, times(0)).findByOrganizationUnitUnitIdNumberAndFunctionalRoleRoleIdIn(anyInt(), any());
    }
    
    @Test
    public void test_query_oneOU_oneRole() {
        crit.getOrganizationUnits().add(new OrganizationUnit());
        crit.getFunctionalRoles().add(new FunctionalRole());
        service.queryAdvisor(crit);
        verify(advisorRepository, times(0)).findByOrganizationUnitUnitIdNumber(anyInt());
        verify(advisorRepository, times(1)).findByOrganizationUnitUnitIdNumberAndFunctionalRoleRoleIdIn(anyInt(), any());
    }
    
    @Test
    public void test_query_zeroOU_oneRole() {
        crit.getFunctionalRoles().add(new FunctionalRole());
        service.queryAdvisor(crit);
        verify(advisorRepository, times(0)).findByOrganizationUnitUnitIdNumber(anyInt());
        verify(advisorRepository, times(0)).findByOrganizationUnitUnitIdNumberAndFunctionalRoleRoleIdIn(anyInt(), any());
    }
    
    @Test
    public void test_query_oneOU_twoRoles() {
        crit.getOrganizationUnits().add(new OrganizationUnit());
        crit.getFunctionalRoles().add(new FunctionalRole());
        crit.getFunctionalRoles().add(new FunctionalRole());
        service.queryAdvisor(crit);
        verify(advisorRepository, times(0)).findByOrganizationUnitUnitIdNumber(anyInt());
        verify(advisorRepository, times(1)).findByOrganizationUnitUnitIdNumberAndFunctionalRoleRoleIdIn(anyInt(), any());
    }
    
    @Test
    public void test_query_twoOU_twoRoles() {
        crit.getOrganizationUnits().add(new OrganizationUnit());
        crit.getOrganizationUnits().add(new OrganizationUnit());
        crit.getFunctionalRoles().add(new FunctionalRole());
        crit.getFunctionalRoles().add(new FunctionalRole());
        service.queryAdvisor(crit);
        verify(advisorRepository, times(0)).findByOrganizationUnitUnitIdNumber(anyInt());
        verify(advisorRepository, times(2)).findByOrganizationUnitUnitIdNumberAndFunctionalRoleRoleIdIn(anyInt(), any());
    }
}
