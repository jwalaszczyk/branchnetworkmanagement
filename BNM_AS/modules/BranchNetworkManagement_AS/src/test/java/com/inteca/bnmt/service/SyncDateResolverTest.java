package com.inteca.bnmt.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.inteca.bnmt.entity.AdSyncData;
import com.inteca.bnmt.repository.AdSyncDataRepository;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class SyncDateResolverTest {
    
    @Mock
    private AdSyncDataRepository dataRepo;
    
    @InjectMocks
    private SyncDateResolver resolver;
    
    @Test
    public void test_first_time() {
        resolver.init();
        verify(dataRepo, times(2)).save(any(AdSyncData.class));
    }
    
    @Test
    public void test_second_time() {
        doReturn("someDate").when(dataRepo).findByKey(anyString());
        resolver.init();
        verify(dataRepo, times(0)).save(any(AdSyncData.class));
    }
    
    @Test
    public void test_second_time_pop() {
        doReturn("someDate").when(dataRepo).findByKey(anyString());
        resolver.init();
        String extRes = resolver.popLastExternalDate();
        String intRes = resolver.popLastInternalDate();
        assertEquals("someDate", extRes);
        assertEquals("someDate", intRes);
    }
    
    @Test
    public void test_first_time_pop() {
        resolver.init();
        String extRes = resolver.popLastExternalDate();
        String intRes = resolver.popLastInternalDate();
        assertTrue(extRes.matches("[0-9]{12}00.0[+,-]?[0-9]{4}"));
        assertTrue(intRes.matches("[0-9]{12}00.0[+,-]?[0-9]{4}"));
    }

    @Test
    public void test_pop_twice() {
        doReturn("someDate").when(dataRepo).findByKey(anyString());
        resolver.init();
        String extRes = resolver.popLastExternalDate();
        String extRes2 = resolver.popLastExternalDate();
        assertFalse(extRes.equals(extRes2));
    }
    
}
