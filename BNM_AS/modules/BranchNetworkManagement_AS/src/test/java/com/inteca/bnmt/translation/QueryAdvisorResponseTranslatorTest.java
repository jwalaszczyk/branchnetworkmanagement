package com.inteca.bnmt.translation;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.mapstruct.factory.Mappers;

import com.inteca.bnmt.entity.Advisor;
import com.inteca.bnmt.entity.FunctionalRole;
import com.inteca.bnmt.entity.OrganizationUnit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import pl.credit_agricole.schemas.bnmt._105._2018._08._001.AdvisorList;

public class QueryAdvisorResponseTranslatorTest {
    
    private QueryAdvisorResponseTranslator translator = Mappers.getMapper(QueryAdvisorResponseTranslator.class);
    
    @Test
    public void test_translate_null() {
        List<Advisor> advisors = null;
        AdvisorList result = translator.translate(advisors);
        assertNotNull(result);
    }
    
    @Test
    public void test_translate_empty() {
        List<Advisor> advisors = new ArrayList<Advisor>();
        AdvisorList result = translator.translate(advisors);
        assertNotNull(result);
    }
    
    @Test
    public void test_translate() {
        List<Advisor> advisors = new ArrayList<Advisor>();
        Advisor ad = new Advisor();
        ad.setSurname("Surname");
        advisors.add(ad);
        AdvisorList result = translator.translate(advisors);
        assertEquals(1, result.getAdvsr().size());
        assertEquals("Surname", result.getAdvsr().get(0).getSrnm());
        assertNotNull(result.getAdvsr().get(0).getFctnlRoleList());
        assertNotNull(result.getAdvsr().get(0).getFctnlRoleList().getFctnlRole());
        assertNotNull(result.getAdvsr().get(0).getSalesPtList());
        assertNotNull(result.getAdvsr().get(0).getSalesPtList().getSalesPt());
    }
    
    @Test
    public void test_translate_with_salespoint() {
        List<Advisor> advisors = new ArrayList<Advisor>();
        Advisor ad = new Advisor();
        OrganizationUnit unit = new OrganizationUnit();
        unit.setUnitIdNumber(101);
        ad.addToOrganizationUnit(unit);
        advisors.add(ad);
        AdvisorList result = translator.translate(advisors);
        assertEquals("101", result.getAdvsr().get(0).getSalesPtList().getSalesPt().get(0).getId());
    }
    
    @Test
    public void test_translate_with_ou_and_diff_primary() {
        List<Advisor> advisors = new ArrayList<Advisor>();
        Advisor ad = new Advisor();
        OrganizationUnit unit = new OrganizationUnit();
        unit.setUnitIdNumber(101);
        ad.addToOrganizationUnit(unit);
        ad.setPrimary(new OrganizationUnit());
        advisors.add(ad);
        AdvisorList result = translator.translate(advisors);
        assertFalse(result.getAdvsr().get(0).getSalesPtList().getSalesPt().get(0).getPmry());
    }
    
    @Test
    public void test_translate_with_ou_and_same_primary() {
        List<Advisor> advisors = new ArrayList<Advisor>();
        Advisor ad = new Advisor();
        OrganizationUnit unit = new OrganizationUnit();
        unit.setUnitIdNumber(101);
        ad.addToOrganizationUnit(unit);
        ad.setPrimary(unit);
        advisors.add(ad);
        AdvisorList result = translator.translate(advisors);
        assertTrue(result.getAdvsr().get(0).getSalesPtList().getSalesPt().get(0).getPmry());
    }

    @Test
    public void test_translate_with_role() {
        List<Advisor> advisors = new ArrayList<Advisor>();
        Advisor ad = new Advisor();
        FunctionalRole role = new FunctionalRole();
        role.setRoleId(102);
        ad.addToFunctionalRole(role );
        advisors.add(ad);
        AdvisorList result = translator.translate(advisors);
        assertEquals("102", result.getAdvsr().get(0).getFctnlRoleList().getFctnlRole().get(0).getId());
    }
}
