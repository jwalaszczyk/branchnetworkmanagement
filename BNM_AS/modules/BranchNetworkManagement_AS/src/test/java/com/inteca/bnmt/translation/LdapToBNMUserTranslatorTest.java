package com.inteca.bnmt.translation;

import com.inteca.bnmt.entity.Advisor;
import com.inteca.bnmt.ldap.domain.LdapUser;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mapstruct.factory.Mappers;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;


/**
 * @author ddedek
 * 08.08.2018
 */
@RunWith(MockitoJUnitRunner.class)
public class LdapToBNMUserTranslatorTest {

    @InjectMocks
    private LdapToBNMUserTranslator ldapToBNMUserTranslator = Mappers.getMapper(LdapToBNMUserTranslator.class);

    private LdapUser ldapUser;

    @Before
    public void populateMocks() {
        ldapUser = new LdapUser();
        initLdapUser();
    }

    private void initLdapUser() {

        ldapUser.setName("name");
        ldapUser.setSurname("surname");
        ldapUser.setLogin("login");
        ldapUser.setLoginDisabled(false);
        ldapUser.setOperatorId("1234");
        ldapUser.setSource("source");
        ldapUser.setObjectGUID("objectGUID");
        ldapUser.setMail("mail@mail.com");
        ldapUser.setMobile("123456789");
        ldapUser.setStationaryPhone("76454545");
        ldapUser.setLastModificationDate("20180710122300.0Z");

    }


    @Test
    public void testToAdvisorWithOperatorId() {
//        ARRANGE
        Advisor advisor = new Advisor();

//        ACT
        ldapToBNMUserTranslator.toAdvisor(ldapUser, advisor);

//        ASSERT
        assertEquals("name", advisor.getFirstName());
        assertEquals("surname", advisor.getSurname());
        assertEquals("login", advisor.getLogin());
        assertFalse(advisor.isActive());
        assertEquals(1234, advisor.getOperatorId());
        assertEquals("source", advisor.getDataSource());
        assertEquals("objectGUID", advisor.getObjectGuid());
        assertEquals("mail@mail.com", advisor.getEmail());
        assertEquals("123456789", advisor.getMobileNumber());
        assertEquals("76454545", advisor.getStationaryNumber());
        assertEquals(DateTime.parse("2018-07-10T12:23:00.000"), advisor.getLastModified());

    }

}