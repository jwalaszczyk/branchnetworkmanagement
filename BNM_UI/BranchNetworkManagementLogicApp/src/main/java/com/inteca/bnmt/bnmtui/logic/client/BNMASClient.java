package com.inteca.bnmt.bnmtui.logic.client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;
import pl.credit_agricole.schemas.bnmt._104._2018._08._001.Document;
import pl.credit_agricole.soap.bnmt._1._2015._09._001.BranchNetworkManagementASPortType;
import pl.credit_agricole.soap.bnmt._1._2015._09._001.FaultMessage;

/**
 * @author lszmolke
 */
@Component
public class BNMASClient {

    @Lazy
    @Autowired
    BranchNetworkManagementASPortType branchNetworkManagementASPortType;

    public pl.credit_agricole.schemas.bnmt._105._2018._08._001.Document queryAdvisorRequest(Document document) throws FaultMessage {
        return branchNetworkManagementASPortType.queryAdvisor(document);
    }

    public pl.credit_agricole.schemas.bnmt._103._2015._08._001.Document querySalesPoint(pl.credit_agricole.schemas.bnmt._102._2015._08._001.Document document) throws FaultMessage {
        return branchNetworkManagementASPortType.querySalesPoint(document);
    }

    public pl.credit_agricole.schemas.bnmt._107._2018._08._001.Document updateAdvisorPrimarySalesPoint(pl.credit_agricole.schemas.bnmt._106._2018._08._001.Document document) throws FaultMessage {
        return branchNetworkManagementASPortType.updateAdvisorPrimarySalesPoint(document);
    }
}
