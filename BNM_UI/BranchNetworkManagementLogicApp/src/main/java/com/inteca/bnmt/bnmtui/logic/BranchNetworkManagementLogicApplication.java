package com.inteca.bnmt.bnmtui.logic;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BranchNetworkManagementLogicApplication {

    public static void main(String[] args) {
        SpringApplication.run(BranchNetworkManagementLogicApplication.class, args);
    }
}
