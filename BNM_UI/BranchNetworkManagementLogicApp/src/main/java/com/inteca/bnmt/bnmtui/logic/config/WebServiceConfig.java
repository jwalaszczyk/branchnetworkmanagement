package com.inteca.bnmt.bnmtui.logic.config;

import com.inteca.bnmt.bnmtui.logic.interceptors.SleuthInInterceptor;
import com.inteca.bnmt.bnmtui.logic.interceptors.SleuthOutInterceptor;
import org.apache.cxf.interceptor.LoggingInInterceptor;
import org.apache.cxf.interceptor.LoggingOutInterceptor;
import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import pl.credit_agricole.soap.bnmt._1._2015._09._001.BranchNetworkManagementASPortType;

import javax.xml.namespace.QName;

/**
 * Configuration class for SOAP web services
 */
@Configuration
public class WebServiceConfig {

    @Autowired
    SleuthInInterceptor sleuthInInterceptor;

    @Autowired
    SleuthOutInterceptor sleuthOutInterceptor;

    @Bean
    @Lazy
    public BranchNetworkManagementASPortType branchNetworkManagementASPortType(
            @Value("${branchNetworkManagementAS.endpointAddress}") String address,
            @Value("${branchNetworkManagementAS.wsdlURL}") String wsdl,
            @Value("${branchNetworkManagementAS.serviceName}") String serviceName,
            @Value("${branchNetworkManagementAS.portName}") String portName
    ) {
        JaxWsProxyFactoryBean factory = new JaxWsProxyFactoryBean();
        factory.setServiceClass(BranchNetworkManagementASPortType.class);
        factory.setAddress(address);
        factory.setWsdlURL(wsdl);

        factory.getInInterceptors().add(new LoggingInInterceptor());
        factory.getOutInterceptors().add(new LoggingOutInterceptor());

        // TODO add keycloack
//        factory.getOutInterceptors().add(new SecurityContextInterceptor(samlProviderUrl, keycloakService));

        // TODO fix sleuth
//        setSleuth(factory);


        QName qServiceName = new QName(factory.getServiceName().getNamespaceURI(), serviceName);
        factory.setServiceName(qServiceName);
        QName qPortName = new QName(factory.getServiceName().getNamespaceURI(), portName);
        factory.setEndpointName(qPortName);

        BranchNetworkManagementASPortType client = (BranchNetworkManagementASPortType) factory.create();
        return client;
    }

    private void setSleuth(JaxWsProxyFactoryBean factory) {
        factory.getOutInterceptors().add(sleuthOutInterceptor);
        factory.getOutFaultInterceptors().add(sleuthOutInterceptor);
        factory.getInInterceptors().add(sleuthInInterceptor);
        factory.getInFaultInterceptors().add(sleuthInInterceptor);
    }
}
