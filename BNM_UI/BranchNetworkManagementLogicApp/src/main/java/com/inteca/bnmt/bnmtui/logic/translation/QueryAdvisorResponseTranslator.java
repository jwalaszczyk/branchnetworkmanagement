package com.inteca.bnmt.bnmtui.logic.translation;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import pl.credit_agricole.schemas.bnmt._105._2018._08._001.*;

/**
 * @author lszmolke
 */

@Mapper
public interface QueryAdvisorResponseTranslator {

    @Mappings({
            @Mapping(target = "advisorList.advisor", source = "qryAdvsrRspn.advsrList.advsr")
    })
    com.inteca.bnmt.bnmtui.logic.model.QueryAdvisorResponse toQueryAdvisorResponse(Document response);

    @Mappings({
            @Mapping(target = "email", source = "email"),
            @Mapping(target = "firstName", source = "frstNm"),
            @Mapping(target = "surname", source = "srnm"),
            @Mapping(target = "functionalRoleList", source = "fctnlRoleList"),
            @Mapping(target = "login", source = "lgn"),
            @Mapping(target = "mobileNumber", source = "mobNb"),
            @Mapping(target = "operatorId", source = "oprtrId"),
            @Mapping(target = "salesPointList", source = "salesPtList"),
            @Mapping(target = "stationaryNumber", source = "sttnryNb")
    })
    com.inteca.bnmt.bnmtui.logic.model.Advisor toAdvisor(Advisor advisor);

    @Mappings({
            @Mapping(target = "salesPoint", source = "salesPt")
    })
    com.inteca.bnmt.bnmtui.logic.model.SalesPointList2 toSalesPointList2(SalesPointList2 salesPointList2);

    @Mappings({
            @Mapping(target = "functionalRole", source = "fctnlRole")
    })
    com.inteca.bnmt.bnmtui.logic.model.FunctionalRoleList toFunctionalRoleList(FunctionalRoleList functionalRoleList);

    @Mappings({
            @Mapping(target = "id", source = "id"),
            @Mapping(target = "code", source = "cd"),
            @Mapping(target = "name", source = "nm")
    })
    com.inteca.bnmt.bnmtui.logic.model.FunctionalRole toFunctionalRole(FunctionalRole functionalRole);

    @Mappings({
            @Mapping(target = "name", source = "nm"),
            @Mapping(target = "primary", source = "pmry"),
            @Mapping(target = "id", source = "id"),
            @Mapping(target = "regionId", source = "rgnId"),
            @Mapping(target = "type", source = "tp"),
            @Mapping(target = "address", source = "adr"),
            @Mapping(target = "brcd", source = "brcd")
    })
    com.inteca.bnmt.bnmtui.logic.model.SalesPoint2 toSalesPoint2(SalesPoint2 salesPoint2);

    @Mappings({
            @Mapping(target = "city", source = "city"),
            @Mapping(target = "country", source = "ctry"),
            @Mapping(target = "flatNo", source = "flatNo"),
            @Mapping(target = "houseNo", source = "hsNo"),
            @Mapping(target = "postOffice", source = "pstOffc"),
            @Mapping(target = "street", source = "strt"),
            @Mapping(target = "streetKind", source = "strtKnd"),
            @Mapping(target = "zipCode", source = "zpCd")
    })
    com.inteca.bnmt.bnmtui.logic.model.Address toSalesPoint2(Address salesPoint2);
}
