package com.inteca.bnmt.bnmtui.logic.mediator;

import com.inteca.bnmt.bnmtui.logic.client.BNMASClient;
import com.inteca.bnmt.bnmtui.logic.model.*;
import com.inteca.bnmt.bnmtui.logic.translation.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author lszmolke
 */

@Component
public class BNMASMediator {

    private static Logger log = LoggerFactory.getLogger(BNMASMediator.class);

    @Autowired
    BNMASClient bnmasClient;

    @Autowired
    QueryAdvisorRequestTranslator queryAdvisorRequestTranslator;

    @Autowired
    QueryAdvisorResponseTranslator queryAdvisorResponseTranslator;

    @Autowired
    QuerySalesPointRequestTranslator querySalesPointRequestTranslator;

    @Autowired
    QuerySalesPointResponseTranslator querySalesPointResponseTranslator;

    @Autowired
    UpdateAdvisorPrimarySalesPointRequestTranslator updateAdvisorPrimarySalesPointRequestTranslator;

    @Autowired
    UpdateAdvisorPrimarySalesPointResponseTranslator updateAdvisorPrimarySalesPointResponseTranslator;

    public QueryAdvisorResponse queryAdvisor(QueryAdvisorRequest request) {
        try {
            // translate request
            pl.credit_agricole.schemas.bnmt._104._2018._08._001.Document translatedRequest = queryAdvisorRequestTranslator.toQueryAdvisorRequest(request);
            // call
            pl.credit_agricole.schemas.bnmt._105._2018._08._001.Document response = bnmasClient.queryAdvisorRequest(translatedRequest);
            // translate response
            return queryAdvisorResponseTranslator.toQueryAdvisorResponse(response);
        } catch (Throwable e) {
            log.error("Unknown error while invoking queryAdvisor.", e);

            //TODO throw proper exception
        }

        //TODO throw proper exception
        return null;
    }

    public QuerySalesPointResponse querySalesPoint(QuerySalesPointRequest request) {
        try {
            // translate request
            pl.credit_agricole.schemas.bnmt._102._2015._08._001.Document translatedRequest = querySalesPointRequestTranslator.toQuerySalesPoint(request);
            // call
            pl.credit_agricole.schemas.bnmt._103._2015._08._001.Document response = bnmasClient.querySalesPoint(translatedRequest);
            // translate response
            return querySalesPointResponseTranslator.toQueryAdvisorResponse(response);
        } catch (Throwable e) {
            log.error("Unknown error while invoking queryAdvisor.", e);

            //TODO throw proper exception
        }

        //TODO throw proper exception
        return null;
    }

    public UpdateAdvisorPrimarySalesPointResponse updateAdvisorPrimarySalesPoint(UpdateAdvisorPrimarySalesPointRequest request) {
        try {
            // translate request
            pl.credit_agricole.schemas.bnmt._106._2018._08._001.Document translatedRequest = updateAdvisorPrimarySalesPointRequestTranslator.toUpdateAdvisorPrimarySalesPoint(request);
            // call
            pl.credit_agricole.schemas.bnmt._107._2018._08._001.Document response = bnmasClient.updateAdvisorPrimarySalesPoint(translatedRequest);
            // translate response
            return updateAdvisorPrimarySalesPointResponseTranslator.updateAdvisorPrimarySalesPointResponseTranslator(response);
        } catch (Throwable e) {
            log.error("Unknown error while invoking queryAdvisor.", e);

            //TODO throw proper exception
        }

        //TODO throw proper exception
        return null;
    }
}
