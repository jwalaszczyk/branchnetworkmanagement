package com.inteca.bnmt.bnmtui.logic.translation;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.springframework.beans.factory.annotation.Value;
import pl.credit_agricole.schemas.bnmt._102._2015._08._001.Advisor1;
import pl.credit_agricole.schemas.bnmt._102._2015._08._001.Document;
import pl.credit_agricole.schemas.bnmt._102._2015._08._001.SalesPoint3;

/**
 * @author lszmolke
 */

@Mapper
public abstract class QuerySalesPointRequestTranslator {

    @Value("${spring.application.name}")
    String appName;

    @Mappings({
            @Mapping(target = "qrySalesPtReq.bzCtx", expression = "java(generateBizCtx())"),
            @Mapping(target = "qrySalesPtReq.advsr", source = "advisor"),
            @Mapping(target = "qrySalesPtReq.salesPt",source = "salesPoint")
    })
    public abstract Document toQuerySalesPoint(com.inteca.bnmt.bnmtui.logic.model.QuerySalesPointRequest request);

    @Mappings({
            @Mapping(target = "id", source = "id"),
            @Mapping(target = "brcd", source = "brcd")
    })
    public abstract SalesPoint3 toSalesPoint3(com.inteca.bnmt.bnmtui.logic.model.SalesPoint3 salesPoint3);

    @Mappings({
            @Mapping(target = "lgn", source = "login"),
            @Mapping(target = "oprtrId", source = "operatorId")
    })
    public abstract Advisor1 toAdvisor1(com.inteca.bnmt.bnmtui.logic.model.Advisor1 advisor1);

    public pl.credit_agricole.schemas.bnmt._102._2015._08._001.BusinessCtxType generateBizCtx() {
        pl.credit_agricole.schemas.bnmt._102._2015._08._001.BusinessCtxType bizCtx = new pl.credit_agricole.schemas.bnmt._102._2015._08._001.BusinessCtxType();
        bizCtx.setBsnsMsgId(appName + java.util.UUID.randomUUID());

        bizCtx.setChnl("SYS");
        bizCtx.setUnvrs("LUCF");
        return bizCtx;
    }
}
