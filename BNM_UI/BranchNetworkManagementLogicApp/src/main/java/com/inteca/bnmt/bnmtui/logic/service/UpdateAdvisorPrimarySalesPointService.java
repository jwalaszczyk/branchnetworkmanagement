package com.inteca.bnmt.bnmtui.logic.service;

import com.inteca.bnmt.bnmtui.logic.mediator.BNMASMediator;
import com.inteca.bnmt.bnmtui.logic.model.UpdateAdvisorPrimarySalesPointRequest;
import com.inteca.bnmt.bnmtui.logic.model.UpdateAdvisorPrimarySalesPointResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author lszmolke
 */

@Service
public class UpdateAdvisorPrimarySalesPointService {
    @Autowired
    BNMASMediator bnmasMediator;

    public UpdateAdvisorPrimarySalesPointResponse updateAdvisorPrimarySalesPoint(UpdateAdvisorPrimarySalesPointRequest request) {
        return bnmasMediator.updateAdvisorPrimarySalesPoint(request);
    }
}
