package com.inteca.bnmt.bnmtui.logic.interceptors;

import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.message.Message;
import org.apache.cxf.phase.AbstractPhaseInterceptor;
import org.apache.cxf.phase.Phase;
import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.cloud.sleuth.Span;
//import org.springframework.cloud.sleuth.Tracer;
import org.springframework.stereotype.Service;

@Service
public class SleuthInInterceptor extends AbstractPhaseInterceptor<Message> {

//    @Autowired
//    private Tracer tracer;

    public SleuthInInterceptor() {
        super(Phase.PRE_INVOKE);
    }

    @Override
    public void handleMessage(Message message) throws Fault {
//        final Span currentSpan = tracer.getCurrentSpan();
//        currentSpan.logEvent(Span.CLIENT_RECV);
//        tracer.close(currentSpan);
    }
}
