package com.inteca.bnmt.bnmtui.logic.translation;

import com.inteca.bnmt.bnmtui.logic.model.Address;
import com.inteca.bnmt.bnmtui.logic.model.QuerySalesPointResponse;
import com.inteca.bnmt.bnmtui.logic.model.SalesPoint;
import com.inteca.bnmt.bnmtui.logic.model.SalesPointList;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

/**
 * @author lszmolke
 */
@Mapper
public interface QuerySalesPointResponseTranslator {

    @Mappings({
            @Mapping(target = "salesPointList", source = "qrySalesPtRspn.salesPtList")
    })
    QuerySalesPointResponse toQueryAdvisorResponse(pl.credit_agricole.schemas.bnmt._103._2015._08._001.Document response);

    @Mappings({
            @Mapping(target = "salesPoint", source = "salesPt")
    })
    SalesPointList toSalesPointList(pl.credit_agricole.schemas.bnmt._103._2015._08._001.SalesPointList salesPointList);

    @Mappings({
            @Mapping(target = "brcd", source = "brcd"),
            @Mapping(target = "id", source = "id"),
            @Mapping(target = "type", source = "tp"),
            @Mapping(target = "regionId", source = "rgnId"),
            @Mapping(target = "name", source = "nm"),
            @Mapping(target = "address", source = "adr"),
            @Mapping(target = "channel", source = "chanl")
    })
    SalesPoint toSalesPoint(pl.credit_agricole.schemas.bnmt._103._2015._08._001.SalesPoint salesPoint);

    @Mappings({
            @Mapping(target = "city", source = "city"),
            @Mapping(target = "country", source = "ctry"),
            @Mapping(target = "flatNo", source = "flatNo"),
            @Mapping(target = "houseNo", source = "hsNo"),
            @Mapping(target = "postOffice", source = "pstOffc"),
            @Mapping(target = "street", source = "strt"),
            @Mapping(target = "streetKind", source = "strtKnd"),
            @Mapping(target = "zipCode", source = "zpCd")
    })
    Address toAddress(pl.credit_agricole.schemas.bnmt._103._2015._08._001.Address address);
}
