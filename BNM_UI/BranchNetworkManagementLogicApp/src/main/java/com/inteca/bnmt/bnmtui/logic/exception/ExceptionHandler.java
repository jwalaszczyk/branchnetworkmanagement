package com.inteca.bnmt.bnmtui.logic.exception;

import com.inteca.bnmt.bnmtui.logic.facade.ApiResponseMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;

/**
 * @author lszmolke
 */

@ControllerAdvice
public class ExceptionHandler {

    private static Logger log = LoggerFactory.getLogger(ExceptionHandler.class);

    @org.springframework.web.bind.annotation.ExceptionHandler
    public ResponseEntity<ApiResponseMessage> handleThrowable(Throwable e) {
        log.error("Unknown exception occured.", e);
        ApiResponseMessage responseMessage = new ApiResponseMessage(ApiResponseMessage.ERROR, "Unknown exception occured.");
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(responseMessage);
    }
}
