package com.inteca.bnmt.bnmtui.logic.translation;

import com.inteca.bnmt.bnmtui.logic.model.ResponseStatus;
import com.inteca.bnmt.bnmtui.logic.model.UpdateAdvisorPrimarySalesPointResponse;
import com.inteca.bnmt.bnmtui.logic.model.Warning;
import com.inteca.bnmt.bnmtui.logic.model.WarningList;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import pl.credit_agricole.schemas.bnmt._107._2018._08._001.Document;

/**
 * @author lszmolke
 */

@Mapper
public interface UpdateAdvisorPrimarySalesPointResponseTranslator {

    @Mappings({
            @Mapping(target = "responseStatus", source = "updAdvsrPmrySalesPtRspn.rspnSts")
    })
    UpdateAdvisorPrimarySalesPointResponse updateAdvisorPrimarySalesPointResponseTranslator(Document response);

    @Mappings({
            @Mapping(target = "status", source = "sts"),
            @Mapping(target = "wrngList", source = "wrngList")
    })
    ResponseStatus toResponseStatus(pl.credit_agricole.schemas.bnmt._107._2018._08._001.ResponseStatus responseStatus);

    @Mappings({
            @Mapping(target = "wrng", source = "wrng")
    })
    WarningList toWarningList(pl.credit_agricole.schemas.bnmt._107._2018._08._001.WarningList warningList);

    @Mappings({
            @Mapping(target = "wrngAttr", source = "wrngAttr"),
            @Mapping(target = "wrngCd", source = "wrngCd"),
            @Mapping(target = "wrngClss", source = "wrngClss"),
            @Mapping(target = "wrngDesc", source = "wrngDsc"),
            @Mapping(target = "wrngElmt", source = "wrngElmt"),
            @Mapping(target = "wrngVal", source = "wrngVl")
    })
    Warning toWarning(pl.credit_agricole.schemas.bnmt._107._2018._08._001.Warning warning);
}
