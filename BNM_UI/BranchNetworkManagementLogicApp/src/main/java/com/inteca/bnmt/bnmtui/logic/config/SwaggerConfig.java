package com.inteca.bnmt.bnmtui.logic.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * @author lszmolke
 */

@Configuration
@ComponentScan(basePackages = "io.swagger.configuration")
public class SwaggerConfig {
}
