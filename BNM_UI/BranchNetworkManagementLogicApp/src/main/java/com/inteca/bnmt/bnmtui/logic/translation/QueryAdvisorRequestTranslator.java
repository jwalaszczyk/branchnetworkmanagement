package com.inteca.bnmt.bnmtui.logic.translation;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.springframework.beans.factory.annotation.Value;
import pl.credit_agricole.schemas.bnmt._104._2018._08._001.*;

/**
 * @author lszmolke
 */
@Mapper
public abstract class QueryAdvisorRequestTranslator {

    @Value("${spring.application.name}")
    String appName;

    @Mappings({
            @Mapping(target = "qryAdvsrReq.bzCtx", expression = "java(generateBizCtx())"),
            @Mapping(target = "qryAdvsrReq.fctnlRoleList", source = "functionalRoleList"),
            @Mapping(target = "qryAdvsrReq.salesPtList", source = "salesPointList")
    })
    public abstract Document toQueryAdvisorRequest(com.inteca.bnmt.bnmtui.logic.model.QueryAdvisorRequest request);

    @Mappings({
            @Mapping(target = "salesPt", source = "salesPoint")
    })
    public abstract SalesPointList1 toSalesPointList(com.inteca.bnmt.bnmtui.logic.model.SalesPointList1 salesPointList1);

    @Mappings({
            @Mapping(target = "id", source = "id")
    })
    public abstract SalesPoint1 toSalesPoint1(com.inteca.bnmt.bnmtui.logic.model.SalesPoint1 salesPoint1);

    @Mappings({
            @Mapping(target = "fctnlRole", source = "functionalRole")
    })
    public abstract FunctionalRoleList toFunctionalRoleList(com.inteca.bnmt.bnmtui.logic.model.FunctionalRoleList functionalRoleList);

    @Mappings({
            @Mapping(target = "id", source = "id"),
            @Mapping(target = "cd", source = "code"),
            @Mapping(target = "nm", source = "name")
    })
    public abstract FunctionalRole toFunctionalRole(com.inteca.bnmt.bnmtui.logic.model.FunctionalRole functionalRole);

    public pl.credit_agricole.schemas.bnmt._104._2018._08._001.BusinessCtxType generateBizCtx() {
        pl.credit_agricole.schemas.bnmt._104._2018._08._001.BusinessCtxType bizCtx = new pl.credit_agricole.schemas.bnmt._104._2018._08._001.BusinessCtxType();
        bizCtx.setBsnsMsgId(appName + java.util.UUID.randomUUID());

        bizCtx.setChnl("SYS");
        bizCtx.setUnvrs("LUCF");
        return bizCtx;
    }
}
