package com.inteca.bnmt.bnmtui.logic.translation;

import com.inteca.bnmt.bnmtui.logic.model.UpdateAdvisorPrimarySalesPointRequest;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.springframework.beans.factory.annotation.Value;
import pl.credit_agricole.schemas.bnmt._106._2018._08._001.Advisor2;
import pl.credit_agricole.schemas.bnmt._106._2018._08._001.Document;
import pl.credit_agricole.schemas.bnmt._106._2018._08._001.SalesPoint1;

/**
 * @author lszmolke
 */

@Mapper
public abstract class UpdateAdvisorPrimarySalesPointRequestTranslator {

    @Value("${spring.application.name}")
    String appName;

    @Mappings({
            @Mapping(target = "updAdvsrPmrySalesPtReq.bzCtx", expression = "java(generateBizCtx())"),
            @Mapping(target = "updAdvsrPmrySalesPtReq.advsr", source = "advisor")
    })
    public abstract Document toUpdateAdvisorPrimarySalesPoint(UpdateAdvisorPrimarySalesPointRequest request);

    @Mappings({
            @Mapping(target = "oprtrId", source = "operatorId"),
            @Mapping(target = "pmrySalesPt", source = "primarySalesPoint")
    })
    public abstract Advisor2 toUpdateAdvisor2(com.inteca.bnmt.bnmtui.logic.model.Advisor2 advisor2);

    @Mappings({
            @Mapping(target = "id", source = "id")
    })
    public abstract SalesPoint1 toSalesPoint1(com.inteca.bnmt.bnmtui.logic.model.SalesPoint1 salesPoint1);

    public pl.credit_agricole.schemas.bnmt._106._2018._08._001.BusinessCtxType generateBizCtx() {
        pl.credit_agricole.schemas.bnmt._106._2018._08._001.BusinessCtxType bizCtx = new pl.credit_agricole.schemas.bnmt._106._2018._08._001.BusinessCtxType();
        bizCtx.setBsnsMsgId(appName + java.util.UUID.randomUUID());

        bizCtx.setChnl("SYS");
        bizCtx.setUnvrs("LUCF");
        return bizCtx;
    }
}
