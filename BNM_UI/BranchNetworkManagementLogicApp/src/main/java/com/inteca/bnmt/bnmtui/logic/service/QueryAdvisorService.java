package com.inteca.bnmt.bnmtui.logic.service;

import com.inteca.bnmt.bnmtui.logic.mediator.BNMASMediator;
import com.inteca.bnmt.bnmtui.logic.model.QueryAdvisorRequest;
import com.inteca.bnmt.bnmtui.logic.model.QueryAdvisorResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author lszmolke
 */

@Service
public class QueryAdvisorService {
    @Autowired
    BNMASMediator bnmasMediator;

    public QueryAdvisorResponse queryAdvisor(QueryAdvisorRequest request) {
        return bnmasMediator.queryAdvisor(request);
    }
}
