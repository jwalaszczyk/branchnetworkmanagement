package com.inteca.bnmt.bnmtui.logic.facade;

import com.inteca.bnmt.bnmtui.logic.model.*;
import com.inteca.bnmt.bnmtui.logic.service.QueryAdvisorService;
import com.inteca.bnmt.bnmtui.logic.service.QuerySalesPointService;
import com.inteca.bnmt.bnmtui.logic.service.UpdateAdvisorPrimarySalesPointService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * @author lszmolke
 */

@RestController
@CrossOrigin
public class BNMLogicFacade implements BnmlogicApi {
    private static Logger log = LoggerFactory.getLogger(BNMLogicFacade.class);

    @Autowired
    QueryAdvisorService queryAdvisorService;

    @Autowired
    QuerySalesPointService querySalesPointService;

    @Autowired
    UpdateAdvisorPrimarySalesPointService updateAdvisorPrimarySalesPointService;

    @Override
    public ResponseEntity<QueryAdvisorResponse> queryAdvisor(@Valid @RequestBody QueryAdvisorRequest request) {
        log.info("START - queryAdvisor");
        QueryAdvisorResponse response = queryAdvisorService.queryAdvisor(request);
        log.info("STOP - queryAdvisor");
        return ResponseEntity.ok(response);
    }

    @Override
    public ResponseEntity<QuerySalesPointResponse> querySalesPoint(@Valid @RequestBody QuerySalesPointRequest request) {
        log.info("START - querySalesPoint");
        QuerySalesPointResponse response = querySalesPointService.querySalesPoint(request);
        log.info("STOP - querySalesPoint");
        return ResponseEntity.ok(response);
    }

    @Override
    public ResponseEntity<UpdateAdvisorPrimarySalesPointResponse> updateAdvisorPrimarySalesPoint(@Valid @RequestBody UpdateAdvisorPrimarySalesPointRequest request) {
        log.info("START - toUpdateAdvisorPrimarySalesPoint");
        UpdateAdvisorPrimarySalesPointResponse response = updateAdvisorPrimarySalesPointService.updateAdvisorPrimarySalesPoint(request);
        log.info("STOP - toUpdateAdvisorPrimarySalesPoint");
        return ResponseEntity.ok(response);
    }
}
