package com.inteca.bnmt.bnmtui.logic.service;

import com.inteca.bnmt.bnmtui.logic.mediator.BNMASMediator;
import com.inteca.bnmt.bnmtui.logic.model.QuerySalesPointRequest;
import com.inteca.bnmt.bnmtui.logic.model.QuerySalesPointResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author lszmolke
 */
@Service
public class QuerySalesPointService {
    @Autowired
    BNMASMediator bnmasMediator;

    public QuerySalesPointResponse querySalesPoint(QuerySalesPointRequest request) {
        return bnmasMediator.querySalesPoint(request);
    }
}
