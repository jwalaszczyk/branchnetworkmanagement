package com.inteca.branchnetworkmanagementui;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BranchNetworkManagementUIApp {

	public static void main(String[] args) {
		SpringApplication.run(BranchNetworkManagementUIApp.class, args);
	}
}
