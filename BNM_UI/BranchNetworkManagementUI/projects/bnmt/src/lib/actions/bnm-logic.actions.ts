/**
 * @author lszmolke
 * */
import {createActions} from 'ngrx-normalizr-crud';
import {queryAdvisorRequestSchema, queryAdvisorResponseSchema} from "../reducers/store-schemas";
import {QueryAdvisorRequest, QueryAdvisorResponse} from "../../../../bnmt-logic-interfaces/src/lib";

export const queryAdvisorRequestActions = createActions<QueryAdvisorRequest>(queryAdvisorRequestSchema);
export const queryAdvisorResponseActions = createActions<QueryAdvisorResponse>(queryAdvisorResponseSchema);

