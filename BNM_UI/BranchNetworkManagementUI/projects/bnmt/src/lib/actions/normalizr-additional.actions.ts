import { Action } from "@ngrx/store";

export class NormalizedAdditionalActionsTypes {
  static readonly CLEAR_NORMALIZED_STORE = "[Normalizr] Clear store";
}

export class ClearNormalizedStore implements Action {
  readonly type = NormalizedAdditionalActionsTypes.CLEAR_NORMALIZED_STORE;
}

export type Actions = ClearNormalizedStore;