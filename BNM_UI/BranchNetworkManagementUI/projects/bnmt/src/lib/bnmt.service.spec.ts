import { TestBed, inject } from '@angular/core/testing';

import { BnmtService } from './bnmt.service';

describe('BnmtService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BnmtService]
    });
  });

  it('should be created', inject([BnmtService], (service: BnmtService) => {
    expect(service).toBeTruthy();
  }));
});
