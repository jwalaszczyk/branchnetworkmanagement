/**
 * @author lszmolke
 * */
import {Injectable} from "@angular/core";
import {LibConfig} from "../../config/lib.config";

@Injectable()
export class ExternalEndpointsConfig {
  public bnmLogicEndpoint = LibConfig.config.bnmLogicEndpoint;
}
