/**
 * @author lszmolke
 * */
import {Injectable} from "@angular/core";
import {DefaultService, QueryAdvisorRequest, QueryAdvisorResponse} from "../../../../../bnmt-logic-interfaces/src/lib";
import {ExternalEndpointsConfig} from "./external-endpoints.config";
import {Observable} from "rxjs";

@Injectable()
export class BnmLogicService {
  constructor(private bnmLogicClient: DefaultService, private endpointConfig: ExternalEndpointsConfig) {
    bnmLogicClient.basePath = endpointConfig.bnmLogicEndpoint;

    console.warn(bnmLogicClient.basePath);
  }

  public queryAdvisor(request: QueryAdvisorRequest): Observable<QueryAdvisorResponse> {
    return this.bnmLogicClient.queryAdvisor(request);
  }
}
