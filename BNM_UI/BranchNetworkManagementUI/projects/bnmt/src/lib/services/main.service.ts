/**
 * @author lszmolke
 * */
import {QueryAdvisorRequest} from "../../../../bnmt-logic-interfaces/src/lib";
import {Store} from "@ngrx/store";
import * as fromMainState from "../reducers/main-state";
import {Injectable} from "@angular/core";
import {queryAdvisorRequestActions, queryAdvisorResponseActions} from "../actions/bnm-logic.actions";
import * as fromMainSelectors from "../reducers/main.selectors";

@Injectable()
export class MainService {
  constructor(private store: Store<fromMainState.State>) {
    this.store.select(fromMainSelectors.entitySelectors.getQuery).subscribe(c => console.warn("test"));
  }

  public queryAdvisor(request: QueryAdvisorRequest): void {
    let action = new queryAdvisorResponseActions.Search(request);
    console.warn("TESts");
    this.store.dispatch(action);
  }
}
