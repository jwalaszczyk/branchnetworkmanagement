import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BnmtComponent } from './bnmt.component';

describe('BnmtComponent', () => {
  let component: BnmtComponent;
  let fixture: ComponentFixture<BnmtComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BnmtComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BnmtComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
