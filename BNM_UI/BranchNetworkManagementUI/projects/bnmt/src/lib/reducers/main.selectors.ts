/**
 * @author lszmolke
 * */
import {createFeatureSelector, createSelector, MemoizedSelector} from "@ngrx/store";
import * as mainState from "./main-state";
import {createSelectors, NormalizedEntityState} from "ngrx-normalizr-crud";
import {QueryAdvisorResponse} from "../../../../bnmt-logic-interfaces/src/lib";
import {queryAdvisorResponseSchema} from "./store-schemas";

const getFeatureState = createFeatureSelector<mainState.FeatureState>('bnmt');
const getQueryAdvisorResponseState: MemoizedSelector<object, NormalizedEntityState['query']> = createSelector(getFeatureState, (state: mainState.FeatureState) => state.queryAdvisorResponseState);

export const entitySelectors = {
  ...createSelectors<QueryAdvisorResponse>(queryAdvisorResponseSchema, getQueryAdvisorResponseState)
}
