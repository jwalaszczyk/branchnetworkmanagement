/**
 * @author lszmolke
 * */
import {NormalizedState} from "ngrx-normalizr";
import * as fromQueryAdvisorState from "./query-advisor-response.reducer";

export interface State {
  bnmt: FeatureState;
}

export interface FeatureState extends NormalizedState {
  queryAdvisorResponseState: fromQueryAdvisorState.State
}
