/**
 * @author lszmolke
 * */

import {createReducer, initialEntityState, NormalizedEntityState} from "ngrx-normalizr-crud";
import {queryAdvisorResponseSchema} from "./store-schemas";
import {QueryAdvisorResponse} from "../../../../bnmt-logic-interfaces/src/lib";
import {NormalizedState} from "ngrx-normalizr";
import {queryAdvisorResponseActions} from "../actions/bnm-logic.actions";

const queryAdvisorResponseReducer = createReducer(queryAdvisorResponseSchema);

export interface State extends NormalizedState {
  queryAdvisorResponse: QueryAdvisorResponse | null;
}

export function reducer(state: NormalizedEntityState = initialEntityState, action: any) {
  // do any reducer logic for your state or in this case
  // simply return the result of the entity reducer
  console.warn("Add to queryAdvisorResponse. State: "+JSON.stringify(state)+". Action = "+JSON.stringify(action));

  switch(action.type) {
    case queryAdvisorResponseActions.SEARCH_COMPLETE: {
      return queryAdvisorResponseReducer(action.payload, action);
    }

    default: {
      return state;
    }
  }

}
