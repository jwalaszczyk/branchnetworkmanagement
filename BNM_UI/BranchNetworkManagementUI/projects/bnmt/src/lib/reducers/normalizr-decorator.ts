import cloneDeep from 'lodash-es/cloneDeep';
import { NormalizeActionTypes, NormalizedEntityState, NormalizeRemoveChildActionPayload, NormalizeChildActionPayload } from 'ngrx-normalizr';
import { NormalizedAdditionalActionsTypes } from '../actions/normalizr-additional.actions';
import { normalized } from 'ngrx-normalizr';

export function reducerDecorator(state: NormalizedEntityState, action: any) {
  switch(action.type) {
    case NormalizeActionTypes.REMOVE_DATA: {
      const { id, key, removeChildren } = action.payload;
      const entities = cloneDeep(state.entities);
      const entity = entities[key][id];

      if (!entity) {
        return state;
      }

      if (removeChildren) {
        Object.entries(removeChildren).map(
          ([key, entityProperty]: [string, string]) => {
            const child = entity[entityProperty];
            if (child && entities[key]) {
              const ids = Array.isArray(child) ? child : [child];
              ids.forEach((oldId: string) => delete entities[key][oldId]);
            }
          }
        );
      }

      delete entities[key][id];

      return {
        result: state.result,
        entities
      };
    }
    case NormalizeActionTypes.REMOVE_CHILD_DATA: {
      const {
        id,
        childSchemaKey,
        parentProperty,
        parentSchemaKey,
        parentId
      } = action.payload as NormalizeRemoveChildActionPayload;
      const newEntities = cloneDeep(state.entities);
      const entity = newEntities[childSchemaKey][id];

      if (!entity) {
        return state;
      }

      const parentRefs = getParentReferences(newEntities, action.payload);
      if (parentRefs && parentRefs.indexOf(id) > -1) {
        newEntities[parentSchemaKey][parentId][parentProperty].splice(
          parentRefs.indexOf(id),
          1
        );
      }

      delete newEntities[childSchemaKey][id];

      return {
        ...state,
        entities: newEntities
      };
    }
    case NormalizeActionTypes.ADD_CHILD_DATA: {
      const {
        result,
        entities,
        parentSchemaKey,
        parentProperty,
        parentId
      } = action.payload as NormalizeChildActionPayload;
      const newEntities = cloneDeep(state.entities);

      /* istanbul ignore else */
      if (getParentReferences(newEntities, action.payload)) {
        newEntities[parentSchemaKey][parentId][parentProperty].push(...result);
      }

      return {
        result,
        entities: Object.keys(entities).reduce((p: any, c: string) => {
          p[c] = { ...p[c], ...entities[c] };
          return p;
        }, newEntities)
      };
    }
    case NormalizedAdditionalActionsTypes.CLEAR_NORMALIZED_STORE: {
      return {
        result: [],
        entities: {}
      };
    }
    default:
      return normalized(state, action);
  }
}

function getParentReferences(
  entities: any,
  payload: NormalizeChildActionPayload
): string | undefined {
  const { parentSchemaKey, parentProperty, parentId } = payload;
  if (
    entities[parentSchemaKey] &&
    entities[parentSchemaKey][parentId] &&
    entities[parentSchemaKey][parentId][parentProperty] &&
    Array.isArray(entities[parentSchemaKey][parentId][parentProperty])
  ) {
    return entities[parentSchemaKey][parentId][parentProperty];
  }
}
