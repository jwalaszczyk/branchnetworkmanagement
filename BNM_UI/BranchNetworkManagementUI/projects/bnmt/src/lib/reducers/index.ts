import {ActionReducerMap} from '@ngrx/store';
import * as mainState from './main-state';
import {reducerDecorator} from './normalizr-decorator';
import * as fromQueryAdvisorState from "./query-advisor-response.reducer";

export const reducers: ActionReducerMap<mainState.FeatureState> = {
  normalized: reducerDecorator,
  queryAdvisorResponseState: fromQueryAdvisorState.reducer
};
