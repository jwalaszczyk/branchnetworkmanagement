/**
 * @author lszmolke
 * */
import {schema} from "normalizr";

// store schemas keys
export enum StoreKey {
  // queryAdvisorRequest
  queryAdvisorRequest,
  salesPoint1,
  salesPointList1,
  functionalRoleList,
  functionalRole,

  // queryAdvisorResponse
  queryAdvisorResponse,
  advisorList,
  advisor,
  salesPointList2,

  // querySalesPointRequest
  querySalesPointRequest,
  advisor1,
  salesPoint3,

  // querySalesPointResponse
  querySalesPointResponse,
  salesPointList,
  salesPoint,
  address,
  countryCodeText,

  // updateAdvisorPrimarySalesPointRequest
  updateAdvisorPrimarySalesPointRequest,
  advisor2,

  // updateAdvisorPrimarySalesPointResponse
  updateAdvisorPrimarySalesPointResponse,
  responseStatus,
  warningList,
  warning
}

export class StoreKeyUtils {
  public static storeKey(key: StoreKey): string {
    return StoreKey[key];
  }
}

// EntityOptions
const options = {idAttribute: "selectorId"};

// schemas

/**
 * WARNING !
 *
 * Each schema should have postfix = "Schema"
 *
 * ...Schema
 *
 * @see #{@link StoreSchemasInitializer}
 * */

//QueryAdvisorRequest
export const salesPoint1Schema = new schema.Entity(StoreKeyUtils.storeKey(StoreKey.salesPoint1), {}, options);
export const salesPoint1ListSchema = new schema.Entity(StoreKeyUtils.storeKey(StoreKey.salesPointList1), {SalesPointList1: [salesPoint1Schema]}, options);
export const functionalRoleSchema = new schema.Entity(StoreKeyUtils.storeKey(StoreKey.functionalRole), {}, options);
export const functionalRoleListSchema = new schema.Entity(StoreKeyUtils.storeKey(StoreKey.functionalRoleList), {functionalRole: [functionalRoleSchema]}, options);
export const queryAdvisorRequestSchema = new schema.Entity(StoreKeyUtils.storeKey(StoreKey.queryAdvisorRequest), {salesPointList: salesPoint1ListSchema, functionalRoleList: functionalRoleListSchema}, options);

//QueryAdvisorResponse
export const salesPoint2ListSchema = new schema.Entity(StoreKeyUtils.storeKey(StoreKey.salesPointList2), {}, options);
export const advisorSchema = new schema.Entity(StoreKeyUtils.storeKey(StoreKey.advisor), {salesPointList2: salesPoint2ListSchema, functionalRoleList: functionalRoleListSchema}, options);
export const advisorListSchema = new schema.Entity(StoreKeyUtils.storeKey(StoreKey.advisorList), {advisor: [advisorSchema]}, options);
export const queryAdvisorResponseSchema = new schema.Entity(StoreKeyUtils.storeKey(StoreKey.queryAdvisorResponse), {advisorList: advisorListSchema}, options);

//QuerySalesPointRequest
export const advisor1Schema = new schema.Entity(StoreKeyUtils.storeKey(StoreKey.advisor1), {}, options);
export const salesPoint3Schema = new schema.Entity(StoreKeyUtils.storeKey(StoreKey.salesPoint3), {}, options);
export const querySalesPointRequestSchema = new schema.Entity(StoreKeyUtils.storeKey(StoreKey.queryAdvisorResponse), {advisor1: advisor1Schema, salesPoint3: salesPoint3Schema}, options);

//QuerySalesPointResponse
export const countryCodeTextSchema = new schema.Entity(StoreKeyUtils.storeKey(StoreKey.countryCodeText), {}, options);
export const addressSchema = new schema.Entity(StoreKeyUtils.storeKey(StoreKey.address), {countryCodeText: countryCodeTextSchema}, options);
export const salesPointSchema = new schema.Entity(StoreKeyUtils.storeKey(StoreKey.salesPoint), {address: addressSchema}, options);
export const salesPointListSchema = new schema.Entity(StoreKeyUtils.storeKey(StoreKey.salesPointList), {salesPoint: [salesPointSchema]}, options);
export const querySalesPointResponseSchema = new schema.Entity(StoreKeyUtils.storeKey(StoreKey.queryAdvisorResponse), {salesPointList: advisorListSchema}, options);

//UpdateAdvisorPrimarySalesPointResponse
export const advisor2Schema = new schema.Entity(StoreKeyUtils.storeKey(StoreKey.advisor2), {primarySalesPoint: salesPoint1Schema}, options);
export const updateAdvisorPrimarySalesPointRequestSchema = new schema.Entity(StoreKeyUtils.storeKey(StoreKey.updateAdvisorPrimarySalesPointRequest), {advisor2: advisor2Schema}, options);

//UpdateAdvisorPrimarySalesPointResponse
export const warningSchema = new schema.Entity(StoreKeyUtils.storeKey(StoreKey.warning), {}, options);
export const warningListSchema = new schema.Entity(StoreKeyUtils.storeKey(StoreKey.warningList), {warning: [warningSchema]}, options);
export const responseStatusSchema = new schema.Entity(StoreKeyUtils.storeKey(StoreKey.responseStatus), {warningList: warningListSchema}, options);
export const updateAdvisorPrimarySalesPointResponseSchema = new schema.Entity(StoreKeyUtils.storeKey(StoreKey.updateAdvisorPrimarySalesPointResponse), {responseStatus: responseStatusSchema}, options);
