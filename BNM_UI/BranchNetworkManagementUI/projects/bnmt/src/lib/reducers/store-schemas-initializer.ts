
import {Injectable} from "@angular/core";
import {StoreService} from "@npm-libs/inteca-utils/src/lib/service/store.service";
import * as fromMainState from "./main-state";
import * as storeSchemas from "./store-schemas";
import {State} from "./main-state";
import {Store} from "@ngrx/store";
import {schema} from "normalizr";
import {StoreKey} from "./store-schemas";

/**
 * Initialize schemas with "Schema" postfix
 *
 * @author lszmolke
 * */
@Injectable()
export class StoreSchemasInitializer {
  constructor(private store: Store<fromMainState.State>, private storeService: StoreService<State>) {
    // find all schemas in store-schemas.ts
    let schemasNames = Object.keys(storeSchemas).filter(schema => schema.includes("Schema"));

    // register schemas
    if (schemasNames) {

      // check StoreKey.length === storeSchemas consts length
      if(schemasNames.length !== Object.values(StoreKey).filter(key => isNaN(key)).length) {
        console.warn("Length of StoreKey enum is different than schemas length !");
      }

      schemasNames.forEach(schemaName => {
        let entitySchema: schema.Entity = storeSchemas[schemaName];
        this.storeService.registerSchema(entitySchema);
      });
    }
  }
}
