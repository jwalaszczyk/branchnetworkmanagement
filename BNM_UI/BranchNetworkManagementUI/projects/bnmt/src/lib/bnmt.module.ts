import {ModuleWithProviders, NgModule} from '@angular/core';
import {BnmtComponent} from './bnmt.component';
import {BrowserModule} from "@angular/platform-browser";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {ButtonModule} from "primeng/button";
import {IntecaUtilsModule} from "@npm-libs/inteca-utils/src/lib/inteca-utils.module";
import {BnmtRoutesModule} from "./bnmt-routes.module";
import {StoreModule} from "@ngrx/store";
import {reducers} from "./reducers";
import {StoreSchemasInitializer} from "./reducers/store-schemas-initializer";
import {BnmLogicService} from "./services/external/bnm-logic.service";
import {DefaultService, QueryAdvisorRequest} from "../../../bnmt-logic-interfaces/src/lib";
import {ExternalEndpointsConfig} from "./services/external/external-endpoints.config";
import {HttpClientModule} from "@angular/common/http";
import {MainService} from "./services/main.service";
import {EffectsModule} from "@ngrx/effects";
import {QueryAdvisorResponseEffects} from "./effects/query-advisor-response.effects";

const primeNgModules = [
  ButtonModule
];


@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    primeNgModules,
    IntecaUtilsModule.forRoot(),
    BnmtRoutesModule,
    HttpClientModule,
    StoreModule.forFeature('bnmt', reducers),
    EffectsModule.forFeature([
      QueryAdvisorResponseEffects
    ])
  ],
  declarations: [BnmtComponent],
  exports: [BnmtComponent],
  providers: [StoreSchemasInitializer, MainService, BnmLogicService, DefaultService, ExternalEndpointsConfig]
})
export class BnmtModule {

  // inject schemas
  constructor(private storeSchemasInitializer: StoreSchemasInitializer, private mainService: MainService) {
    console.warn("Call query advisor");
    this.mainService.queryAdvisor({
      channel: "123",
      operatorId: "operator",
      selectorId: 1,
      functionalRoleList: {
        functionalRole: [
          {selectorId: 2, code: "3", id: "3", name: "3"}
        ]
      },
      salesPointList: {
        selectorId: 3,
        salesPoint: [
          {
            id: "3",
            selectorId: 4
          }
        ]
      }
    } as QueryAdvisorRequest)
  }

  static forRoot(): ModuleWithProviders {
    return {
      ngModule: BnmtModule,
      providers: []
    };
  }
}
