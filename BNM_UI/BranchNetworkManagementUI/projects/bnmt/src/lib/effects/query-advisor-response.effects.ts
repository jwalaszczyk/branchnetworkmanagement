/**
 * @author lszmolke
 * */
import {Injectable} from "@angular/core";
import {EntityCrudEffect} from 'ngrx-normalizr-crud';
import {QueryAdvisorResponse} from "../../../../bnmt-logic-interfaces/src/lib";
import {Actions, Effect} from "@ngrx/effects";
import {BnmLogicService} from "../services/external/bnm-logic.service";
import {queryAdvisorResponseSchema} from "../reducers/store-schemas";
import {map} from "rxjs/operators";
import {StoreService} from "@npm-libs/inteca-utils/src/lib/service/store.service";
import {State} from "../reducers/main-state";

@Injectable()
export class QueryAdvisorResponseEffects extends EntityCrudEffect<QueryAdvisorResponse> {
  constructor(protected actions$: Actions, private bnmLogicClient: BnmLogicService, private storeService: StoreService<State>) {
    super(actions$, queryAdvisorResponseSchema);
  }

  @Effect()
  queryAdvisorResponseEffect$ = this.createSearchEffect(
    action => {
      return this.bnmLogicClient.queryAdvisor(action.payload).pipe(
        map(response => {
          console.warn("QueryAdvisorResponse test effect");
          if(response) {
            console.warn(JSON.stringify(response));
          }

          // this.storeService.addData(response, StoreKeyUtils.storeKey(StoreKey.queryAdvisorResponse));

          return [response];
        })
      )
    }
  );
}
