import { InjectionToken } from '@angular/core';

export const BASE_PATH = '';
export const COLLECTION_FORMATS = {
    'csv': ',',
    'tsv': '   ',
    'ssv': ' ',
    'pipes': '|'
}
