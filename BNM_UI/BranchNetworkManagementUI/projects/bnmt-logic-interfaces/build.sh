#!/bin/bash

SWAGGER="src/lib/dist/swagger-codegen-cli-2.3.1.jar"

echo "getting swagger-codegen-cli jar"
mkdir -p src/lib/dist
test -f "$SWAGGER" || curl -o "$SWAGGER" http://central.maven.org/maven2/io/swagger/swagger-codegen-cli/2.3.1/swagger-codegen-cli-2.3.1.jar

echo "cleaning model and api folders"
rm -r src/lib/model
rm -r src/lib/api

java -jar src/lib/dist/swagger-codegen-cli-2.3.1.jar generate -i ../../../BranchNetworkManagementLogicServiceApi/src/main/resources/BranchNetworkManagementService_Logic.json \
-o src/lib -l typescript-angular --additional-properties 'modelPropertyNaming=original' --additional-properties 'ngVersion=6.0' --type-mappings date=Date

echo "appending SelectorBase"

find src/lib/model -type f | xargs sed 's/interface [a-zA-Z0-9]*/& extends SelectorBase/g' -i # interface Pet extends SelectorBase
find src/lib/model -type f | xargs sed 's/ \*\//&\nimport { SelectorBase } from '\''.\/selectorBase'\'';/g' -i # import { SelectorBase } from './selectorBase';
echo "export * from './selectorBase';" >> src/lib/model/models.ts

echo "export interface SelectorBase {" > src/lib/model/selectorBase.ts
echo "    selectorId: number;" >> src/lib/model/selectorBase.ts
echo "}" >> src/lib/model/selectorBase.ts

sed 's/protected basePath/public basePath/g' -i src/lib/api/default.service.ts
sed 's/rxjs\/Observable/rxjs/g' -i src/lib/api/default.service.ts
sed 's/export const BASE_PATH .*$/export const BASE_PATH = '\'''\'';/g' -i src/lib/variables.ts
sed -e '/@param\|@return/ s/{.*} //' -i src/lib/configuration.ts
