import {Injectable} from '@angular/core';
import {IAppConfig} from '../config/app-config.model';
import {environment} from '../../environments/environment';
import {LibConfig} from "../../../projects/bnmt/src/lib/config/lib.config";
// import { LibConfig} from '@ufeintc/apqc10185-bcla';
// import {LibConfig as LibConfigCustomerOrder}   from '@ufeintc/customer-order-search';

@Injectable()
export class AppConfig {

  static settings: IAppConfig;


  static load() {
      const jsonFile = `assets/config/config.${environment.name}.json`;
      const jsonBnmFile = `assets/config/config.bnm.${environment.name}.json`;
      return new Promise((resolve, reject) => {
          const xhr = new XMLHttpRequest();
          xhr.overrideMimeType('application/json');
          xhr.open('GET', jsonFile, true);
          xhr.onreadystatechange = () => {
              if (xhr.readyState === 4) {
                  if (xhr.status === 200) {
                      this.settings = JSON.parse(xhr.responseText);
                      //get props for bcla
                      const xhrb = new XMLHttpRequest();

                      xhrb.open('GET', jsonBnmFile, true);
                      xhrb.onreadystatechange = () => {
                          if (xhrb.readyState === 4) {
                              if (xhrb.status === 200) {
                                  LibConfig.config = JSON.parse(xhrb.responseText);
                                  resolve();
                              }
                              else {
                                  reject(`Could not load file '${jsonBnmFile}': ${xhrb.status}`);
                              }
                          }

                      }
                      xhrb.send(null);


                  } else {
                      reject(`Could not load file '${jsonFile}': ${xhr.status}`);
                  }
              }
          };
          xhr.send(null);
      });
  }

}
